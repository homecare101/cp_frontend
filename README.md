# Micasacare


# Prerequisite
   * [react.js]
   * [apollo-client]
   * [apollo-link-state]
   * [apollo-cache-inmemory]
   * [apollo-link-http]
   * [apollo-link]
   * [yarn]

### Technologies
Micasacare uses following modules:
	
	* [react] - javascript framework
	
	* [apollo-client] - Apollo Client is a fully-featured caching GraphQL client with integrations for React, Angular, and more. It allows you to easily build UI components that fetch data via GraphQL. To get the most value out of apollo-client, you should use it with one of its view layer integrations.

	* [apollo-link-state] - Managing remote data from an external API is simple with Apollo Client.

	* [apollo-cache-inmemory] - apollo-cache-inmemory is the recommended cache implementation for Apollo Client 2.0. InMemoryCache is a normalized data store that supports all of Apollo Client 1.0's features without the dependency on Redux.

	* [apollo-link-http] - The http link is a terminating link that fetches GraphQL results from a GraphQL endpoint over an http connection.

	* [apollo-link] - apollo-link is a standard interface for modifying control flow of GraphQL requests and fetching GraphQL results

### Installation and start
* yarn install
* yarn start