import { ApolloClient } from 'apollo-client';
import { withClientState } from 'apollo-link-state';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { ApolloLink } from 'apollo-link';
import { editorDefaults, editorResolvers } from './resolvers/editor';

import { API_URL } from './utils/constants';

const cache = new InMemoryCache();
const stateLink = withClientState({ cache, resolvers: editorResolvers, defaults: editorDefaults })
const client = new ApolloClient({
  cache,
  link: ApolloLink.from([stateLink, new HttpLink({
    uri: `${API_URL}/server`,
  })]),
});

export default client;