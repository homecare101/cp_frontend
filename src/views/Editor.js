import React, { Component } from 'react';
import {
    Container,
    Row,
    Col
} from 'reactstrap';

import EditorSidebar from '../components/Editor/EditorSidebar'
import EditorSelectedTab from '../components/Editor/EditorSelectedTab'
import MainEditor from '../components/Editor/MainEditor';
import Rotatetext from '../components/Editor/Rotatetext';
import { withRouter } from 'react-router';
import { ApolloProvider, Query, graphql, compose, withApollo } from 'react-apollo';
import './Editor.css';
import MobileEditorTab from '../components/common/MobileEditorTab';
import MobileCaseMaterialTab from '../components/common/MobileCaseMaterialTab';
import MobilePopupTab from '../components/common/MobilePopupTab';
import MobileEditorTabList from '../components/common/MobileEditorTabList';
import ModelTabMutationConnected from '../components/Editor/Tabs/ModelTab';
import ModelDropDown from '../components/common/caseDropDown/ModelDropDown';
import MaterialDropDown from '../components/common/materialDropDown/MaterialDropDown';
import { closePopUp } from '../utils/functions';
import MutateEditor from '../components/HOC/MutateEditor';
import WithEditor from '../components/HOC/WithEditor';
import { getCaseDataMutation } from '../Graphql/Query/editor';

class Editor extends Component {
    
    componentWillMount(){

        if(this.props.match.params.caseId){
            var caseId =this.props.match.params.caseId
            var userId = localStorage.getItem('id')
            this.props.client.query({
                query: getCaseDataMutation,
                variables: {
                    userId: userId,
                    caseId: caseId
                },
            }).then(res => {
                let caseData = res.data.getUserCasesById.caseData
                caseData = JSON.parse(caseData)
                let dataToSend = caseData.editor
                this.props.mutateStore({
                    activeElementDims: dataToSend.activeElementDims,
                    backgroundColor:  dataToSend.backgroundColor,
                    height: dataToSend.height,
                    width: dataToSend.width,
                    isMouseDown: dataToSend.isMouseDown,
                    offset: dataToSend.offset,
                    resizeCoords: dataToSend.resizeCoords,
                    model: dataToSend.model,
                    activeElementIndex: dataToSend.activeElementIndex,
                    activeRotation: dataToSend.activeRotation,
                    collage: dataToSend.collage,
                    currentMousePointer: dataToSend.currentMousePointer,
                    elements: dataToSend.elements,
                    isRotating: dataToSend.isRotating,
                    removeExternalLayers: dataToSend.removeExternalLayers,
                    resizeEdge: dataToSend.resizeEdge,
                    selectedPath: dataToSend.selectedPath,
                })
    
            })

            .catch(error => {
                console.log(error, "error")
            })
        }
    }
    redirectToHome(){
        this.props.history.push('/')
    }
    render () {
        var height =  window.innerHeight
        return (
            <Container fluid style={{height:height, padding:0}}>
               
                <div className="mainContainerEditor">

                <div className='editor-modal-close'>
                    <button type="button" onClick={this.redirectToHome.bind(this)}  className="editor-modal-close-buttonMain" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                    <Row className="full-height outerCon">
                        <Col md={6} className="svgContainer full-height">
                            <MainEditor />
                             <Rotatetext className="d-none d-md-block"/> 
                        </Col>
                        <Col md={3} className="full-height selectTab d-none d-md-block editorAddFiles">
                            <EditorSelectedTab />
                        </Col>
                        <Col md={3} className="Editorsidebar full-height no-padding d-none d-md-block">
                            <EditorSidebar />
                        </Col>
                    </Row>
                </div>
                <Row id="mobile-tool-list" className="mobile-tool-list d-xs-block  d-md-none d-lg-none">
                    <Col xs={6}>
                        <MobilePopupTab />
                    </Col>
                    <Col xs={12} className="popContainer">
                        <Row id="material-model-dropdown" className="material-model-dropdown">
                            <Col style={{borderRight:'1px solid #d9d9d9'}} ><ModelDropDown /></Col>
                            <Col><MaterialDropDown /></Col>
                        </Row>
                        <Row>
                            <Col>
                                <button id="mobile-tab-close" className="mobile-tab-close" onClick={closePopUp}>
                                    DONE
                                </button>
                            </Col>
                        </Row>

                    </Col>
                    <Col xs={12}>
                        <MobileEditorTabList />
                    </Col>
                </Row>
            </Container>

        )

    }

}

const EditorConnected = WithEditor(Editor)
const RotatetextMutationConnected = MutateEditor(EditorConnected)
export default withRouter(withApollo(RotatetextMutationConnected));