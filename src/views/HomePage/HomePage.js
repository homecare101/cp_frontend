
import React from 'react';
import { Button, Modal, Input, ModalHeader, ModalBody, ModalFooter, Form, FormGroup} from 'reactstrap';

import './HomePage.css';

class ModalExample extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  render() {
    return (
      <div>
        <Button className="btn" color="danger" onClick={this.toggle}>{this.props.buttonLabel} Login </Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle} centered>
          <div className="signup-modal-container">
            <div className="signup-modal-title"><span>Sign Up</span></div>
            <div className="signup-modal-content">
              <Form>
                <FormGroup>
                  <Input className="email" type="email" name="email" id="exampleEmail" placeholder="Email" />
                </FormGroup>
                <FormGroup>
                  <Input type="password" name="password" id="password" placeholder="Password" />
                </FormGroup>
                <FormGroup>
                  <Input type="password" name="confirmPassword" id="confirmPassword" placeholder="Confirm Password" />
                </FormGroup>
                <Button onClick={this.toggle} block>Sign Up</Button>
              </Form>
            </div>
            <div className='signup-lower-link'>
              <Button color="link">Login</Button>
            </div>
            <div className='signup-lower-link'>
              <Button color="link">Reset Password</Button>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

export default ModalExample;
