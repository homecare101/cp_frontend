import axios from 'axios';
import { API_URL } from './utils/constants';
import { showLoader, stopLoader } from './utils/functions';

export const uploadPhoto = file => {

    let formData = new FormData();

    formData.append('file', file)

    showLoader()

    return new Promise((response, reject) => {

        axios.post(`${API_URL}/upload`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(res => {

            stopLoader()

            response(res.data)

        })
        .catch(reject)

    })

}