import React from 'react'
import { Container, Row, Col, Button } from 'reactstrap';
import './LightFooter.css'
import { Link } from 'react-router-dom';


class LightFooter extends React.Component {
    render() {
        return (
            <Container fluid>
                <Row>
                    <Col md={12} className="light-footer-main">
                        <div className="footer-out">
                        <Row>
                            <Col md={6} sm={6} xs={12}>
                                <Row>
                                    <Col md={6} className="alineCenter">
                                        <div className="footer-imageOut">
                                            <img src="/logo.png" className="imageAll" />
                                        </div>
                                    </Col>
                                    <Col md={6} className="no-padding alineCenter">
                                        <div className="footer-text">
                                            <div>
                                                <p>케이스 만들기</p>
                                            </div>
                                            <div>
                                                <p>케이스 상점</p>
                                            </div>
                                            <div>
                                                <p>고객센터</p>
                                            </div>

                                        </div>
                                    </Col>
                                </Row>
                            </Col>
                            <Col md={6} sm={6} xs={12}>
                                <Row>
                                    <Col md={6} xs={12} className="no-padding footerContact">
                                        <div className="footer-text">
                                            <div>
                                                <p>이용약관</p>
                                            </div>
                                            <div>
                                                <p>개인정보 취급방침</p>
                                            </div>
                                            <div>
                                                <p>통신판매업신고 2016-고양일산동-1300</p>
                                            </div>
                                        </div>
                                    </Col>
                                    <Col md={6} xs={12} className="no-padding footerContact">
                                        <div className="footer-text-last">
                                            <p>+82 67 0923 5692</p>
                                            <p>hello@custompark.com</p>
                                        </div>
                                    </Col>
                                </Row>
                            </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
            </Container>
        )
    }
}
export default LightFooter