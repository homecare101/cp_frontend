import React, { Component } from 'react';
import './header.css'
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom'
import qs from 'query-string';
import { withApollo, Query, compose, graphql } from 'react-apollo';


import {
    Col,
    Collapse,
    Container,
    Navbar,
    Row,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
} from 'reactstrap';
import CartMenu from '../CartMenu/cartmenu';
import { AuthModalConsumer } from '../Auth/AuthModalProvider/AuthModalProvider';
import { checkCartMutation } from '../../Graphql/Mutations/cart';
import { setToast } from '../../actions/editor';
import { getCaseMutation } from '../../Graphql/Query/editor';

class ShowResetModal extends React.Component {

    componentDidMount() {
        const parsed = qs.parse(window.location.search);
        const verficationCode = parsed.verificationCode
        if (verficationCode) {
            this.showModal()
        }
    }
    showModal() {
        this.props.setFormType('ResetPassword')
        this.props.openModal()
    }
    render() {
        return null
    }
}
class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            formType: 'Login',
            verficationCode: '',
            isOpenNav: false,
            CartOpen: false,
            label: 'logo',
            showCart: true,
            loginChange: false,
            showLogo: false,
            hideText: false,
            saveDesignData: [],
            cartItemData: []
        };
    }
    toggleHeader() {
        this.setState({ isOpenNav: !this.state.isOpenNav });
    }
    shoppingToggle() {
        let token = localStorage.getItem('token')
        if (token != null) {
            let CartOpen = this.state.CartOpen
            this.setState({ CartOpen: !CartOpen })
        }
        else {
            return null
        }
    }
    componentWillMount() {
        if (this.props.label) {
            this.setState({ label: "Admin Pannel", showCart: false })
        }
    }
    componentDidMount() {
        let token = localStorage.getItem('token')
        const parsed = qs.parse(window.location.search);
        const verficationCode = parsed.verificationCode
        if (verficationCode) {
            this.setState({ verficationCode, modal: true, formType: 'ResetPassword' })
        }
        if (token == null) {
            this.setState({ showCart: false })
        } else {
            this.setState({ showCart: true })
        }
        this.handleResize()
        window.addEventListener('resize', () => {
            this.handleResize()
        });

        let userId = localStorage.getItem('id')

        if (userId) {
            /////save design
            this.props.client.query({
                query: getCaseMutation,
                fetchPolicy: "network-only",
                variables: {
                    userId: userId,
                },
            }).then(res => {
                var array = res.data.getUserCases.cases
                this.setState({ saveDesignData: array })
            })
                .catch(error => {
                    console.log(error, "error")
                    let errorText = { ...error }
                    if (errorText !== {}) {
                        this.props.setToast({
                            variables: { type: 'error', message: "Some error occurs Please try Agin later" }
                        })
                    }
                })
            /////cart design
            this.props.createCart({
                variables: {
                    userId: userId
                },
            }).then(res => {
                let cartData = res.data.checkCart.items
                this.setState({ cartItemData: cartData, showLoader: false })
            })
                .catch(error => {
                    console.log(error, "error 	")
                    let errorText = { ...error }
                    if (errorText !== {}) {
                        this.props.setToast({
                            variables: { type: 'error', message: "Some error occurs Please try Agin later" }
                        })
                    }
                })
        }

    }
    handleResize() {
        if (window.innerWidth < 1020) {
            this.setState({ showLogo: true, hideText: true })
        }
        if (window.innerWidth > 1020) {
            this.setState({ showLogo: false, hideText: false })
        }
    }
    login() {
        var userId = localStorage.getItem('id')
        if (userId === null) {
            let { loginChange } = this.state
            this.setState({ loginChange: !loginChange })
        } else {
            this.props.history.push('/userDetails/orders')
        }
    }
    closeModal(params) {
        this.setState({ loginChange: false })
    }
    checkToken() {
        let token = localStorage.getItem('token')
        if (token) {
            return true
        } else {
            return false
        }
    }
    openUserDetails() {
        localStorage.setItem('selectedTabId', 1)
        this.props.history.push('/userDetails/orders')
    }
    openSavedDetails() {
        localStorage.setItem('selectedTabId', 2)
        this.props.history.push('/userDetails/savedDesign')
    }
    render() {
        return (
            <div>
                <AuthModalConsumer>
                    {
                        ({ openModal, setFormType }) =>
                            <ShowResetModal openModal={openModal} setFormType={setFormType} />
                    }
                </AuthModalConsumer>
                <Container fluid>
                    <Row className="navbar_header">
                        <Col md={12}>
                            <Row>
                                <Col md={3} xs={4} className="logoOutMain">
                                    {this.state.showLogo ?
                                        <NavbarBrand>
                                            <Link to="/">
                                                <img src="/logoSmall.png" className="imageAll" />
                                            </Link>
                                        </NavbarBrand>
                                        :
                                        <NavbarBrand className="logoImageOut">
                                            <Link to="/">
                                                <img src="/logo.png" className="imageAll" />
                                            </Link>
                                        </NavbarBrand>
                                    }
                                </Col>
                                <Col md={9} xs={8} className="header-content-align">
                                    {this.state.hideText ?
                                        <div className="toogleBtn" onClick={this.toggleHeader.bind(this)}>
                                            <div class="bar1"></div>
                                            <div class="bar2"></div>
                                            <div class="bar3"></div>
                                        </div>
                                        :
                                        <div className="header-center-text-outter">
                                            <Row className="displayNone">
                                                <Col className="header-center-text alignEnd">
                                                    <Link to="/editor">
                                                        <p className='header-span'>
                                                            케이스 만들기
                                                        </p>
                                                    </Link>
                                                </Col>
                                                <Col className="header-center-text alignCenter">
                                                    <Link to="/stock">
                                                        <p className='header-span'>
                                                            케이스 상점
                                                        </p>
                                                    </Link>
                                                </Col>
                                                <Col className="header-center-text">
                                                    <Link to="/contactus/">
                                                        <p className='header-span'>
                                                            질문 & 답변
                                                        </p>
                                                    </Link>
                                                </Col>
                                            </Row>
                                        </div>
                                    }
                                    <div className="shop_head">
                                        <div className="right-logo">
                                            <AuthModalConsumer>
                                                {
                                                    ({ openModal }) => <div className="right-logo" onClick={this.checkToken() ? this.openSavedDetails.bind(this) : openModal}>
                                                        <div className="iconMainCon">
                                                            <img src="/heart@3x.png" className="imageAll" />
                                                            {this.state.saveDesignData.length ? <div className="badgeIcon"></div> : null}
                                                        </div>
                                                    </div>
                                                }
                                            </AuthModalConsumer>
                                            <AuthModalConsumer>
                                                {
                                                    ({ openModal }) => <div className="right-logo" onClick={this.checkToken() ? this.openUserDetails.bind(this) : openModal}>
                                                        <div className="iconMainCon">
                                                            <img src="/person@3x.png" className="imageAll" />
                                                        </div>
                                                    </div>
                                                }
                                            </AuthModalConsumer>
                                            {
                                                this.checkToken() ? <div className="iconMainCon" onClick={this.shoppingToggle.bind(this)}>
                                                    <img src="/cart@3x.png" className="imageAll" />
                                                    {this.state.cartItemData.length ? <div className="badgeIcon"></div> : null}
                                                </div>
                                                    : null}
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Container>

                {this.state.isOpenNav ?
                    <div className="menuModalOpen">
                        <Link to="/editor">
                            <p className='header-span'>
                                케이스 만들기
                            </p>
                        </Link>
                        <Link to="/stock">
                            <p className='header-span'>
                                케이스 상점
                            </p>
                        </Link>
                        <Link to="/contactus/">
                            <p className='header-span'>
                                질문 & 답변
                            </p>
                        </Link>
                    </div> : null}

                {this.state.CartOpen ?
                    <div className="cartModalOpen">
                        <CartMenu />
                    </div> : null}
            </div>
        )
    }
}

export default withRouter(compose(
    withApollo, [
        graphql(checkCartMutation, { name: 'createCart' }),
        graphql(setToast, { name: 'setToast' })]
)(Header))