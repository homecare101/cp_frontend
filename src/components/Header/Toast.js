import React from 'react'
import './header.css'
import { Alert } from 'reactstrap'

class Toast extends React.Component {
    constructor() {
        super()
        this.state = {
            close: false
        }
    }
    
    closeToast() {
        HideToast()
    }
    render() {
        return (
            <div id='toastshow' className="tosteClose">
                <p id="toastText">
                    {this.props.text}
                </p>
                <i onClick={this.closeToast.bind(this)} className="fa fa-close"></i>
            </div>
        )
    }
}

export const ShowToast = () => document.getElementById('toastshow').classList.add('tosteMian')
export const HideToast = () => document.getElementById('toastshow').classList.remove('tosteMian')

export default Toast