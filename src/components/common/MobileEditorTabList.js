import React, { Component } from 'react'
import { graphql } from 'react-apollo';

import SidebarTabs from '../Editor/Tabs/SidebarTabs';
import QueryConnected from '../HOC/QueryConnected';
import { Row, Col} from 'reactstrap'
import { getSelectedTab,  setSelectedTab } from '../../actions/editor';
import MobileEditorTab from './MobileEditorTab';

class MobileEditorTabList extends Component {

    componentDidMount () {
        
        this.props.mutate({ variables: { selectedTabID: SidebarTabs[0].id } });
    }

    onTabSelect = tab => {
        this.props.mutate({ variables: { selectedTabID: tab.id } });
        document.getElementById('MobilePopupTab').classList.add("active")
        document.getElementById('mobile-tool-list').classList.add("close-mobile-tab")
        document.getElementById('material-model-dropdown').classList.add("display-none")
        document.getElementById('mobile-tab-close').classList.add("display-block")



    }

    render () {

        return (
            <Row className="mobile-tab-bar d-xs-block d-md-none d-lg-none">
                {
                    SidebarTabs.filter(tab => !tab.hideMobile).map(
                        (tab, index) => <Col key={index} onClick={() => this.onTabSelect(tab)}><MobileEditorTab src={tab.src} title={tab.title}/></Col>
                    )
                }
            </Row>
        )
    }

}

const MobileEditorTabListConnected = QueryConnected(getSelectedTab)(MobileEditorTabList)
const MobileEditorTabListMutationConnected = graphql(setSelectedTab)(MobileEditorTabListConnected)

export default MobileEditorTabListMutationConnected

