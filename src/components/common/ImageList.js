import React, { Component } from 'react';
import WithEditor from '../HOC/WithEditor';
import MutateEditor from '../HOC/MutateEditor';


class ImageList extends Component {
    render () {
        const {
            editor
        } = this.props;

        const { collage } = editor
        let { data } = this.props

        return (
            <div className="imageList">
                <ul> 
                    {
                        data.map(item => {
                            return(
                            <li key={item.id} >
                                <div className="innerImgCon" onClick={() => this.props.onClick(item)}>
                                   <img style={{
                                        width: '100%',
                                        height: 100,
                                        objectFit: 'contain',
                                    }} src={item.src} className={ item.id == collage ?  'activeCollageItem'  : 'img-responsive' } /> 
                                </div>
                            </li>
                            )
                        })
                    } 
                </ul>

            </div>
        )

    }

}

const ImageListConnected = WithEditor(ImageList)
const ImageListMutationConnected = MutateEditor(ImageListConnected)

export default ImageListMutationConnected