import React, { Component } from 'react';

class MobileCaseMaterialTab extends Component {
    render () {

        const { info, title } = this.props;

        return (
            <div className="MobileCaseMaterialTab">
                <lable className="case-mat-title">{title}</lable>
                <p className="">{info}</p>
            </div>
        )

    }

}

export default MobileCaseMaterialTab