import React from 'react';
import TabLink from './TabLink';

const TabLinkList = ({
    tabs = [],
    onSelect = () => {},
    selectedTab = null
}) => tabs.map(
    tab =>{ 
        return <TabLink
            isActive={selectedTab == tab.id}
            key={tab.title}
            onClick={() => onSelect(tab)}
            {...tab}
        />
    }
)

export default TabLinkList;