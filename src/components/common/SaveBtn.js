import React from 'react'
import { AuthModalConsumer } from './../Auth/AuthModalProvider/AuthModalProvider'
import { Modal, Input, Button } from 'reactstrap';
import QueryConnected from './../HOC/QueryConnected';
import { updateElements, getEditor, setToast } from './../../actions/editor';
import WithEditor from './../HOC/WithEditor';
import MutateEditor from './../HOC/MutateEditor';
import { withRouter } from 'react-router';
import { compose, graphql } from 'react-apollo'
import { editorMutaion, editCaseMutation } from './../../Graphql/Mutations/editor';
class SaveBtn extends React.Component {
    constructor() {
        super()
        this.state = {
            modal: false,
            errorInd: false,
            name: '', 
        }
    }
    getChildElement(child, parent){
        let childElements = document.getElementsByClassName(child)
          let correctChild = ""
          for(let i = 0; i<=childElements.length - 1; i++){
            if(childElements[i].ownerSVGElement.parentElement.id === parent){
              correctChild = childElements[i]
            }
          }
          return correctChild
      }
      changePathOfElement (pathVal, child, parent) {
        let childElements = document.getElementsByClassName(child)
        for(let i = 0; i<=childElements.length - 1; i++){
          
          if(childElements[i].ownerSVGElement.parentElement.id === parent){
            childElements[i].setAttribute("d", pathVal)
            childElements[i].style.stroke = "#0096ff"
          }
        }
      }
    openModal() {
        this.props.mutate({
          variables: {
            activeElementIndex: null,
            removeExternalLayers: true
          }
        }).then((res) => {
          let imgData = document.getElementById('svg-container').outerHTML
          let checkExistingSvg = document.getElementById('outlineSVG')
          if( checkExistingSvg ){
            checkExistingSvg.innerHTML = imgData
          } else {
            let outlineSVG = document.createElement('div')
            outlineSVG.setAttribute("id", "outlineSVG")
            outlineSVG.style.display = "none"
            outlineSVG.innerHTML = imgData
            document.body.appendChild(outlineSVG)
          }
          let svgElement = document.querySelector('#outlineSVG > #svg-container')
          svgElement.style.transform = "scale(6)"
          let shadowElement = this.getChildElement("shadowImage","outlineSVG")
          shadowElement.style.display = "none"
          let simpleImageElement = this.getChildElement("simpleImage","outlineSVG")
          simpleImageElement.style.display = "none"
          // let outlineInnerPathElement = this.getChildElement("outlineInnerPath","outlineSVG")
          // outlineInnerPathElement.style.display = "inline"
          let outerPathToGive = document.getElementById('hiddenPath').getAttribute("d")
          this.changePathOfElement(outerPathToGive, "phoneCaseClipPath", "outlineSVG")
          let phoneCaseClipElem = this.getChildElement("phoneCaseClip","outlineSVG")
          phoneCaseClipElem.id = "finalPhoneCaseClipPath"
          let respectiveClipElem = this.getChildElement("respectiveClip","outlineSVG")
          respectiveClipElem.removeAttribute("clip-path")
          respectiveClipElem.setAttribute("clip-path","url(#finalPhoneCaseClipPath)")
          let outlineSvgData = document.getElementById('outlineSVG').innerHTML
          this.setState({ preview: imgData, modal: true, outlineSvgData: outlineSvgData})
    
        })
    
      }
    checkToken() {
        let token = localStorage.getItem('token')
        if (token) {
          return true
        } else {
          return false
        }
      }
      handleChange(key, event) {
        let value = event.target.value;
        let { name } = this.state
        name = value
        this.setState({ name });
      }
      saveCase() {
        const { data } = this.props;
        let value = JSON.stringify(data)
        let user = localStorage.getItem('id')
        let { preview, name, outlineSvgData} = this.state
        let caseId = this.props.match.params.caseId
        if (this.props.match.params.caseId) {
          console.log("update")
          this.props.editData({
            variables: {
              caseId: caseId,
              caseData: value,
              name: name,
              count: 1,
              price: 15,
              userId: user,
              preview: preview,
              material: data.editor.materials,
              model: data.editor.model,
              outlineSvgData: outlineSvgData,
              brand: '',
            }
          }).then(response => {
          console.log("update22")
    
            this.props.setToast({
              variables: {
                type: 'success',
                message: "case Edited Successfully"
              }
            })
            this.props.history.push(`/userDetails/savedDesign`)
          }).catch(error => {
            console.log(error, "error")
            let errorText = { ...error }
            if (errorText !== {}) {
              this.props.setToast({
                variables: { type: 'error', message: "Some error occur" }
              })
            }
          })
    
        } else {
          this.props.saveData({
            variables: {
              caseData: value,
              name: name,
              userId: user,
              preview: preview,
              count: 1,
              price: 15,
              outlineSvgData: outlineSvgData,
              material: data.editor.materials,
              model: data.editor.model,
              brand: '',
            }
          }).then(response => {
            this.props.setToast({
              variables: {
                type: 'success',
                message: "case Saved Successfully"
              }
            })
            this.setState({ modal: false })
            this.props.history.push(`/userDetails/savedDesign`)
          }).catch(error => {
            console.log(error, "error")
            let errorText = { ...error }
            if (errorText !== {}) {
              this.props.setToast({
                variables: { type: 'error', message: errorText.graphQLErrors[0].message }
              })
            }
          })
        }
      }

  loginToggle() {
    this.setState({ modal: !this.state.modal });
  }
    render() {
        return (
            <div>
            <AuthModalConsumer>
              {
                ({ openModal }) => {
                  let token = localStorage.getItem('token')
                  const onClick = token ? this.openModal.bind(this) : openModal
                  return <Button className="savebtnEditor" onClick={onClick}>{!this.checkToken() ? 'Login to Save' : (this.state.toggleBtn ? 'Update' : 'Save')}</Button>
                }
              }
            </AuthModalConsumer>
            <Modal isOpen={this.state.modal} centered className="home-auth-modal">
                    <div>
                        <div className='home-modal-close'>
                        <button type="button" onClick={this.loginToggle.bind(this)} className="home-modal-close-button" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <div className="modalOpen">
                        <h1>케이스 이름을 입력하세요</h1>
                        <div className="displayFlex">
                            <div className="IconInput">
                            <Input className="name" type="text" name="name" value={this.state.name} onChange={this.handleChange.bind(this, 'name')} placeholder="사례 이름" />
                            {this.state.errorInd ? <span className="hasError errorMsgCard">This field is required</span> : null}
                            </div>
                        </div>
                        <Button className="savebtn" onClick={this.saveCase.bind(this)}>{this.state.toggleBtn ? '수정' : "저장하기"}</Button>
                        </div>
                    </div>
                    </Modal>
            </div>
        )
    }
}

const SaveBtnQuery = QueryConnected(getEditor)(SaveBtn)
const SaveBtnQueryGql = graphql(updateElements)(SaveBtnQuery)

const SaveBtnQueryGqlWithEditor = WithEditor(SaveBtnQueryGql)
const SaveBtnQueryGqlWithEditorMUtualEditor = MutateEditor(SaveBtnQueryGqlWithEditor)


export default withRouter(compose([
    graphql(editorMutaion, { name: 'saveData' }),
    graphql(setToast, { name: 'setToast' }),
    graphql(editCaseMutation, { name: 'editData' })]

  )(SaveBtnQueryGqlWithEditorMUtualEditor));