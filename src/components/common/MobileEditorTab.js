import React, { Component } from 'react';

class MobileEditorTab extends Component {
    render () {

        const { src, title } = this.props;

        return (
            <div className="MobileEditorTab text-center">
                    <img src={src} className="mobile-icon" height="20" width="20"/>
                    <p className="mobile-title">{title}</p>
            </div>
        )

    }

}

export default MobileEditorTab