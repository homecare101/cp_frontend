import React, { PureComponent } from 'react';
import {
    ListGroupItem, Row
} from 'reactstrap';
import './tabStyle.css'

class TabLink extends PureComponent {
    render() {
        const {
            title = '',
            activeSrc,
            src,
            id,
            onClick = () => {},
            isActive = false
        } = this.props
        return (
            <ListGroupItem onClick={onClick} className={isActive ? 'active' : ''} >
                <div className="ListEditorOut">
                    <div className="imageListEditor">
                        <img src={isActive ? activeSrc :src} style={{ height: '100%', width: '100%' }} />
                    </div>
                    <span className="innertags">{title}</span>
                </div>
            </ListGroupItem>
        )
    }

}

export default TabLink