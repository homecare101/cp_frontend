import React, { Component } from 'react';
import ImageUploader from '../Editor/EditorTools/Image/ImageUploader';

class HeadlessImageUploader extends Component {

    render () {

        return (
            <ImageUploader
                onImageUpload={this.props.onImageUpload}
                forwardRef={this.props.forwardRef}
                style={{ display: 'none' }}
            ></ImageUploader>
        )

    }

}

export default HeadlessImageUploader