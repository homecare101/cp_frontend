import React from 'react';

import Slider from 'react-rangeslider'
import { updateElementProps, getActiveGroupDims } from '../../utils/elements';
import WithEditor from '../HOC/WithEditor';
import MutateEditor from '../HOC/MutateEditor';


class RotateSlider extends React.Component {
    constructor(props, context) {
        super(props, context)
        this.state = {
            degree: 0,
        }
    }

    handleChangeStart = (degree) => {

        this.rotationDegree = 0

        this.props.mutateStore({
            activeRotation:true
        })

    };

    handleChange = degree => {

        this.setState({
            degree
        })

        const { activeElementIndex, elements } = this.props.editor

        const selectedElement = elements[activeElementIndex] || {}

        const newElements = [...elements]

        let modifiedElement = selectedElement

        if (activeElementIndex !== null) {

            const activeElementBox = document.getElementById('activeChildId')

            let width = 0, height = 0;

            if(activeElementBox != null) {

                width = activeElementBox.getBBox().width
                height = activeElementBox.getBBox().height

            }

            var angle = degree

            const parsedProps = JSON.parse(selectedElement.props)

            modifiedElement = updateElementProps(modifiedElement, {
                rotationAngle: (parsedProps.rotationAngle || 0) + (angle - this.rotationDegree),
                rotationWidth: width / 2,
                rotationHeight: height / 2
            })

            this.rotationDegree = angle

            newElements[activeElementIndex] = modifiedElement

            this.props.mutateStore({
                elements: newElements,
                isRotating: true,
                activeRotation:true
            })

        }
    };

    handleChangeComplete = () => {
        this.currentRotatingDims = null
        this.rotationDegree = 0
        this.props.mutateStore({
            isRotating: false,
            activeRotation:false
        })
    };

    rotationDegree = 0
    render() {

        const { degree } = this.state

        return (

            <div className='slider'>
            
                <Slider
                    min={-360}
                    max={360}
                    tooltip={true}
                    value={degree}
                    onChangeStart={this.handleChangeStart}
                    onChange={this.handleChange}
                    onChangeComplete={this.handleChangeComplete}
                />
            </div>

        );
    }
}

const RotateSliderConnected = WithEditor(RotateSlider)
const RotateSliderMutationConnected = MutateEditor(RotateSliderConnected)

export default RotateSliderMutationConnected