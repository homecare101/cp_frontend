import React, { Component } from 'react';
import { graphql } from 'react-apollo'

import QueryConnected from '../../HOC/QueryConnected';
import { updateModel, getEditor } from '../../../actions/editor';

import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { Cases } from '../../../utils/constants';


class ModelDropDown extends Component {
    constructor(props) {
        super(props);
        this.state = {
          dropdownOpen: false,
          modelName : 'iPhone X'
        };
      }
    
      toggle = () => {

        this.setState({
          dropdownOpen: !this.state.dropdownOpen
        });
      }
    
      select = event => {

        this.setState({
          dropdownOpen: !this.state.dropdownOpen,
          modelName: event.target.innerText
        });

        this.props.mutate( { variables : { model : event.target.innerText } })

      }

    render () {

        const {
            data
        } = this.props;

        const {
            editor = {}
        } = data;
        return (
            <div className="ModelDropDown">
                <p>Model</p>
                <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                    <DropdownToggle><span className="modelName">{this.state.modelName} </span><span className='fa fa-angle-down' /></DropdownToggle>
                    <DropdownMenu>
                        {
                           Cases.map((data,index) => {
                               return(
                                    <DropdownItem onClick={this.select} key={index} >{data.model}</DropdownItem>
                               )
                           }) 
                        }
                    </DropdownMenu>
                </ButtonDropdown>
            </div>
        )

    }

}

const ModelDropDownConnected = QueryConnected(getEditor)(ModelDropDown)
const ModelDropDownMutationConnected = graphql(updateModel)(ModelDropDownConnected)

export default ModelDropDownMutationConnected

