import React, { Component } from 'react';
import { graphql, Query } from 'react-apollo'

import QueryConnected from '../../HOC/QueryConnected';
import { updateModel, getEditor, getMaterials } from '../../../actions/editor';

import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { Materials} from '../../../utils/constants';


class MaterialDropDown extends Component {
    constructor(props) {
        super(props);
        this.state = {
          dropdownOpen: false,
          modelName : 'Material#1'
        };
    }
    

    toggle = () => {
        this.setState({
          dropdownOpen: !this.state.dropdownOpen
        });
      }
    
      select = event => {

        this.setState({
          dropdownOpen: !this.state.dropdownOpen,
          modelName: event.target.innerText
        });

      }

    render () {

        const {
            data
        } = this.props;

        const {
            editor = {}
        } = data;
        return (
            <div className="MaterialDropDown">
                <p>Materials</p>
                <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                    <DropdownToggle><span className="modelName">{this.state.modelName} </span><span className='fa fa-angle-down' /></DropdownToggle>
                    <DropdownMenu>
                        <Query query={getMaterials}>
                        {
                            ({data}) => {
                                const { materials = [] } = data
                                return materials.map((item) => {
                                    return <DropdownItem key={item.id} onClick={this.select}>{item.title}</DropdownItem>
                                })
                                
                            }
                        } 
                        </Query>
                    </DropdownMenu>
                </ButtonDropdown>
            </div>
        )

    }

}

const MaterialDropDownConnected = QueryConnected(getEditor)(MaterialDropDown)
const MaterialDropDownMutationConnected = graphql(updateModel)(MaterialDropDownConnected)

export default MaterialDropDownMutationConnected

