import React, { Component } from 'react';
import EditorSelectedTab from '../Editor/EditorSelectedTab';
import { closePopUp } from '../../utils/functions';
class MobilePopupTab extends Component {
    
    render () {

        return (
            <div id="MobilePopupTab" className="MobilePopupTab"> 
                <div className="pull-left mobilepopupclose"  style={{width:'100%',marginBottom:'10px'}} onClick={closePopUp}>
                    <img 
                        src="/blackdelete.png" 
                        height="20" width="20" 
                        className="pull-right" 
                        style={{cursor:'pointer', marginTop:'10px'}}
                        onClick={this.onClosePopUp}
                    />
                </div>
                <EditorSelectedTab />
            </div>
        )

    }

}

export default MobilePopupTab