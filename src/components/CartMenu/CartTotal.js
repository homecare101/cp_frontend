import React from 'react'
import { Button, Input, Form, FormGroup, Container, Row, Col, Alert } from 'reactstrap';
import './cartMenu.css'
import { Link } from 'react-router-dom';

class CartTotal extends React.Component {
    constructor() {
        super()
    }
    render() {
        return (
            <div className="total-back">
                <Container fluid>
                    <Row>
                        <div className="total-heading-out">
                            <p>Cart Total</p>
                        </div>
                    </Row>
                    {/* <Row className="total-text">
                        <p>Subtotal</p>
                        <div><h6>$34</h6></div>
                    </Row> */}
                    <Row className="total-text">
                        <Col md={6} xs={6} className="flex-start nopadding">
                            <p>Subtotal</p>
                            <p>Shippping</p>
                        </Col>
                        <Col md={6} xs={6} className="flex-end">
                            <h6>${this.props.subTotal}</h6>
                            <h6>$14</h6>
                        </Col>
                    </Row>
                    <Row className="deliveryDate">
                        <p>Express delivery 3-5 days</p>
                    </Row>
                    <Col md={12} className="alineCenterBtn nopadding">
                       <Link to="/checkOut"><Button className="checkoutBtn">Proceed to checkout</Button></Link>
                       <Link to="/stock"><Button className="continueBtn">Continue shopping</Button></Link> 
                    </Col>
                    <Col md={12} className="coupon-cart">
                        <Col md={6} className="flex-start nopadding">
                            <Input className="AddCouponBtn" placeholder="Coupon"></Input>
                        </Col>
                        <Col md={6} className="flex-end">
                            <p className="ApplyeBtn">Apply</p>
                        </Col>
                    </Col>
                </Container>
            </div>
        )
    }
}
export default CartTotal