import React, { Component } from 'react';
import './cartMenu.css'
import { Link } from 'react-router-dom'
import { isContext } from 'vm';
import { withRouter } from 'react-router';
import { withApollo, graphql, compose } from 'react-apollo';
import { addToCartMutaion } from '../../Graphql/Mutations/userDetails';
import { checkCartMutation } from '../../Graphql/Mutations/cart';
import { } from 'async';
import { Container, Row, Col, Button, ListGroup, ListGroupItem } from 'reactstrap';
import ReactLoading from 'react-loading';
import { setToast } from '../../actions/editor';

class CartMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showLoader: true,
            data: []
        }
    }
    componentWillMount() {
        let userId = localStorage.getItem('id')
        this.props.createCart({
            variables: {
                userId: userId
            },
        }).then(res => {
            let cartData = res.data.checkCart.items
            this.setState({ data: cartData, showLoader: false })
        })
            .catch(error => {
                console.log(error, "error 	")
                let errorText = { ...error }
                if (errorText !== {}) {
                    this.props.setToast({
                        variables: { type: 'error', message: errorText.graphQLErrors[0].message }
                    })
                }
            })
    }
    onDeleteCart(id) {
        let userID = localStorage.getItem('id')
        let cartData = JSON.parse(localStorage.getItem('cartData'))
        cartData = JSON.parse(JSON.stringify(cartData))
        let cartItems = this.state.data
        let indexCheck = cartItems.map(function (e) { return e._id; }).indexOf(id);
        cartItems.splice(indexCheck, 1)
        let newCart = []
        if (cartItems.length > 0) {
            cartItems.forEach((item, key) => {
                newCart.push({
                    caseData: item.caseData,
                    count: item.count,
                    name: item.name,
                    preview: item.preview,
                    price: item.price,
                    _id: item._id
                })
            });
        }
        this.props.addCart({
            variables: {
                userId: userID,
                subTotal: 0,
                cartId: cartData._id,
                items: newCart
            },
        }).then(res => {
            this.props.setToast({
                variables: { type: 'success', message: "Item Deleted From Cart" }
            })
            cartData.items = newCart
            this.setState({ data: newCart })
            localStorage.setItem('cartData', JSON.stringify(cartData))
        })
            .catch(error => {
                console.log(error)
                let errorText = { ...error }
                if (errorText !== {}) {
                    this.props.setToast({
                        variables: { type: 'error', message: errorText.graphQLErrors[0].message }
                    })
                }
            })
    }
    confirmPayment() {
        this.props.history.push(`/checkOut`)
    }
    render() {
        let subTotal = 0
        return (
            <div className="cartHeight">
                <div className={this.state.showLoader ? "smallLoader" : "smallLoader notActive"}>
                    <ReactLoading type='spin' color='#fff' height={50} width={40} />
                </div>
                <div className="cartCon">
                    {this.state.data.length > 0 ? this.state.data.map((val, index) => {
                        return (
                            <div key={index}>
                                <Row className="cartItemText cartCenter">
                                    <Col md={4} xs={4} className="nopadding">
                                        <img src={val.preview} style={{ width: '45px', marginLeft: '22px' }} />
                                    </Col>
                                    <Col md={6} xs={6} className="nopadding">
                                        <p>{val.name}</p>
                                        <p>{val.count} x $ {val.price}</p>
                                    </Col>
                                    <Col md={2} xs={2} className="nopadding">
                                        <i onClick={this.onDeleteCart.bind(this, val._id)} className="fa fa-close iconCross" ></i>
                                    </Col>
                                </Row>
                                <div className="cartBorder"></div>
                                <p style={{ display: 'none' }}>{
                                    subTotal = subTotal + (val.count * val.price)
                                }</p>
                            </div>
                        )
                    }) : <Col md={12} className="emptytext"><p>Cart is empty</p></Col>}
                </div>

                <div className="cartButtonsFixed">
                    <div className="cartItemPrice">
                        <p>
                        합계 {subTotal}
                        </p>
                    </div>
                    <div className="cartItemPrice">
                        <Link to="/ShowCart"><Button className="buttonMain" block>장바구니</Button></Link>
                    </div>
                    <div className="cartItemPrice">
                        <Button onClick={this.confirmPayment.bind(this)} className="buttonCart" block>결제하기</Button>
                    </div>
                </div>
            </div>
        )
    }
}
export default withRouter(compose(
    withApollo,
    [graphql(checkCartMutation, { name: 'createCart' }),
    graphql(addToCartMutaion, { name: 'addCart' }),
    graphql(setToast, { name: 'setToast' })]
)(CartMenu))