import React, { Component } from 'react'
import { graphql } from 'react-apollo';
import { Link } from 'react-router-dom'
import TabLinkList from '../common/TabLinkList';
import SidebarTabs from './Tabs/SidebarTabs';
import QueryConnected from '../HOC/QueryConnected';

import { getSelectedTab,  setSelectedTab } from '../../actions/editor';
import ModelTab from './Tabs/ModelTab';
import './editorMain.css'

class EditorSidebar extends Component {

    componentDidMount () {
        
        this.props.mutate({ variables: { selectedTabID: SidebarTabs[0].id } });
    }

    onTabSelect = tab => {
        this.props.mutate({ variables: { selectedTabID: tab.id } });
    }

    render () {
        return (
            <div>
                <div className="mainNavlinks">
                    <div className="navlinks">
                        <div className="editorImgOut">
                            <Link to="/">
                                <img src="/logo.png" className="imageAllEditor" />
                            </Link>
                        </div>
                        <div className="editorListMarginTop">
                            {
                                <TabLinkList
                                    tabs={SidebarTabs}
                                    selectedTab={this.props.data.selectedTabID}
                                    onSelect={this.onTabSelect}
                                />
                            }
                        </div>
                    </div>
                </div>
                <ModelTab />
             </div>
        )
    }

}

const EditorSidebarConnected = QueryConnected(getSelectedTab)(EditorSidebar)
const EditorSidebarMutationConnected = graphql(setSelectedTab)(EditorSidebarConnected)

export default EditorSidebarMutationConnected