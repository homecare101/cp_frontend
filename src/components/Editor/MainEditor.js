import React, { Component, Fragment, createRef } from 'react'

import ElementList from './EditorTools/ElementList';
import PhoneCase from './EditorTools/PhoneCase'
import { RenderElementHOC } from '../HOC/RenderElementHOC';
import SelectableLayerContainer from './EditorTools/SelectTools/SelectableLayer/SelectableLayerContainer'
import Collage from './EditorTools/Collage';
import WithEditor from '../HOC/WithEditor';
import MutateEditor from '../HOC/MutateEditor';
import { withRouter } from 'react-router';

// utils
import { partial, calculateAspectRatioFit } from '../../utils/functions';
import {
    updateOrder,
    updateElementProps,
    updateWidthFromLeft,
    updateHeightFromTop,
    updateHeightFromBottom,
    updateWidthFromRight,
    getActiveElementDims,
    getActiveGroupDims,
    convertCoordsToSVGCoords,
    findImageUsingClipPath,
    getElementCenter,
    findDimensions,
    createElement
} from '../../utils/elements';

import { Cases, Collages, API_URL } from '../../utils/constants';
import HeadlessImageUploader from '../common/HeadlessImageUploader';
import { Query, compose, withApollo } from 'react-apollo';
import { getPhoneModel } from '../../actions/editor';
import { uploadPhoto } from '../../Api';

import ReactLoading from 'react-loading';
import { getCaseModelQuery } from '../../Graphql/Query/Cases';
import SaveBtn from '../common/SaveBtn';


class MainEditor extends Component {

    constructor (props) {
        super(props);

        this.state = {
            mobileCases: null
        }

        this.imageUploader = createRef();
    }

    previousMouseCoords = {
        x: 0,
        y: 0
    }

    state = {
        isMouseDown: false,
        selectedEditPath: '',
        collageVisible: true,
        showEditPath: false
    }


    updatePosition = (index, position) => {

        this.previousMouseCoords.x = position.x
        this.previousMouseCoords.y = position.y

        const positionInSVG = this.previousMouseCoords.matrixTransform(this.mainSVG.getScreenCTM().inverse())

        const { elements = [] } = this.props.editor

        const copiedElements = [...elements]

        copiedElements[index] = {
            ...copiedElements[index],
            isRotating: false,
            position: {
                x: positionInSVG.x,
                y: positionInSVG.y,
                __typename: 'ElementPosition'
            }
        }

        this.props.mutateStore({ elements: copiedElements })

    }

    updateOrder = (direction, index) => {

        const { elements } = this.state

        const { updatedELements, updatedIndex } = updateOrder(elements, direction, index)

        this.setState({
            elements: updatedELements,
            activeElementIndex: updatedIndex
        })

    }

    moveFront = partial(this.updateOrder, 'front')
    moveBack = partial(this.updateOrder, 'back')

    onMouseMove = e => {

        let currentMousePointer = convertCoordsToSVGCoords(e.clientX, e.clientY)

        this.props.mutateStore({
            currentMousePointer: {
                x: currentMousePointer.x,
                y: currentMousePointer.y
            }
        })

        const { isMouseDown } = this.state
        const { activeElementIndex, activeElementDims, offset, resizeEdge, elements, isRotating } = this.props.editor

        const selectedElement = elements[activeElementIndex] || {}

        let { position } = selectedElement

        const newElements = [...elements]
        let modifiedElement = selectedElement


        if(isRotating && activeElementIndex !== null) {

            const activeElementBox = document.getElementById('activeChildId').getBBox()

            const { width, height } = activeElementBox

            const centerX = position.x + ( width / 2 )
            const centerY = position.y + ( height / 2 )

            const positionInSVG = convertCoordsToSVGCoords(e.clientX, e.clientY)

            var angle = Math.atan2(positionInSVG.x - centerX, - (positionInSVG.y - centerY) )*(180/Math.PI);

            const parsedProps = JSON.parse(selectedElement.props)

            modifiedElement = updateElementProps(modifiedElement, {
                rotationAngle: parsedProps.rotationAngle + (angle - this.rotationDegree),
                rotationWidth: width / 2,
                rotationHeight: height / 2
            })

            this.rotationDegree = angle

            newElements[activeElementIndex] = modifiedElement

            this.props.mutateStore({
                elements: newElements,
                currentMousePointer: {
                    x: currentMousePointer.x,
                    y: currentMousePointer.y
                }
            })

            return

        }

        if(resizeEdge && isMouseDown) {

            let { width, height } = activeElementDims
            const Dims = getActiveElementDims()

            if (Dims.width) {
                width = Dims.width;
                height = Dims.height
            }

            let parsedProps = JSON.parse(selectedElement.props)

            const resizeCoordsInSvg = convertCoordsToSVGCoords(this.previousResizeCoords.x, this.previousResizeCoords.y)
            const newCoordsInSvg = convertCoordsToSVGCoords(e.clientX, e.clientY)

            this.previousResizeCoords.x = e.clientX
            this.previousResizeCoords.y = e.clientY

            if (selectedElement.type === 'text') {

                let { style } = parsedProps

                const newFontSizeFromLeft = updateWidthFromLeft(resizeCoordsInSvg, newCoordsInSvg, style.fontSize)
                const newFontSizeFromTop = updateHeightFromTop(resizeCoordsInSvg, newCoordsInSvg, style.fontSize)
                const newFontSizeFromRight = updateWidthFromRight(resizeCoordsInSvg, newCoordsInSvg, style.fontSize)
                const newFontSizeFromBottom = updateHeightFromBottom(resizeCoordsInSvg, newCoordsInSvg, style.fontSize)

                switch (resizeEdge) {

                    case 'topLeft':

                        position = {
                            x: newCoordsInSvg.x + 5,
                            y: newCoordsInSvg.y + height
                        }
                            
                        style.fontSize = newFontSizeFromLeft
                        style.fontSize = newFontSizeFromTop
    
                        break;
                    
                    case 'bottomLeft':

                        position = {
                            x: position.x + 5 + ( newCoordsInSvg.x - position.x ), 
                            y: newCoordsInSvg.y - 10
                        }
    
                        style.fontSize = newFontSizeFromLeft
                        style.fontSize = newFontSizeFromBottom
    
                        break;
                    
                    case 'topRight':

                        position = {
                            x: position.x, 
                            y: position.y + ( newCoordsInSvg.y - position.y ) + height
                        }

                        style.fontSize = newFontSizeFromRight
                        style.fontSize = newFontSizeFromTop
    
                        break;
                    
                    case 'bottomRight': 
    
                        style.fontSize = newFontSizeFromRight
                        style.fontSize = newFontSizeFromBottom
    
                        break
    
                }

                parsedProps = {
                    ...parsedProps,
                    style
                }
                
            }

            if (selectedElement.type === 'image') {

                // because resize having issue with rotated images

                width = activeElementDims.width,
                height = activeElementDims.height

                const newWidthFromLeft = updateWidthFromLeft(resizeCoordsInSvg, newCoordsInSvg, width)
                const newHeightFromTop = updateHeightFromTop(resizeCoordsInSvg, newCoordsInSvg, height)
                const newWidthFromRight = updateWidthFromRight(resizeCoordsInSvg, newCoordsInSvg, width)
                const newHeightFromBottom = updateHeightFromBottom(resizeCoordsInSvg, newCoordsInSvg, height)

                switch (resizeEdge) {

                    case 'topLeft':
    
                        position = {
                            x: newCoordsInSvg.x + 5,
                            y: newCoordsInSvg.y
                        }
                            
                        width = newWidthFromLeft
                        height = newHeightFromTop
    
                        break;
                    
                    case 'bottomLeft': 
                        
                        position = {
                            x: position.x + 5 + ( newCoordsInSvg.x - position.x ), 
                            y: position.y
                        }
    
                        width = newWidthFromLeft
                        height = newHeightFromBottom
    
                        break;
                    
                    case 'topRight': 
                        
                        position = {
                            x: position.x, 
                            y: position.y + ( newCoordsInSvg.y - position.y )
                        }

                        width = newWidthFromRight
                        height = newHeightFromTop
    
                        break;
                    
                    case 'bottomRight': 
    
                        width = newWidthFromRight
                        height = newHeightFromBottom
    
                        break;
    
                }
            }

            // update position

            this.updatePosition(activeElementIndex, position)

            switch (selectedElement.type) {

                case 'image':
                    modifiedElement = updateElementProps(selectedElement, {
                        width,
                        height,
                        rotationWidth: width / 2,
                        rotationHeight: height / 2
                    })
                    break;
                case 'text':
                    modifiedElement = updateElementProps(selectedElement, parsedProps)
                    break;

            }

            newElements[activeElementIndex] = {
                ...modifiedElement,
                position: {
                    ...position,
                    __typename: 'ElementPosition'
                }
            }

            this.props.mutateStore({
                elements: newElements,
                activeElementDims: {
                    width,
                    height,
                    __typename: 'activeElementDims'
                }
            })
            
            return

        }

        if (isMouseDown && activeElementIndex !== null) {

            this.updatePosition(activeElementIndex, {
                x: e.clientX - offset.x,
                y: e.clientY - offset.y
            })
        }

    }

    onMouseDown = e => {
        this.setState({
            isMouseDown: true
        })
    }

    onMouseUp = e => {
        this.setState({
            isMouseDown: false,
        })

        this.props.mutateStore({
            resizeEdge: '',
            isRotating: false
        })
    }

    changeSvgBackgroundColor = color => {
        
        this.setState({ svgBackgroundColor: color.rgb });

    };

    onSvgClick = e => {

        if ( e.target.id === 'svg-container' || e.target.id === 'shadowImage' ) {
            this.onCancelPath()
        }

    }

    toggleCollageVisibility = collageVisible => this.setState({
        collageVisible
    })

    openUploader = selectedPath => {

        this.imageUploader.current.open()

        this.setState({
            selectedPath
        })

    }

    onSelectedpath = path => {

        this.setState({
            selectedEditPath: ''
        })

        this.props.mutateStore({
            activeElementIndex: null,
            selectedPath: path.id
        })

    }
    
    onCancelPath = () => {
        this.setState({
            selectedEditPath: '',
            selectedPath: null
        })

        this.props.mutateStore({
            activeElementIndex: null,
            selectedPath: ''
        }) 
        
    }

    onEditCollagePath = path => {

        let existingImageIndex = findImageUsingClipPath(path.id, this.props.editor.elements)

        if ( existingImageIndex > -1 ) {
            this.props.mutateStore({
                activeElementIndex: existingImageIndex
            })
        }
        
        this.setState({
            selectedEditPath: path.id
        })

    }

    renderSelectLayer = () => {

        const {
            editor
        } = this.props;

        const {
            selectedEditPath,
        } = this.state;

        return <SelectableLayerContainer
            selectedEditPath={selectedEditPath}
        />

    }

    onImageUpload = files => {

        const { elements } = this.props.editor
        const { selectedPath = {} } = this.state

        let existingImageIndex = findImageUsingClipPath(selectedPath.id, elements)

        let modifiedElements = elements;

        if ( existingImageIndex > -1 ) {
            modifiedElements = modifiedElements.filter(
                (element, index) => index !== existingImageIndex
            )

            this.props.mutateStore({
                elements: modifiedElements
            })
        }

        files.forEach(
            ({ width, height, file }) => {

                uploadPhoto(file)
                    .then(
                        ({ filename }) => {

                            const dataUrl = `${API_URL}/uploads/${filename}`

                            const selectedPathNode = document.getElementById(`collage-path-${selectedPath.id}`)

                            const pathDims = findDimensions(selectedPathNode)

                            const dims = calculateAspectRatioFit(width, height, pathDims.width, pathDims.height)

                            let position = {
                                x: 0,
                                y: 0
                            }

                            const pathCenter = getElementCenter(selectedPathNode)

                            position = {
                                x: pathCenter.x - dims.width / 2,
                                y: pathCenter.y - dims.height / 2,
                            }

                            const element = createElement({xlinkHref: dataUrl, width: dims.width, height: dims.height, clipPath: selectedPath.id}, position, 'image')

                            let newElements = modifiedElements.concat(element)

                            this.props.mutateStore({
                                elements: newElements,
                                activeElementIndex: (newElements.length - 1),
                                activeElementDims: {
                                    width: dims.width,
                                    height: dims.height,
                                    __typename: 'activeElementDims'
                                }
                            })

                            this.setState({
                                selectedEditPath: selectedPath.id
                            })
                        }
                    )

            } 
        )

    }

    previousDims = {
        x: 0,
        y: 0
    }

    componentDidMount () {

        this.props.client.query({
            query: getCaseModelQuery,
            variables: {},
        }).then(res => {


            this.setState({ mobileCases : res.data.getCaseModel.list })
        })
        .catch(error => {
            console.log(error, "error")
        })

        
        this.previousMouseCoords = this.mainSVG.createSVGPoint()
        this.previousResizeCoords = this.mainSVG.createSVGPoint()

        this.mainSVG.addEventListener('touchstart', (e) => {
            this.onMouseDown(e)
        })
        this.mainSVG.addEventListener('touchend', () => {
            this.onMouseUp()
        })
        this.mainSVG.addEventListener('touchmove', e => {
            this.onMouseMove(e.changedTouches[0])
        })

        if (this.props.editor.collage) {

            const selectedCollage = Collages.find(collage => collage.id == this.props.editor.collage)

            this.setState({
                selectedCollage
            })
        }
    }

    componentDidUpdate (prevProps) {

        // const newDims = getActiveElementDims()

        // if (newDims.x != this.previousDims.x || newDims.y != this.previousDims.y) {

        //     console.log(newDims.x, this.previousDims.x, newDims.y, this.previousDims.y)

        //     const { activeElementIndex } = this.props.editor

        //     this.updatePosition(activeElementIndex, {
        //         x: newDims.x,
        //         y: newDims.y
        //     })

        //     this.previousDims = newDims

        // }

    }

    componentWillReceiveProps (nextProps) {

        const { editor } = nextProps
        if(!this.props.editor.resizeEdge && editor.resizeEdge) {

            this.previousResizeCoords.x = editor.resizeCoords.x
            this.previousResizeCoords.y = editor.resizeCoords.y

        }

        if(!this.props.editor.removeExternalLayers && editor.removeExternalLayers) {
            this.setState({collageVisible : !editor.removeExternalLayers})
        }

        if(!this.props.editor.isRotating && nextProps.editor.isRotating) {

            const Dims = getActiveElementDims()

            this.currentRotatingDims = Dims
            this.rotationDegree = 0

        }

        if(this.props.editor.isRotating && !nextProps.editor.isRotating) {

            this.currentRotatingDims = null
            this.rotationDegree = 0
        
        }

        if (editor.collage !== this.props.editor.collage) {
            const selectedCollage = Collages.find(collage => collage.id == editor.collage)

            this.setState({
                selectedCollage: null
            }, () => {
                this.setState({
                    selectedCollage,
                    collageVisible: true
                })
            })
        }


    }

    currentRotatingDims = null
    rotationDegree = 0


    render() {
        
        const {
            editor
        } = this.props;

        const {
            elements = [],
            height,
            width,
            activeElementIndex,
            model,
        } = editor;

        const {
            selectedCollage = {},
            collageVisible
        } = this.state

        // const { cases = [] } = this.props

        let { mobileCases } = this.state

        const modelFind = mobileCases && mobileCases.find(data => data.model == model) || {}

        let showCollage = collageVisible

        // because activeElementIndex can be 0
        if ( activeElementIndex != null ) {
            showCollage = true
        }
        return (
            <Fragment>
                <div id={'loader'} className="loader">
                    <ReactLoading type='spin' color='#fff' height={60} width={60} />
                </div>
                <div className="savebtnEditor mobileview">
                <SaveBtn  />
                </div>

                
                <svg
                    ref={c => this.mainSVG = c}
                    width={'100%'}
                    height={'100%'}
                    onMouseMove={this.onMouseMove}
                    onMouseDown={this.onMouseDown}
                    onMouseUp={this.onMouseUp}
                    onClick={this.onSvgClick}
                    className="svgbg"
                    id="svg-container"
                    xmlnsXlink="http://www.w3.org/1999/xlink"
                    xmlns="http://www.w3.org/2000/svg"
                    version="1.1"
                >
                    <PhoneCase
                        phoneModel={modelFind}
                        backgroundColor={editor.backgroundColor || "#fff"}
                        isPreviewMode={this.state.isPreviewMode}
                        activeElementIndex={activeElementIndex}
                    >
                        <ElementList 
                            elements={elements}
                            componentRenderer={RenderElementHOC}
                            activeElementIndex={activeElementIndex}
                        />
                    </PhoneCase>
                    { selectedCollage && selectedCollage.paths && selectedCollage.paths.length ?
                        <Collage
                            openUploader={this.openUploader}
                            collage={selectedCollage}
                            onEditPath={this.onEditCollagePath}
                            visibility={showCollage}
                            onSelectedpath={this.onSelectedpath}
                            toggleVisibility={this.toggleCollageVisibility}
                            onCancelPath={this.onCancelPath}
                        />
                    : null}
                    {this.renderSelectLayer()}
                </svg>
                <HeadlessImageUploader
                    forwardRef={this.imageUploader}
                    onImageUpload={this.onImageUpload}
                />
            </Fragment>
        );
    }
}

const MainEditorConnected = WithEditor(MainEditor)
const MainEditorMutationConnected = MutateEditor(MainEditorConnected)


export default withRouter(compose(
    withApollo)(MainEditorMutationConnected));

// props => {
    // return <Query query={getCaseModelQuery}>
    //     {
    //         ({data}) => <MainEditorMutationConnected cases={data.cases} {...props} />
    //     }
    // </Query>
 //}