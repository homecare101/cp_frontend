import React from 'react';

import { Tooltip, UncontrolledTooltip } from 'reactstrap';
import DelementElementHOC from '../HOC/DeleteElementHOC';
import UploadImageHOC from '../HOC/UploadImageHOC'
import WithEditor from '../HOC/WithEditor';
import MutateEditor from '../HOC/MutateEditor';

const DeleteConnectedIcon = DelementElementHOC(
  props => <span
    onClick={props.removeElement}
    style={{ cursor: 'pointer', width: "26px",  borderRadius: "4px", backgroundColor: "#9b9b9b", margin: "0 20px", float: 'right' }}
    id="DisabledAutoHideExample">
    <i class="fa fa-trash-o fa-lg" style={{ color: "#fff" }}></i>
  </span>
)

const UploadImageIcon = UploadImageHOC(
  props => <span
    style={{ cursor: 'pointer', width: "26px", borderRadius: "4px", backgroundColor: "#9b9b9b", float: 'right' }} id="UncontrolledTooltipExample">
    {/*<i class="fa fa-download" style={{ color: "#fff" }}></i>*/}
     <img 
                        src="/uploads.png" 
                        height="15" width="15" 
                        
                    />
  </span>
)

class TabsButton extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      tooltipOpen: false,
    };
  }

  toggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    });
  }
  render() {
    const { activeElementIndex } = this.props.editor
    return (

      <div className="rotatetext">
        <div style={
          activeElementIndex !== null ? {} : { display: 'none' }
        }>
          <DeleteConnectedIcon />
          <Tooltip placement="top" arrowClassName={'tooltip-arrow'} isOpen={this.state.tooltipOpen} autohide={false} style={{ background: "#fff", padding: "12px 20px" }} target="DisabledAutoHideExample" toggle={this.toggle}>
            <h4 className="imageTooltip">Delete</h4>
          </Tooltip>
        </div>
        <div>
          <UploadImageIcon />
          <UncontrolledTooltip placement="top" autohide={false} arrowClassName={'tooltip-arrow'}  target="UncontrolledTooltipExample" id="test" style={{ background: "#fff", padding: "12px 20px" }}>
            <h4 className="imageTooltip">Upload new image</h4>
          </UncontrolledTooltip>
        </div>
      </div>

    );
  }
}



const TabsButtonConnected = WithEditor(TabsButton)
const TabsButtonMutationConnected = MutateEditor(TabsButtonConnected)

export default TabsButtonMutationConnected
