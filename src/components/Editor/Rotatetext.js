import React from 'react';
import TabsButton from './TabsButton'
import RotateSlider from '../common/RotateSlider';
import ResizeSlider from './EditorTools/ResizeSlider';
import WithEditor from '../HOC/WithEditor';
import MutateEditor from '../HOC/MutateEditor';


class Rotatetext extends React.Component {
  render() {

    const { activeElementIndex } = this.props.editor

    return (

      <div className={`rotatetext ${this.props.className}`}>
      {
        activeElementIndex !== null ?

        <div>
          <div className="customslider">
            <div className="customslider1">
              <span>이미지 회전 </span><RotateSlider />
            </div>
            <div className="customslider1">
              <span>크기 조정 </span><ResizeSlider />
            </div>
          </div>
          <TabsButton />
        </div>
        :''
      }
        
      </div>

    );
  }
}

const RotatetextConnected = WithEditor(Rotatetext)
const RotatetextMutationConnected = MutateEditor(RotatetextConnected)

export default RotatetextMutationConnected