import React, { Component } from 'react';
import { Query } from 'react-apollo';

import SidebarTabs from './Tabs/SidebarTabs';

import { getSelectedTab } from '../../actions/editor';

class EditorSelectedTab extends Component {

    render () {

        return <Query query={getSelectedTab}>
            {({ loading, error, data }) => {

                let SelectedTab = SidebarTabs.find(tab => data.selectedTabID === tab.id)

                let ComponentToRender = () => null
                
                if (SelectedTab) {
                    ComponentToRender = SelectedTab.component
                }

                return <div className="renderedTab full-height">
                    <ComponentToRender />
                </div>
            }}
        </Query>

    }

}

export default EditorSelectedTab;