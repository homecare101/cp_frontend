import React, { Component } from 'react'
import { graphql } from 'react-apollo';

import SelectableLayer from './SelectableLayer';
import WithEditor from '../../../../HOC/WithEditor';

import { getEditor, updateSelectable } from '../../../../../actions/editor';
import { findDimensions } from '../../../../../utils/elements';

class SelectableLayerContainer extends Component {

    onMovableActive = index => e => {

        const { editor } = this.props

        const { elements } = editor;

        const { height, width } = findDimensions(e.target)

        const filteredElements = this.filterElements()

        const selectedElement = filteredElements[index]

        const selectedElementIndex = elements.findIndex(element => selectedElement.id == element.id )

        this.props.mutate({ variables: {
            activeElementIndex: selectedElementIndex,
            activeElementDims: {
                height, width
            }
        } })
    }
 
    onMovableDown = (offset, dims) => {

        const { height, width } = dims

        this.props.mutate({ variables: {
            offset,
            activeElementDims: { height, width }
        } })

    }

    filterElements = () => {

        const {
            editor,
            selectedEditPath
        } = this.props

        const {
            elements
        } = editor

        let newElements = elements

        if ( editor.collage ) {

            newElements = elements.filter(
                element => {
                
                    if (element.type === 'image') {

                        const parsedProps = JSON.parse(element.props)

                        if ( parsedProps.clipPath ) {

                            if ( selectedEditPath === parsedProps.clipPath ) {

                                return element

                            } else {
                                return false
                            }

                        } else {
                            return element
                        }

                    }

                    return element

                }
            )

        }

        return newElements
    }

    render () {
        
        const {
            editor,
            selectedEditPath
        } = this.props

        const {
            elements,
            activeElementIndex,
            activeElementDims,
            resizeEdge
        } = editor

        const selectedElement = elements[activeElementIndex] || {}

        let showRectangle = activeElementIndex !== null

        if ( editor.collage &&  selectedElement.type === 'image' ) {

            const parsedProps = JSON.parse(selectedElement.props)

            if ( parsedProps.clipPath ) {

                if ( selectedEditPath !== parsedProps.clipPath ) {

                    showRectangle = false

                }

            }

        }

        const filteredElements = this.filterElements()

        return <SelectableLayer
            position={selectedElement.position}
            width={activeElementDims.width}
            height={activeElementDims.height}
            selectedElementType={selectedElement.type}
            elements={filteredElements}
            onMovableActive={this.onMovableActive}
            onMovableDown={this.onMovableDown}
            activeElementIndex={activeElementIndex}
            showRectangle={showRectangle}
            collageId={editor.collage}
        />

    }

}

const SelectableLayerContainerConnected = WithEditor(SelectableLayerContainer)
const SelectableLayerContainerMutationConnected = graphql(updateSelectable)(SelectableLayerContainerConnected)

export default SelectableLayerContainerMutationConnected