import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'

import { defaultPosition } from '../../../../../utils/constants'

import ElementList from '../../ElementList';
import { RenderMovableElementHOC } from '../../../../HOC/RenderElementHOC';
import SelectRectangleContainer from '../SelectRectangle/SelecteRactangleContainer';
import { convertCoordsToSVGCoords } from '../../../../../utils/elements';


export default class SelectableLayer extends Component {
    
    state = {
        hover: {
            x: 0,
            y: 0,
            height: 0,
            width: 0
        },
        previousMouseOver: null
    }

    setMouseOverTimeout = null

    onElementHover = index => (position, dims) => {

        const { elements } = this.props

        if ( index == this.state.previousMouseOver ) {
            return;
        }

        let newPosition = convertCoordsToSVGCoords(dims.x, dims.y)

        const selectedElement = elements[index] || {}

        // if(selectedElement.type == 'text') {

        //     newPosition = {
        //         x: position.x,
        //         y: position.y - dims.height
        //     }

        // }

        this.setState({
            hover: {
                x: newPosition.x,
                y: newPosition.y,
                width: dims.width,
                height: dims.height
            },
            previousMouseOver: index
        })

    }

    onElementMouseOut = e => {
        this.setState({
            previousMouseOver: null
        })
    }

    componentDidMount () {
        document.getElementById('svg-container').addEventListener('click', this.onElementMouseOut)
    }

    componentWillUnmount () {
        document.getElementById('svg-container').removeEventListener('click', this.onElementMouseOut)
    }

    render () {

        const {
            activeElementIndex,
            elements,
            showRectangle,
            collageId
        } = this.props

        const {
            hover
        } = this.state;

        const selectedStyle = {
            fill: "transparent",
            strokeWidth: 1,
            stroke: "rgb(151, 151, 151)"
        }

        return (
            <Fragment>
                {   
                    !collageId && activeElementIndex !== this.state.previousMouseOver && this.state.previousMouseOver !== null ?
                        <g transform={`translate(${hover.x - 15}, ${hover.y - 15})`}>
                            <rect
                                onMouseLeave={this.onElementMouseOut}
                                width={hover.width + 30}
                                height={hover.height + 30}
                                style={selectedStyle}
                            />
                        </g>
                    : null
                }
                {
                    showRectangle ?
                        <SelectRectangleContainer />
                    : null
                }
                <ElementList
                    hideElements
                    isSelectable={true}
                    elements={elements}
                    onMovableActive={this.props.onMovableActive}
                    componentRenderer={RenderMovableElementHOC}
                    onElementHover={this.onElementHover}
                    componentProps={{
                        onMovableDown: this.props.onMovableDown,
                        onElementMouseOut: this.onElementMouseOut,
                    }}
                />
            </Fragment>
        )

    }

}

SelectableLayer.defaultProps = {
    position: defaultPosition,
    width: 0,
    height: 0,
    selectedElementType: '',
    onRemove: () => {}
}

SelectableLayer.propTypes = {
    position: PropTypes.object,
    width: PropTypes.number,
    height: PropTypes.number,
    selectedElementType: PropTypes.string,
    onRemove: PropTypes.function
}