import React, { Component, Fragment } from 'react';
import { graphql } from 'react-apollo';

import SelectRectangle from './SelectRectangle';
import WithEditor from '../../../../HOC/WithEditor';

import { updateResizeMode } from '../../../../../actions/editor';
import { getActiveGroupDims, convertCoordsToSVGCoords } from '../../../../../utils/elements';

class SelectRectangleContainer extends Component {

    onEdgeClick = resizeEdge => e => this.props.mutate({
        variables: {
            resizeCoords: {
                x: e.clientX,
                y: e.clientY,
                __typename: 'resizeCoords'
            },
            resizeEdge
        }
    }) 

    disableResize = () => this.props.mutate({
        variables: {
            resizeCoords: {
                x: 0,
                y: 0,
                __typename: 'resizeCoords'
            },
            resizeEdge: ''
        }
    })

    render () {

        const {
            elements = [],
            activeElementIndex,
            activeElementDims,
            currentMousePointer,
            isRotating,
            activeRotation
        } = this.props.editor

        const selectedElement = elements[activeElementIndex] || {}

        let {
            height,
            width
        } = activeElementDims

        const groupDims = getActiveGroupDims()

        let activeGroupPositionInSVG = selectedElement.position

        if (groupDims.width) {

            activeGroupPositionInSVG = convertCoordsToSVGCoords(groupDims.x, groupDims.y)
            
            width = groupDims.width
            height = groupDims.height

        }

        const selectedStyle = {
            fill: "transparent",
            strokeWidth: 2,
            stroke: "black"
        }

        const centerX = activeGroupPositionInSVG.x + ( width / 2 )
        const centerY = activeGroupPositionInSVG.y + ( height / 2 )

        return (
            <Fragment>
                { isRotating && !activeRotation?
                    <line x1={currentMousePointer.x} y1={currentMousePointer.y} x2={centerX} y2={centerY} stroke="#9b9b9b" />
                : null }
                <SelectRectangle
                    style={{ opacity: isRotating ? 0 : 1}}
                    position={activeGroupPositionInSVG}
                    width={groupDims.width}
                    height={groupDims.height}
                    selectedElement={selectedElement}
                    selectedStyle={selectedStyle}
                    onEdgeClick={this.onEdgeClick}
                    disableResize={this.disableResize}
                />
            </Fragment>
        )
    }

}

const SelectRectangleContainerConnected = WithEditor(SelectRectangleContainer)
const SelectRectangleContainerMutationConnected = graphql(updateResizeMode)(SelectRectangleContainerConnected)
export default SelectRectangleContainerMutationConnected
