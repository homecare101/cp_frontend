import React, { Component, Fragment } from 'react';
import DelementElementHOC from '../../../../HOC/DeleteElementHOC';

class RemoveIcon extends Component {

    render () {
        return (
            <Fragment>
                <g onClick={this.props.removeElement} transform={this.props.transform} style={{cursor:'pointer'}}>
                <rect width="20" height="20" transform={`translate(${ -5}, ${-5})`} style={{fill:'grey',cursor:'pointer'}} rx="5" ry="5"></rect>
                    <image xlinkHref='/rubbish-bin.png' alt='delete' height='10' width='10'/>
                </g>
            </Fragment>
        )

    }

}

export default DelementElementHOC(RemoveIcon)