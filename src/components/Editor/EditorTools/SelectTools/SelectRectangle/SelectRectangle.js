import React, { Component } from 'react';

import RemoveIcon from './RemoveIcon';

import { getEdgeCordinates, toSvgPosition } from '../../../../../utils/elements';
import RotateIconAndLine from './RotateIconAndLine';

export default class SelectRectangle extends Component {

    render () {

        const {
            width,
            height,
            position,
            selectedStyle,
            onEdgeClick,
            disableResize,
        } = this.props

        const edgeCoordinates = getEdgeCordinates(width, height)

        let selectBorderPosition = {
            x: position.x - 5,
            y: position.y - 5
        }

        let pointsToRender = []

        for ( let edge in edgeCoordinates ) {

            pointsToRender.push(
                <g key={edge} transform={toSvgPosition(edgeCoordinates[edge])}>
                    <rect
                        width={20}
                        height={20}
                        style={{...selectedStyle, fill: '#585858',stroke: '#585858', cursor: edgeCoordinates[edge].cursor}}
                        onTouchStart={e => onEdgeClick(edge)(e.changedTouches[0])}
                        onMouseDown={onEdgeClick(edge)}
                        onTouchEnd={disableResize}
                        onMouseUp={disableResize}
                    />
                </g>
            )

        }

        const reloadIconWidth = 30

        const reloadIconCoords = {
            x: (width / 2) - (reloadIconWidth / 2),
            y: (reloadIconWidth * -3)
        }

        const line = {
            x1: (width / 2),
            y1: 0,
            x2: reloadIconCoords.x + (reloadIconWidth / 2),
            y2: reloadIconCoords.y
        }

        return (
            <g style={this.props.style} transform={`translate(${selectBorderPosition.x}, ${selectBorderPosition.y })`} >
                
                <RotateIconAndLine
                    lineCoords={line}
                    reloadIconCoords={reloadIconCoords}
                    reloadIconWidth={reloadIconWidth}
                />

                <rect
                    id="select-rectangle"
                    width={width + 10}
                    height={height + 10}
                    style={{...selectedStyle, strokeWidth: '1',stroke: '#979797', cursor: 'move'}}
                />
                {pointsToRender}
                <RemoveIcon transform={`translate(${ -6}, ${40})`}/>
            </g>
        )

    }

}