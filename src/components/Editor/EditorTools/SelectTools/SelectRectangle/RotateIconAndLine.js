import React, { Component, Fragment } from 'react';
import MutateEditor from '../../../../HOC/MutateEditor';

class RotateIconAndLine extends Component {

    onRotateClick = () => {
        this.props.mutateStore({
            isRotating: true
        })
    }

    render () {

        const {
            lineCoords = {},
            reloadIconCoords = {},
            reloadIconWidth = {}
        } = this.props

        return (
            <Fragment>
                <line x1={lineCoords.x1} y1={lineCoords.y1} x2={lineCoords.x2} y2={lineCoords.y2} stroke="#9b9b9b" />
                <g onTouchStart={this.onRotateClick} onMouseDown={this.onRotateClick} transform={`translate(${reloadIconCoords.x}, ${reloadIconCoords.y})`} style={{stroke:'#9b9b9b'}}>
                    <image class="rotateIcon" height={reloadIconWidth} width={reloadIconWidth} xlinkHref="/reload.svg" />
                </g>
            </Fragment>
        )

    }

}

export default MutateEditor(RotateIconAndLine)