import React from 'react';
import ReactDOMServer from 'react-dom/server'
import PropTypes from 'prop-types';

import { activeElementGroupID, activeElementID } from '../../../utils/constants';

const ElementList = ({elements, onMovableActive, hideElements, componentRenderer, activeElementIndex, onElementHover = () => {}, componentProps}) => {

	return (
			
        elements.map(
            (element, index) => {
                const { props, position } = element
                
                let parsedProps = JSON.parse(props)

                let parentId = index == activeElementIndex ? activeElementGroupID : '';

                parsedProps = {
                    ...parsedProps,
                    id: index == activeElementIndex ? activeElementID : '',
                    style: {
                        ...(parsedProps.style || {}),
                        opacity: hideElements ? 0 : 1
                    }
                }

                switch ( element.type ) {
                    case 'text':
                        let textRotation = parsedProps.rotationWidth || 0;

                        switch (parsedProps.textAnchor) {
                            case 'middle':
                                textRotation = 0;
                                break;
                            case 'start':
                                textRotation = textRotation;
                                break;
                            case 'end':
                                textRotation = 0 - textRotation;
                                break;
                        }

                        parsedProps = {
                            ...parsedProps,
                            transform: `${parsedProps.transform || ''} rotate(${parsedProps.rotationAngle || 0} ${textRotation} ${parsedProps.rotationHeight || 0})`,
                            textAnchor: parsedProps.textAnchor,
                        }
                        break;
                    case 'image':
                        let { transform, rotationAngle = 0, rotationWidth = 0, rotationHeight = 0 } = parsedProps

                        transform = `${transform || ''} rotate(${rotationAngle} ${rotationWidth} ${rotationHeight})`
            
                        const newProps = {
                            ...parsedProps,
                            transform
                        }
            
                        parsedProps = {
                            ...parsedProps,
                            htmlToRender: ReactDOMServer.renderToStaticMarkup(<image {...newProps} />)
                        }

                        break;
                        
                }


                const ComponentToRender = componentRenderer({...element, props: parsedProps})
                
                const PropsToComponent = {
                    ...parsedProps,
                    ...componentProps,
                    position
                }

                return (
                    <ComponentToRender
                        key={index}
                        onMovableActive={onMovableActive(index)}
                        onElementHover={onElementHover(index)}
                        parentId={parentId}
                        {...PropsToComponent}
                    />
                )
            }
        )
	)
}

ElementList.propTypes = {
    elements:PropTypes.array.isRequired,
    onMovableActive:PropTypes.func.isRequired,
    onMovableDown:PropTypes.func.isRequired
}

ElementList.defaultProps = {
    elements:[],
    onMovableActive:() => {},
    onMovableDown:() => {}
}

export default ElementList;