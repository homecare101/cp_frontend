import React, { Component } from 'react'
import Dropzone from 'react-dropzone'

import { fileToDataUrl } from '../../../../utils/elements'
import WithRefHOC from '../../../HOC/WithRefHOC';

const DropzoneWithRef = WithRefHOC(Dropzone)

export default class ImageUploader extends Component {

    onImageUpload = (files) => {

        if(files && files.length) {

            const file = files[0]

            const filePromises = files.map(
                file => fileToDataUrl(file)
            )

            Promise.all(filePromises)
                .then(
                    filesWithDataUrl => {
                        this.props.onImageUpload(filesWithDataUrl)
                    }
                )

        }

    }

    render () {

        const {
            children,
            forwardRef,
            style,
            ...restProps
        } = this.props

        return (
            <DropzoneWithRef style={{...style, maxHeight: 0}} {...restProps} ref={forwardRef} onDrop={this.onImageUpload}>
                {children}
            </DropzoneWithRef>
        )

    }

}

ImageUploader.defaultProps = {
    onImageUpload: () => {}
}