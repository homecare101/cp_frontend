import React from 'react';

import Slider from 'react-rangeslider'
import { updateElementProps, getActiveGroupDims } from '../../../utils/elements';
import WithEditor from '../../HOC/WithEditor';
import MutateEditor from '../../HOC/MutateEditor';


class ResizeSlider extends React.Component {
    constructor(props, context) {
        super(props, context)
        this.state = {
            ratio: 0,
        }
    }

    handleChange = ratio => {

        const prevRatio = this.state.ratio

        this.setState({
            ratio
        })

        const { activeElementIndex, elements } = this.props.editor

        const selectedElement = elements[activeElementIndex] || {}


        const newElements = [...elements]

        let parsedProps = JSON.parse(selectedElement.props)

        let modifiedElement = selectedElement

        if (selectedElement.type == 'image') {
            modifiedElement = updateElementProps(selectedElement, {
                width: parsedProps.width + (ratio - prevRatio),
                height: parsedProps.height + (ratio - prevRatio)
            })
    
        }

        if (selectedElement.type == 'text') {

            let { style } = parsedProps

            style.fontSize = style.fontSize + (ratio - prevRatio)

            parsedProps = {
                ...parsedProps,
                style
            }

            modifiedElement = updateElementProps(selectedElement, parsedProps)

        }

        newElements[activeElementIndex] = {
            ...modifiedElement
        }

        this.props.mutateStore({
            elements: newElements
        })

    };

    render() {

        const { ratio } = this.state

        return (

            <div className='slider'>
                <Slider
                    min={-100}
                    max={100}
                    tooltip={true}
                    value={ratio}
                    onChangeStart={this.handleChangeStart}
                    onChange={this.handleChange}
                    onChangeComplete={this.handleChangeComplete}
                />
            </div>

        );
    }
}

const ResizeSliderConnected = WithEditor(ResizeSlider)
const ResizeSliderMutationConnected = MutateEditor(ResizeSliderConnected)

export default ResizeSliderMutationConnected