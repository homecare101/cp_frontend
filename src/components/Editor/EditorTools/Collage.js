import React, { Component, Fragment } from 'react';
import { findDimensions, toSvgPosition, getSVGCenter, convertCoordsToSVGCoords, getSVGContainerDims, isElementExist, getScale } from '../../../utils/elements';
import AutoDimensions from '../../HOC/AutoDimensions';
import WithEditor from '../../HOC/WithEditor';
import MutateEditor from '../../HOC/MutateEditor';
import { partial } from '../../../utils/functions';

class Collage extends Component {

    state = {
        position: {
            x: 0,
            y: 0
        },
        selectedDims: {
            x: 0,
            y: 0
        },
        selectedHoverDims: null,
        isEditing: false
    }

    updateCollageDims = () => {

        const collageContainer = document.getElementById('collage-container')

        const collageDims = findDimensions(collageContainer)

        const svgSize = getSVGContainerDims()

        const svgPositon = {
            x: svgSize.width / 2 - collageDims.width / 2 - 30,
            y: svgSize.height / 2 - collageDims.height / 2 - 36
        }

        this.setState({
            position: svgPositon
        })

    }

    displayHoverBox = dims => {

        let convertedCords = convertCoordsToSVGCoords(dims.x, dims.y)

        const { position } = this.state;

        convertedCords = {
            x: convertedCords.x + position.x,
            y: convertedCords.y + position.y
        }

        this.setState({
            selectedHoverDims: {
                ...convertedCords,
                width: dims.width,
                height: dims.height
            }
        })

    }

    hideHoverBox = () => {

        this.setState({
            selectedHoverDims: null
        })

    }

    onSelectedpath = (path, dims) => {

        const { selectedPath } = this.props.editor
        const { position } = this.state;

        if (path.id == selectedPath) {
            return;
        }

        this.setState(
            {
                isEditing: false
            }
        )

        let convertedCords = convertCoordsToSVGCoords(dims.x, dims.y)

        convertedCords = {
            x: convertedCords.x + position.x + dims.width + 5,
            y: convertedCords.y + position.y + 5
        }

        this.props.onSelectedpath(path)

        this.setState({
            selectedDims: convertedCords
        })

    }

    openUploader = selectedPath => this.props.openUploader(selectedPath)

    hideCollage = e => {

        if ( e.target.id === 'svg-container' ) {

            this.onCancel()

        }

    }

    onCancel = () => {
        this.setState({
            isEditing: false
        })
        this.hideHoverBox()
        this.props.toggleVisibility(false)
        this.props.onCancelPath()
    }

    showCollage = partial(this.props.toggleVisibility, true)

    editPath = (selectedPath) => {

        this.setState({
            isEditing: true
        })

        this.props.onEditPath(selectedPath)

    }

    componentDidMount() {

        this.updateCollageDims()

        document.getElementById('svg-container').addEventListener('click', this.hideCollage)

    }

    componentDidUpdate(prevProps) {

        if (prevProps.collage.id !== this.props.collage.id) {
            this.updateCollageDims()
        }

        if (prevProps.editor.elements.length !== this.props.editor.elements.length) {

            const { selectedPath, elements } = this.props.editor;

            if( selectedPath ) {

                const isElement = isElementExist(selectedPath, elements)

                if (isElement) {

                    this.setState({
                        isEditing: true
                    })

                }
            }

        }

    }

    componentWillUnmount() {
        document.getElementById('svg-container').removeEventListener('click', this.hideCollage)
    }

    render() {

        const {
            position,
            selectedDims = {},
            isEditing,
            selectedHoverDims
        } = this.state

        const {
            collage = {},
            visibility,
            editor
        } = this.props;

        const { elements, selectedPath } = editor

        const isActive = isElementExist(selectedPath, elements)

        const choosenPath = collage.paths.find(path => path.id == selectedPath)

        const scale = getScale();

        return (
            <Fragment>
                {
                    selectedHoverDims ? <rect
                        width={selectedHoverDims.width}
                        height={selectedHoverDims.height}
                        transform={toSvgPosition(selectedHoverDims)}
                        onMouseOut={this.hideHoverBox}
                        style={{
                            stroke: 'grey',
                            strokeWidth: 1,
                            fill: 'transparent',
                            cursor: 'pointer',
                        }}>
                    </rect>
                    : null
                }
                <g
                    style={{ opacity: visibility ? 1 : 0 }}
                    id="collage-container"
                    transform={`${toSvgPosition(position)}, scale(${scale})`}
                    onClick={this.showCollage}
                >
                    {
                        collage.paths.map(
                            (path, index) => <AutoDimensions>
                                {
                                    dims => {

                                        const convertedCords = convertCoordsToSVGCoords(dims.x, dims.y)

                                        return <Fragment>
                                            <path key={index}
                                                className={`svgCollagePath ${selectedPath == path.id ? 'active' : ''}`}
                                                d={path.d}
                                                onClick={() => this.onSelectedpath(path, dims)}
                                                id={`collage-path-${path.id}`}
                                                onMouseOver={() => this.displayHoverBox(dims)}
                                            >
                                            </path>
                                        </Fragment>
                                    }
                                }
                            </AutoDimensions>
                        )
                    }
                    <defs>
                        {
                            collage.paths.map(
                                (path, index) => <clipPath key={index} id={path.id}>
                                    <path style={{
                                        fill: 'none',
                                        stroke: 'grey',
                                        strokeWidth: 0.5
                                    }} d={path.d} transform={`${toSvgPosition(position)} scale(${scale})`}></path>
                                </clipPath>
                            )
                        }
                    </defs>
                </g>
                {selectedPath ? <Fragment>
                        <rect
                            onClick={() => this.openUploader(choosenPath)}
                            transform={toSvgPosition(selectedDims)}
                            width='20'
                            height='20'
                            style={{
                                fill: 'grey',
                                cursor: 'pointer',
                            }} rx="5" ry="5">
                        </rect>
                        <image
                            onClick={() => this.openUploader(choosenPath)}
                            xlinkHref='/upload.png'
                            height='10'
                            width='10'
                            style={{
                                cursor: 'pointer'
                            }}
                            transform={toSvgPosition({
                                x: selectedDims.x + 5,
                                y: selectedDims.y + 4
                            })} />
                            {isActive ?
                            
                                isEditing ? 
                                <Fragment>
                                    <rect
                                        onClick={(e) => this.onCancel()}
                                        transform={
                                            toSvgPosition({
                                                x: selectedDims.x,
                                                y: selectedDims.y + 25
                                            })
                                        }
                                        width='20'
                                        height='20'
                                        style={{
                                            fill: 'grey',
                                            cursor: 'pointer',
                                        }} rx="5" ry="5">
                                    </rect>
                                    <image
                                        onClick={(e) => this.onCancel()}
                                        xlinkHref='/delete.png'
                                        height='10'
                                        width='10'
                                        style={{
                                            cursor: 'pointer'
                                        }}
                                        transform={toSvgPosition({
                                            x: selectedDims.x + 5,
                                            y: selectedDims.y + 30
                                        })} /> 
                                </Fragment>
                                :
                                <Fragment>
                                    <rect
                                        onClick={(e) => this.editPath(choosenPath)}
                                        transform={
                                            toSvgPosition({
                                                x: selectedDims.x,
                                                y: selectedDims.y + 25
                                            })
                                        }
                                        width='20'
                                        height='20'
                                        style={{
                                            fill: 'grey',
                                            cursor: 'pointer',
                                        }} rx="5" ry="5">
                                    </rect>
                                    <image
                                        onClick={(e) => this.editPath(choosenPath)}
                                        xlinkHref='/edit.png'
                                        height='10'
                                        width='10'
                                        style={{
                                            cursor: 'pointer'
                                        }}
                                        transform={toSvgPosition({
                                            x: selectedDims.x + 5,
                                            y: selectedDims.y + 30
                                        })} /> 
                                </Fragment>

                            : null}

                    </Fragment>
                : null}
            </Fragment>
        )

    }

}


const CollageConnected = WithEditor(Collage)
const CollageMutationConnected = MutateEditor(CollageConnected)

export default CollageMutationConnected
