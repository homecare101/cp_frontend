import React, { Component } from 'react'
import { textToTspan } from '../../../utils/elements';

export const SvgElementText = () =>
    ({
        position = {
            x: 0,
            y: 0
        },
        parentId = '',
        clipPath = '',
        children = '',
        ...restProps
    }) => {

        const childrenString = textToTspan(children)

        return (
            <g clipPath={`url(#${parentId ? '' : clipPath})`}>
                <g
                    id={parentId}
                    transform={`translate(${position.x}, ${position.y})`}
                >
                    <text {...restProps} dangerouslySetInnerHTML={{ __html: childrenString}} />
                </g>
            </g>
        )

    }
        

export const SvgMarkupElement = () =>  
    ({
        position = {
            x: 0,
            y: 0
        },
        clipPath = '',
        parentId = '',
        htmlToRender = ''
    }) =>
        <g clipPath={`url(#${parentId ? '' : clipPath})`}>
            <g
                id={parentId}
                transform={`translate(${position.x}, ${position.y})`}
                dangerouslySetInnerHTML={{ __html: htmlToRender}}
            ></g>
        </g>

export const SvgElement = WrappedComponent => 
    ({
        position = {
            x: 0,
            y: 0
        },
        parentId = '',
        clipPath = '',
        ...restProps
    }) =>
        <g clipPath={`url(#${parentId ? '' : clipPath})`}>
            <g transform={`translate(${position.x}, ${position.y})`} id={parentId} >
                <WrappedComponent
                    {...restProps}
                />
            </g>
        </g>