import React, { Component } from 'react'

import { findDimensions, textToTspan } from '../../../utils/elements';

const MovableHOC = WrappedComponent =>
    class Movable extends Component {

        onMovableDown = e => {

            const {
                onMovableDown = () => {},
                onMovableActive = () => {}
            } = this.props;

            const dim = findDimensions(e.target);
            const x = e.clientX - dim.left;
            const y = e.clientY - dim.top;

            onMovableDown({x, y}, dim)
            onMovableActive(e)

        }

        onElementHover = e => {
            const {
                onElementHover = () => {}
            } = this.props;

            const dim = findDimensions(e.target);

            onElementHover(this.props.position, dim)

        }

        render () {

            const {
                position = {
                    x: 0,
                    y: 0
                },
                onMovableDown = () => {},
                onMovableActive = () => {},
                onElementHover = () => {},
                ...restProps
            } = this.props

            return <WrappedComponent
                onMovableActive={onMovableActive}
                onMovableDown={this.onMovableDown}
                onElementHover={this.onElementHover}
                position={position}
                {...restProps}
            />

        }

    }

export const MovableMarkup = () => MovableHOC(({
    position,
    onMovableActive,
    onMovableDown,
    onElementHover,
    onElementMouseOut,
    htmlToRender,
    ...restProps
}) => <g onTouchStart={e => onMovableDown(e.changedTouches[0])} onMouseOver={onElementHover} transform={`translate(${position.x}, ${position.y})`} onClick={onMovableActive} onMouseDown={onMovableDown} dangerouslySetInnerHTML={{ __html: htmlToRender}}></g>)

export const MovableSvgText = () => MovableHOC(({
    position,
    onMovableActive,
    onMovableDown,
    onElementHover,
    onElementMouseOut,
    children,
    ...restProps
}) => 
    {
        const childrenString = textToTspan(children)

        return <g onTouchStart={e => onMovableDown(e.changedTouches[0])} onMouseOver={onElementHover} transform={`translate(${position.x}, ${position.y})`} onClick={onMovableActive} onMouseDown={onMovableDown}>
            <text
                dangerouslySetInnerHTML={{ __html: childrenString}}
                {...restProps}
            />
        </g>
    }
)

export const MovableSvgElement = WrappedComponent => MovableHOC(({
    position,
    onMovableActive,
    onMovableDown,
    onElementHover,
    onElementMouseOut,
    ...restProps
}) => <g onTouchStart={e => onMovableDown(e.changedTouches[0])} onMouseOver={onElementHover} transform={`translate(${position.x}, ${position.y})`} onClick={onMovableActive} onMouseDown={onMovableDown} onTouchStart={onMovableDown}>
    <WrappedComponent
        {...restProps}
    />
</g>)