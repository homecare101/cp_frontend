import React from 'react';

import WithEditor from '../../HOC/WithEditor';

import { isElementExist } from '../../../utils/elements';

const CollageElementExist = ({
    pathId,
    editor,
    children
}) => {

    const { elements } = editor;

    return isElementExist(pathId, elements) ? children : null

}

export default WithEditor(CollageElementExist)