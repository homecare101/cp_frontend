import React, { Component, Fragment } from 'react';
import ReactDOMServer from 'react-dom/server';
// import { RenderElementHOC } from '../../HOC/RenderElementHOC';
import WithEditor from '../../HOC/WithEditor';
import MutateEditor from '../../HOC/MutateEditor';
import { findDimensions, getSVGContainerDims, getScale } from '../../../utils/elements';
import { SvgMarkupElement } from './SvgElement';

const SvgImage = SvgMarkupElement()
class PhoneCase extends Component {

    state = {
        svgSize:'',
        imgSize:''
    }

    componentDidMount(){
        const svgDims = getSVGContainerDims()
        const imgDims = findDimensions(document.getElementById('simpleImage'))


        this.setState({
            svgSize : svgDims,
            imgSize: imgDims
        })
    }

    componentDidUpdate (prevProps) {

        if ( prevProps.phoneModel.model !== this.props.phoneModel.model ) {

            const svgDims = getSVGContainerDims()
            const imgDims = findDimensions(document.getElementById('simpleImage'))
    
    
            this.setState({
                svgSize : svgDims,
                imgSize: imgDims
            })

        }

    }

    render () {

        
        const { svgSize ,imgSize} = this.state

        
        const {
            phoneModel = {},
            backgroundColor = "",
            children,
            activeElementIndex = null
        } = this.props

        const svgPositon = {
            
            x: svgSize.width/2 - imgSize.width/2,
            y: svgSize.height/2 - imgSize.height/2
        }

        let scale = getScale();

        const PhoneImageProps = {
            display: activeElementIndex === null ? "inline" : "none",
            className: 'simpleImage',
            id:'simpleImage',
            xlinkHref: phoneModel.image,
            width: phoneModel.width,
            height: phoneModel.height,
            transform: `${phoneModel.transform}, scale(${scale})`
        }

        PhoneImageProps.htmlToRender = ReactDOMServer.renderToStaticMarkup(<image {...PhoneImageProps} />)

        let PhoneShadowProps = {
            display: activeElementIndex === null ? "inline" : "none",
            className: 'shadowImage',
            id: 'shadowImage',
            xlinkHref: phoneModel.shadowImage,
            width: phoneModel.width,
            height: phoneModel.height,
            transform: `${phoneModel.transform}, scale(${scale})`
        }

        PhoneShadowProps.htmlToRender = ReactDOMServer.renderToStaticMarkup(<image {...PhoneShadowProps} />)

        return (

            <Fragment>
                <SvgImage position={{
                        x: svgPositon.x,
                        y: svgPositon.y
                    }}
                    {...PhoneImageProps}
                />
                <clipPath id="phoneCaseClipPath" className="phoneCaseClip"><path className="phoneCaseClipPath" d={activeElementIndex === null ? phoneModel.path : phoneModel.activePathBack} transform={`translate(${svgPositon.x},${svgPositon.y}), scale(${scale})`} style={{
                            fill: backgroundColor,
                            stroke: activeElementIndex === null ? "none" : "#0096ff"
                        }}
                ></path></clipPath>
                <g className="respectiveClip" clipPath={ activeElementIndex === null ? 'url(#phoneCaseClipPath)' : '' }>
                    <path
                    transform={`translate(${svgPositon.x},${svgPositon.y}), scale(${scale})`}
                    id="phoneModelPath"
                    className="phoneCaseClipPath"
                    d={activeElementIndex === null ? phoneModel.path : phoneModel.activePathBack}
                    style={{
                            fill: backgroundColor, stroke: activeElementIndex === null ? "none" : "#0096ff"
                        }}
                    />
                    {
                        children
                    }
                </g>
                <g>
                    <path d= {phoneModel.activePathMain} className="outlineInnerPath" style={{fill: "none", stroke: "#ff00ff", display:activeElementIndex === null ? "none" : "inline"}} transform={`translate(${svgPositon.x},${svgPositon.y}), scale(${scale})`}></path>
                </g>
                <g style={{display: "none"}}><path id="hiddenPath" d={phoneModel.activePathBack} ></path></g>
                <SvgImage position={{
                        x: svgPositon.x,
                        y: svgPositon.y
                    }}
                    {...PhoneShadowProps}
                />
            </Fragment>

        )

    }

}
const PhoneCaseConnected = WithEditor(PhoneCase)
const PhoneCaseMutationConnected = MutateEditor(PhoneCaseConnected)

export default PhoneCaseMutationConnected