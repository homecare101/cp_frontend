import React, { Component } from 'react';

import ImageList from '../../common/ImageList';
import MutateEditor from '../../HOC/MutateEditor';

import { Collages } from '../../../utils/constants';
import WithEditor from '../../HOC/WithEditor';

class CollageTab extends Component {

    onSelectCollage =  selectedCollage => {

        if(selectedCollage.id == 'collage-0') {
            const {
                editor,
            } = this.props
            const {
                elements
            } = editor

            let newElements = elements
            newElements = elements.filter(
                element => {
                    const parsedProps = JSON.parse(element.props)

                    if(parsedProps.clipPath) {
                        return false
                    }

                    return true
                }
            )
            this.props.mutateStore({
                collage: selectedCollage.id,
                selectedPath: '',
                activeElementIndex: null,
                elements: newElements
            })
        } else {
            this.props.mutateStore({
                collage: selectedCollage.id,
                selectedPath: '',
                activeElementIndex: null
            })
        }
    }
    render() {
        return (
            <div>
                <ImageList onClick={this.onSelectCollage} data={Collages} />
            </div>
        )

    }

}

const CollageTabConnected = WithEditor(CollageTab)
const CollageTabMutationConnected = MutateEditor(CollageTabConnected)

export default CollageTabMutationConnected


