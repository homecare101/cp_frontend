import React from 'react';
import { Col } from 'reactstrap';
import { setBackgroundColor, getEditor } from '../../../actions/editor';
import QueryConnected from '../../HOC/QueryConnected';
import { graphql } from 'react-apollo'
import { getColorsQuery } from '../../../Graphql/Query/stocks';

class PhoneColor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      colors: [],
      SelectedColorId: ''
    }
  }
  componentDidMount() {
    this.props.client.query({
      query: getColorsQuery,
    }).then(res => {
      let colorsData = res.data.getColors.list
      this.setState({ colors: colorsData })
    })
      .catch(error => {
        console.log(error, "error")
      })
  }
  onSelectColor(id) {
    this.setState({ SelectedColorId: id })
  }
  clickFilter(type, value) {
    this.props.mutate({ variables: { backgroundColor: value } })
  }

  
  render() {
    let { colors } = this.state
    return (
      <div className="EditorColor">
        <Col md={4} className="noPadding">
          <p>색상</p>
        </Col>
        <Col md={8} className="noPadding EditorColorScroll">
          {
            colors.map((item, index) => {
              return (
                <label className="radio-out" onClick={this.onSelectColor.bind(this, item._id)}>
                  <input className="radio" type="radio" name="question_1_2" value="black" onClick={this.clickFilter.bind(this, 'color', item.value)} />
                  {this.state.SelectedColorId == item._id ?

                    <div className="colorOutMain">
                      <p className="colorOutTickEditor">&#x2714;</p>
                      <span style={{ backgroundColor: item.code, display: 'block', width: 14, height: 14, borderRadius: '100%' }}></span>
                    </div>
                    :
                    <span style={{ backgroundColor: item.code, display: 'block', width: 15, height: 15, borderRadius: '100%' }}></span>
                  }
                </label>
              )
            })
          }
        </Col>
      </div>

    );
  }
}
const colorsTabConnected = QueryConnected(getEditor)(PhoneColor)
const colorsTabMutationConnected = graphql(setBackgroundColor )(colorsTabConnected)

export default colorsTabMutationConnected