import React, { Component } from 'react';
import { graphql } from 'react-apollo'
import QueryConnected from '../../HOC/QueryConnected';
import { getEditor, updateMaterial } from '../../../actions/editor';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, Input, Row, Col, Label } from 'reactstrap';
import './tabs.css'
import { getMaterialsQuery } from '../../../Graphql/Query/stocks';

class MaterialTab extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dropdownOpenMaterial: false,
            materialToShow: [],
            materialName: 'material1',
        };
    }
    componentDidMount() {
        let { materialToShow } = this.state
        this.props.client.query({
            query: getMaterialsQuery,
        }).then(res => {
            materialToShow = res.data.getMaterials.list
            this.setState({ materialToShow })

        })
            .catch(error => {
                console.log(error, "error")
            })
    }
   
    toggleMaterial = () => {
        this.setState({
            dropdownOpenMaterial: !this.state.dropdownOpenMaterial
        });
    }
    selectMaterial =  data => {
         this.props.mutate({ variables: { materials: data.name } })

         this.setState({
            dropdownOpenMaterial: !this.state.dropdownOpenMaterial,
            materialName: data.name
        });
    }
   
    render() {
        let { materialToShow, colors } = this.state

        const {
            data
        } = this.props;

        return (
                <div className="modelMaterialInline">
                    <Col md={4} className="noPadding">
                        <p>케이스 종류</p>
                    </Col>
                    <Col md={8} className="noPadding">
                        <ButtonDropdown isOpen={this.state.dropdownOpenMaterial} toggle={this.toggleMaterial}>
                            <DropdownToggle className="DropDownMainEditor"><span className="modelName">{this.state.materialName} </span><span className='fa fa-angle-down' /></DropdownToggle>
                            <DropdownMenu className="btnDropMaterial">
                                {
                                    materialToShow.map((data, index) => {
                                        return (
                                            <DropdownItem onClick={this.selectMaterial.bind(this,data)} key={index} >{data.name}</DropdownItem>
                                        )
                                    })
                                }
                            </DropdownMenu>
                        </ButtonDropdown>
                    </Col>
                </div>
        )

    }
}
const ModelTabConnected = QueryConnected(getEditor)(MaterialTab)
const ModelTabMutationConnected = graphql(updateMaterial)(ModelTabConnected)

export default ModelTabMutationConnected

