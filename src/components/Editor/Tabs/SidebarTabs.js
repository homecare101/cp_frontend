import BackgroundTab from './BackgroundTab';
import UploadPhotoTab from './UploadPhotoTab';
import CollageTab from './CollageTab';
import MaterialTab from './MaterialTab';
import TextTab from './TextTab';
import ClipartsTab from './ClipartsTab';

const SidebarTabs = [
    {
        title: '내 이미지 넣기',
        id: 'Uploadphoto',
        component: UploadPhotoTab,
        src:"/uploadBlack.png" ,
        activeSrc:"/upload@3x.png" ,
    },
    {
        title: '콜라주',
        id: 'Collage',
        component: CollageTab,
        src:"/collage@3x.png",
        activeSrc:"/collage.png",

    },
    // {
    //     title: 'Material',
    //     id: 'Material',
    //     component: MaterialTab,
    //     src:'https://d6ce0no7ktiq.cloudfront.net/images/svg/inverted-text.svg"',
    //     hideMobile: true
    // },
    {
        title: '텍스트 넣기',
        id: 'Text',
        component: TextTab,
        src:'/text@3x.png',
        activeSrc:"/text.png",
    },
    {
        title: '바탕 색상',
        id: 'Background',
        component: BackgroundTab,
        src:"/bg@3x.png",
        activeSrc:"/bg.png",
    },
    {
        title: '클립아트',
        id: 'Cliparts',
        component: ClipartsTab,
        src:'/clipart@3x.png',
        activeSrc:"/clipart.png",
    }
]


export default SidebarTabs;