import React, { Component } from 'react';
import { SketchPicker } from 'react-color';
import { graphql } from 'react-apollo';

import { setBackgroundColor, getEditor } from '../../../actions/editor';
import QueryConnected from '../../HOC/QueryConnected';

class BackgroundTab extends Component {

    onChangeComplete = color => {
        const backgroundColor = `rgba(${color.rgb.r}, ${color.rgb.g}, ${color.rgb.b}, ${color.rgb.a})`
        this.props.mutate({ variables: { backgroundColor: backgroundColor } })
    }

    render () {
        const {
            data
        } = this.props;

        const {
            editor = {}
        } = data;

        return (
            <div className='setBgTop'>
                <p className="componentHeading">Background</p>
                <SketchPicker
                    color={editor.backgroundColor}
                    onChangeComplete={this.onChangeComplete}
                    className="innersketch"
                />
            </div>
        )
    }

}

const BackgroundTabConnected = QueryConnected(getEditor)(BackgroundTab)
const BackgroundTabMutationConnected = graphql(setBackgroundColor)(BackgroundTabConnected)

export default BackgroundTabMutationConnected;