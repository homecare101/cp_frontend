import React from 'react';
import { Button, Col } from 'reactstrap';
import { withRouter } from 'react-router';
import { compose, graphql } from 'react-apollo'
import QueryConnected from '../../HOC/QueryConnected';
import { updateElements, getEditor, setToast } from '../../../actions/editor';
import { editorMutaion, editCaseMutation } from '../../../Graphql/Mutations/editor';
import { Modal, Input } from 'reactstrap';
import './tabs.css'
import WithEditor from '../../HOC/WithEditor';
import MutateEditor from '../../HOC/MutateEditor';
import { AuthModalConsumer } from '../../Auth/AuthModalProvider/AuthModalProvider'
import { getCaseDataMutation } from '../../../Graphql/Query/editor';
import SaveBtn from '../../common/SaveBtn';

class PriceTab extends React.Component {
  constructor() {
    super()
    this.state = {
      modal: false,
      name: '',
      errorInd: false,
      preview: '',
      toggleBtn: false,
      btnText: false,
      outlineSvgData: ''
    }
  }
  componentWillMount() {
    let userId = localStorage.getItem('id')
    if (!userId) {
      this.setState({ btnText: true })
    }

    if (this.props.match.params.caseId) {
      this.setState({ toggleBtn: true })
      this.props.client.query({
        query: getCaseDataMutation,
        fetchPolicy: "network-only",
        variables: {
          userId: userId,
          caseId: this.props.match.params.caseId,
        },
      }).then(res => {
        var array = res.data.getUserCasesById.name
        this.setState({ name: array })
      })
        .catch(error => {
          console.log(error, "error")
        })
    }
  }
  handleChange(key, event) {
    let value = event.target.value;
    let { name } = this.state
    name = value
    this.setState({ name });
  }
  getChildElement(child, parent){
    let childElements = document.getElementsByClassName(child)
      let correctChild = ""
      for(let i = 0; i<=childElements.length - 1; i++){
        if(childElements[i].ownerSVGElement.parentElement.id === parent){
          correctChild = childElements[i]
        }
      }
      return correctChild
  }

  changePathOfElement (pathVal, child, parent) {
    let childElements = document.getElementsByClassName(child)
    for(let i = 0; i<=childElements.length - 1; i++){
      
      if(childElements[i].ownerSVGElement.parentElement.id === parent){
        childElements[i].setAttribute("d", pathVal)
        childElements[i].style.stroke = "#0096ff"
      }
    }
  }

  openModal() {
    this.props.mutate({
      variables: {
        activeElementIndex: null,
        removeExternalLayers: true
      }
    }).then((res) => {
      let imgData = document.getElementById('svg-container').outerHTML
      let checkExistingSvg = document.getElementById('outlineSVG')
      if( checkExistingSvg ){
        checkExistingSvg.innerHTML = imgData
      } else {
        let outlineSVG = document.createElement('div')
        outlineSVG.setAttribute("id", "outlineSVG")
        outlineSVG.style.display = "none"
        outlineSVG.innerHTML = imgData
        document.body.appendChild(outlineSVG)
      }
      let svgElement = document.querySelector('#outlineSVG > #svg-container')
      svgElement.style.transform = "scale(6)"
      let shadowElement = this.getChildElement("shadowImage","outlineSVG")
      shadowElement.style.display = "none"
      let simpleImageElement = this.getChildElement("simpleImage","outlineSVG")
      simpleImageElement.style.display = "none"
      // let outlineInnerPathElement = this.getChildElement("outlineInnerPath","outlineSVG")
      // outlineInnerPathElement.style.display = "inline"
      let outerPathToGive = document.getElementById('hiddenPath').getAttribute("d")
      this.changePathOfElement(outerPathToGive, "phoneCaseClipPath", "outlineSVG")
      let phoneCaseClipElem = this.getChildElement("phoneCaseClip","outlineSVG")
      phoneCaseClipElem.id = "finalPhoneCaseClipPath"
      let respectiveClipElem = this.getChildElement("respectiveClip","outlineSVG")
      respectiveClipElem.removeAttribute("clip-path")
      respectiveClipElem.setAttribute("clip-path","url(#finalPhoneCaseClipPath)")
      let outlineSvgData = document.getElementById('outlineSVG').innerHTML
      this.setState({ preview: imgData, modal: true, outlineSvgData: outlineSvgData})

    })

  }
  checkToken() {
    let token = localStorage.getItem('token')
    if (token) {
      return true
    } else {
      return false
    }
  }
  saveCase() {
    const { data } = this.props;
    let value = JSON.stringify(data)
    let user = localStorage.getItem('id')
    let { preview, name, outlineSvgData} = this.state
    let caseId = this.props.match.params.caseId
    if (this.props.match.params.caseId) {
      console.log("update")
      this.props.editData({
        variables: {
          caseId: caseId,
          caseData: value,
          name: name,
          count: 1,
          price: 15,
          userId: user,
          preview: preview,
          material: data.editor.materials,
          model: data.editor.model,
          outlineSvgData: outlineSvgData,
          brand: '',
        }
      }).then(response => {
      console.log("update22")

        this.props.setToast({
          variables: {
            type: 'success',
            message: "case Edited Successfully"
          }
        })
        this.props.history.push(`/userDetails/savedDesign`)
      }).catch(error => {
        console.log(error, "error")
        let errorText = { ...error }
        if (errorText !== {}) {
          this.props.setToast({
            variables: { type: 'error', message: "Some error occur" }
          })
        }
      })

    } else {
      this.props.saveData({
        variables: {
          caseData: value,
          name: name,
          userId: user,
          preview: preview,
          count: 1,
          price: 15,
          outlineSvgData: outlineSvgData,
          material: data.editor.materials,
          model: data.editor.model,
          brand: '',
        }
      }).then(response => {
        this.props.setToast({
          variables: {
            type: 'success',
            message: "case Saved Successfully"
          }
        })
        this.setState({ modal: false })
        this.props.history.push(`/userDetails/savedDesign`)
      }).catch(error => {
        console.log(error, "error")
        let errorText = { ...error }
        if (errorText !== {}) {
          this.props.setToast({
            variables: { type: 'error', message: errorText.graphQLErrors[0].message }
          })
        }
      })
    }
  }

  loginToggle() {
    this.setState({ modal: !this.state.modal });
  }
  render() {
    return (
      <div className="pricetabEditor">
        <div className="editorPriceInline">
          <Col md={5} className="noPadding">
            <h4 className="priceTextEditor">19,000원</h4>
          </Col>
          <Col md={7} className="noPadding alineEndEditor savebtnEditor">
            <SaveBtn className="savebtnEditor"/>
          </Col>
        </div>

        <img ref="image" src={'./edit.png'} className="hidden" />
        {/* <Modal isOpen={this.state.modal} centered className="home-auth-modal">
          <div>
            <div className='home-modal-close'>
              <button type="button" onClick={this.loginToggle.bind(this)} className="home-modal-close-button" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modalOpen">
              <h1>케이스 이름을 입력하세요</h1>
              <div className="displayFlex">
                <div className="IconInput">
                  <Input className="name" type="text" name="name" value={this.state.name} onChange={this.handleChange.bind(this, 'name')} placeholder="사례 이름" />
                  {this.state.errorInd ? <span className="hasError errorMsgCard">This field is required</span> : null}
                </div>
              </div>
              <Button className="savebtn" onClick={this.saveCase.bind(this)}>{this.state.toggleBtn ? '수정' : "저장하기"}</Button>
            </div>
          </div>
        </Modal> */}
      </div>
    );
  }
}

const PriceTabconnected = QueryConnected(getEditor)(PriceTab)
const PriceTabcon = graphql(updateElements)(PriceTabconnected)

const PriceTabConnected = WithEditor(PriceTabcon)
const PriceTabMutationConnected = MutateEditor(PriceTabConnected)

export default withRouter(compose([
  graphql(editorMutaion, { name: 'saveData' }),
  graphql(setToast, { name: 'setToast' }),
  graphql(editCaseMutation, { name: 'editData' })]
)(PriceTabMutationConnected));