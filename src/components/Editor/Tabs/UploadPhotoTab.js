import React, { Component } from 'react';
import UploadImageHOC from '../../HOC/UploadImageHOC'
import './tabs.css'

class UploadPhotoTab extends Component {
    render () {
        return (
            <div className="imageupload">
                <div className="imagedownload">
                    <img src="/uploadCase.png" className="imageAll" />
                </div>
                <p>내 이미지를 여기로 <br /> 가져오세요 <br /></p>
                <span className="imageEditorTextDownload">이미지 찾기</span><br />
                <span className='img-extensions'>pdf, png, jpeg, tiff, <br /> svg, gif, bmp</span>
            </div>
        )
    }
}

export default UploadImageHOC(UploadPhotoTab)
