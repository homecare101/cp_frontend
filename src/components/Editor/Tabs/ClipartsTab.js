import React, { Component } from 'react';
import { graphql, Query } from 'react-apollo'

import QueryConnected from '../../HOC/QueryConnected';
import { addElements, getEditor, getCliparts } from '../../../actions/editor';
import { createElement, findDimensions, getCenter } from '../../../utils/elements';
import { getImageDims, calculateAspectRatioFit } from '../../../utils/functions';
import ImageList from '../../common/ImageList';

class ClipartsTab extends Component {

    selectedClipArt = clipart => {

        getImageDims(clipart.src)
            .then(
                ({ width, height }) => {

                    const pathDims = findDimensions(document.getElementById('phoneModelPath'))
                    const dims = calculateAspectRatioFit(width, height, pathDims.width, pathDims.height)

                    const position = getCenter(dims.width, dims.height)

                    const element = createElement({href: clipart.src,xlinkHref: clipart.src, width: dims.width, height: dims.height}, position, 'image')

                    this.props.mutate({ variables : { elements: [element] } })

                }
            )

    }

    render () {

        return (
            <div >
                <Query query={getCliparts}>
                {
                    ({data}) =>{
                        const { cliparts = [] } = data
                        return <ImageList onClick={this.selectedClipArt} data={cliparts}/>
                    }
                }
                </Query>
            </div>
        )

    }

}

const ClipartsTabConnected = QueryConnected(getEditor)(ClipartsTab)
const ClipartsTabMutationConnected = graphql(addElements)(ClipartsTabConnected)

export default ClipartsTabMutationConnected

// export default ClipartsTabMutationConnected

// import React, { Component } from 'react';
// import { graphql } from 'react-apollo'

// import QueryConnected from '../../HOC/QueryConnected';
// import { addElements, getEditor } from '../../../actions/editor';
// import { createElement, findDimensions, getCenter } from '../../../utils/elements';
// import { getImageDims, calculateAspectRatioFit } from '../../../utils/functions';
// import ImageList from '../../common/ImageList';
// import { Cliparts } from '../../../utils/constants';

// import WithEditor from '../../HOC/WithEditor'
// import MutateEditor from '../../HOC/MutateEditor'

// class ClipartsTab extends Component {

//     selectedClipArt = clipart => {

//         const { editor } = this.props

//         let { elements } = editor

//         getImageDims(clipart.src)
//             .then(
//                 ({ width, height }) => {

//                     // const element = createElement({xlinkHref: clipart.src, width: dims.width, height: dims.height}, position, 'image')

//                     // this.props.mutate({ variables : { elements: [element] } })

//                     if ( editor.collage ) {

//                         let pathId = editor.selectedPath || ''
            
//                         if (pathId) {
            
//                             elements = removeExistingCollageImage(editor.selectedPath, elements)
            
            
//                         } else {
            
//                             const selectedCollage = Collages.find(collage => collage.id == editor.collage)
                            
//                             const emptyPathId = getEmptyPath(selectedCollage, elements)
            
//                             pathId = emptyPathId
            
//                             if (!emptyPathId) {
//                                 return
//                             }
            
//                         }
            
//                         const resizedImage = getResizedImagePath(image, pathId)
            
//                         const pathElement = createCollagePathElement(pathId, {...image, ...resizedImage})
            
//                         const modifiedElements = elements.concat(pathElement)
            
//                         this.props.mutateStore({
//                             elements: modifiedElements,
//                             activeElementIndex: (modifiedElements.length - 1),
//                             activeElementDims: {
//                                 width: resizedImage.width,
//                                 height: resizedImage.height,
//                                 __typename: 'activeElementDims'
//                             }
//                         })
            
//                     } else {
            
//                         const pathDims = findDimensions(document.getElementById('phoneModelPath'))
//                         const dims = calculateAspectRatioFit(width, height, pathDims.width, pathDims.height)
    
//                         const position = getCenter(dims.width, dims.height)

//                         const element = createImage(image.width, image.height, image.dataUrl)

//                         this.props.mutateStore({ elements: elements.concat(element) })
            
//                         this.props.mutateStore({ elements: elements.concat(element) })
            
//                     }

//                 }
//             )

//     }

//     render () {

//         return (
//             <div >
//                 <ImageList onClick={this.selectedClipArt} data={Cliparts}/>
//             </div>
//         )

//     }

// }

// const ClipartsTabConnected = WithEditor(ClipartsTab)
// const ClipartsTabMutationConnected = MutateEditor(ClipartsTabConnected)

// export default ClipartsTabMutationConnected