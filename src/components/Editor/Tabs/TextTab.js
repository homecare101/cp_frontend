import React, { Component, Fragment } from 'react';
import ReactDOMServer from 'react-dom/server';

import { FormGroup, Label, Input } from 'reactstrap';
import FontPicker from 'font-picker-react';
import { SketchPicker } from 'react-color';

import WithEditor from '../../HOC/WithEditor';
import MutateEditor from '../../HOC/MutateEditor';
import { SvgMarkupElement } from '../EditorTools/SvgElement'; 

import { createElement, updateElementProps, getCenter } from '../../../utils/elements';

class TextTab extends Component {

	constructor() {
		super();

		this.state = { 
			text:'',
			activeFont: 'Open Sans',
			background: '#fff',
			textAnchor: 'start'
		}
	}

	updateTextElement = props => {

		const { elements = [], activeElementIndex } = this.props.editor

		const copiedElements = [...elements]

		const selectedElement = copiedElements[activeElementIndex]

		const modifiedElement = updateElementProps(selectedElement, props)

		copiedElements[activeElementIndex] = modifiedElement;

		this.props.mutateStore({ elements: [...copiedElements] })

	}

	updateStyle = styleToUpdate => {

		const { elements = [], activeElementIndex } = this.props.editor

		const { props } = elements[activeElementIndex]

		const parsedProps = JSON.parse(props)

		this.updateTextElement({
			...parsedProps,
			style: {
				...parsedProps.style,
				...styleToUpdate
			}
		})

	}

	onChangeText = e => {

		const { elements = [], activeElementIndex } = this.props.editor

		const selectedElement = elements[activeElementIndex] || {}

		// if activeElementIndex is null or selected item is not text then add a new text element
		
		if (selectedElement.type !== 'text') {

			const position = getCenter();

			let element = createElement({ children: e.target.value, style: { fontSize: 40 }, textAnchor: 'start'}, position, 'text')
			this.props.mutateStore({ elements: [...elements, element], activeElementIndex: elements.length })
		}

		if (selectedElement.type == 'text') {

			this.updateTextElement({
				children: e.target.value
			})

		}

		this.setState({
			text: e.target.value
		})

	}

	handleChangeComplete = color => {

		this.updateStyle({
			fill: color.hex
		})

	};

	updateFonts = nextFont => {

		this.updateStyle({
			fontFamily: nextFont.family
		})

	}

	componentDidUpdate (prevProps) {
		if ( prevProps.editor.activeElementIndex !== this.props.editor.activeElementIndex ) {

			const { elements = [], activeElementIndex = null } = this.props.editor
			const selectedElement = elements[activeElementIndex] || {}


			if ( activeElementIndex == null || selectedElement.type != 'text') {
				this.setState({
					text: '',
					textAnchor: 'start'
				})

				return;
			}

			if (selectedElement.type == 'text') {
				const { props } = selectedElement

				const parsedProps = JSON.parse(props)

				this.setState({
					text: parsedProps.children,
					textAnchor: parsedProps.textAnchor
				})
			}
		}
	} 

	updateAlignment = textAnchor => {
		this.setState({
			textAnchor
		})

		this.updateTextElement({
			textAnchor
		})
	}

    render () {

		const { editor = {} } = this.props

		const { text, textAnchor } = this.state;

		const { elements = [], activeElementIndex = null } = editor

		const selectedElement = elements[activeElementIndex] || {}
		let color = 'black'
		let fontFamily = 'Open Sans'

		if ( (activeElementIndex !== null) && selectedElement.type == 'text' ) {

			const { props } = elements[activeElementIndex]

			const parsedProps = JSON.parse(props)

			if(parsedProps.style) {

				color = parsedProps.style.color || ''
				fontFamily = parsedProps.style.fontFamily || fontFamily

			}

		}

        return (
            <div className="full-width">
				<FormGroup>
					<Label for="exampleText"><h3 className="textLogo">텍스트 넣기</h3></Label>
					<Input 
						type="textarea"
						placeholder="텍스트를 여기에 넣으세요"
						autoFocus 
						value={text}
						onChange={this.onChangeText}
						width="265px"
						className="innertext"
					/>
                </FormGroup>
				{
					activeElementIndex !== null ?
						<Fragment>
								<h5 className="InnerFonts">폰트</h5>
								<div className="display-none-font">
									<FontPicker
										apiKey="AIzaSyAOuohdvROulN7J5Wj4vwoBfz8WSP0NnV8"
										activeFont={fontFamily}
										onChange={this.updateFonts}
									/>
								</div>
							<div>
								<SketchPicker
									color={ color }
									onChangeComplete={ this.handleChangeComplete }
									// style={{marginTop:'20px',boxSizing: 'inherit !important',width: '100% !important'}}
									className="innersketch"
								/>
							</div>
								<h5 className="InnerFonts">텍스트 정렬</h5>
								<div className="alignment-container">
									<span className={textAnchor == 'start' ? 'active' : '' } onClick={() => this.updateAlignment('start')}>
										<i class="fa fa-align-left" aria-hidden="true"></i>
									</span>
									<span className={textAnchor == 'middle' ? 'active' : '' } onClick={() => this.updateAlignment('middle')}>
										<i class="fa fa-align-center" aria-hidden="true"></i>
									</span>
									<span className={textAnchor == 'end' ? 'active' : '' } onClick={() => this.updateAlignment('end')}>
										<i class="fa fa-align-right" aria-hidden="true"></i>
									</span>
								</div>
								<div className="mobile-text-tab">
									<FontPicker
										apiKey="AIzaSyAOuohdvROulN7J5Wj4vwoBfz8WSP0NnV8"
										activeFont={fontFamily}
										onChange={this.updateFonts}
									/>
									<div className="alignment-container">
										<span className={textAnchor == 'start' ? 'active' : '' } onClick={() => this.updateAlignment('start')}>
											<i class="fa fa-align-left" aria-hidden="true"></i>
										</span>
										<span className={textAnchor == 'middle' ? 'active' : '' } onClick={() => this.updateAlignment('middle')}>
											<i class="fa fa-align-center" aria-hidden="true"></i>
										</span>
										<span className={textAnchor == 'end' ? 'active' : '' } onClick={() => this.updateAlignment('end')}>
											<i class="fa fa-align-right" aria-hidden="true"></i>
										</span>
									</div>
								</div>
						</Fragment>
					: null
				}

            </div>
        )

    }

}

const TextTabConnected = WithEditor(TextTab)
const TextTabMutationConnected = MutateEditor(TextTabConnected)

export default TextTabMutationConnected