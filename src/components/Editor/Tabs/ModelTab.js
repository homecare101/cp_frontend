import React, { Component } from 'react';
import { graphql, Query, compose, withApollo } from 'react-apollo'

import QueryConnected from '../../HOC/QueryConnected';
import { updateModel, getEditor, getPhoneModel } from '../../../actions/editor';

import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, Col } from 'reactstrap';
// import { Cases } from '../../../utils/constants';
import { withRouter } from 'react-router';
import MaterialTab from './MaterialTab';
import PriceTab from './PriceTab';
import './tabs.css'
import PhoneColor from './PhoneColor';
import { getCaseModelQuery } from '../../../Graphql/Query/Cases';
import { getColorsQuery } from '../../../Graphql/Query/stocks';

class ModelTab extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allCasesModel: [],
            dropdownOpen: false,
            modelName: 'iPhone X',
            colors: [],
            SelectedColorId: '',
            mobileCases: null,
            dropdownBrandOpen: false,
            Brandname:'iphone'
        };
    }
    componentDidMount() {

        this.props.client.query({
            query: getCaseModelQuery,
            variables: {},
        }).then(res => {
            this.setState({ mobileCases : res.data.getCaseModel.list, allCasesModel: res.data.getCaseModel.list})
        })
        .catch(error => {
            console.log(error, "error")
        })

        this.props.client.query({
            query: getColorsQuery,
        }).then(res => {
            let colorsData = res.data.getColors.list
            this.setState({ colors: colorsData })
        })
        .catch(error => {
            console.log(error, "error")
        })
        const { cases = [] } = this.props;
        if (cases.length > 0 ) {
            this.setState({modelName: cases[0].model})
        }
    }
    toggleBrand = () => {
        this.setState({
            dropdownBrandOpen: !this.state.dropdownBrandOpen
        });
    }
    toggle = () => {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }
    select = event => {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen,
            modelName: event.target.innerText
        });
        this.props.mutate({ variables: { model: event.target.innerText } })
    }

    selectBrand = event =>{
        let cases = [], selectedCase = ""
        cases = this.state.allCasesModel && this.state.allCasesModel.filter((data, index) => {
            console.log(data.brand, event.target.innerText,"11111")
            if(data.brand === event.target.innerText){
                return data
            }
        })
        if(cases.length > 0){
            selectedCase = cases[0].model
            this.props.mutate({ variables: { model: cases[0].model } })
        }
        this.setState({
            mobileCases: cases,
            modelName: selectedCase,
            dropdownBrandOpen: !this.state.dropdownBrandOpen,
            Brandname: event.target.innerText,

        });
    }
    onSelectColor(id) {
        this.setState({ SelectedColorId: id })
    }
    clickFilter(type, value) {
    }
    render() {
        let { mobileCases} = this.state

        const {
            data
        } = this.props;
        const {
            editor = {}
        } = data;

        return (
            <div className="modelSection">

                <div className="modelMaterialInline">
                    <Col md={4} className="noPadding">
                        <p>제조사</p>
                    </Col>
                    <Col md={8} className="noPadding">
                        <ButtonDropdown isOpen={this.state.dropdownBrandOpen} toggle={this.toggleBrand}>
                            <DropdownToggle className="DropDownMainEditor"><span className="modelName">{this.state.Brandname} </span><span className='fa fa-angle-down' /></DropdownToggle>
                            <DropdownMenu onClick={this.selectBrand} >
                                <DropdownItem>
                                    iphone
                                </DropdownItem>
                                <DropdownItem>
                                    samsung
                                </DropdownItem>
                            </DropdownMenu>
                        </ButtonDropdown>
                    </Col>
                </div>

                <div className="modelMaterialInline">
                    <Col md={4} className="noPadding">
                        <p>모델명</p>
                    </Col>
                    <Col md={8} className="noPadding">

                        <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                            <DropdownToggle className="DropDownMainEditor"><span className="modelName">{this.state.modelName} </span><span className='fa fa-angle-down' /></DropdownToggle>
                            <DropdownMenu>
                                { mobileCases && mobileCases.length > 0 ?  
                                    mobileCases && mobileCases.map((data, index) => {
                                            return (
                                                <DropdownItem onClick={this.select} key={index} >{data.model}</DropdownItem>
                                            ) 
                                        })
                                    :  
                                    <DropdownItem>No Data</DropdownItem>
                                    } 
                            </DropdownMenu>
                        </ButtonDropdown>
                    </Col>
                </div>

                <MaterialTab />
                <PhoneColor />
                <PriceTab />
            </div>
        )

    }

}

const ModelTabConnected = QueryConnected(getEditor)(ModelTab)
const ModelTabMutationConnected = graphql(updateModel)(ModelTabConnected)

export default withRouter(compose(
    withApollo)(ModelTabMutationConnected));


// props => {
//     return <Query query={getCaseModelQuery}>
//         {
//             ({data}) => <ModelTabMutationConnected cases={data.cases} {...props} />
//         }
//     </Query>
//  }

