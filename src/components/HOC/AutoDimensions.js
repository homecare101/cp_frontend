import React, { Component } from 'react';
import ReactDOMServer from 'react-dom/server';
import { findDimensions } from '../../utils/elements';

export default class AutoDimensions extends Component {

    state = {
        dims: {}
    }

    componentDidMount () {

        const dims = findDimensions(this.groupElement)

        this.setState({
            dims
        })

    }

    render () {

        const {
            dims = {}
        } = this.state

        return <g ref={ c => this.groupElement = c }>
            {this.props.children(dims)}
        </g>

    }

}