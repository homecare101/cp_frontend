import React, { Component } from 'react'

const WithRefHOC = WrappedComponent => {
    class WithRefComponent extends Component {
      render() {
        const {forwardedRef, ...rest} = this.props;
        return <WrappedComponent ref={forwardedRef} {...rest} />
      }
    }

    return React.forwardRef((props, ref) => {
      return <WithRefComponent {...props} forwardedRef={ref} />
    })
}

export default WithRefHOC;