import React, { Component } from 'react';
import MutateEditor from './MutateEditor';
import WithEditor from './WithEditor';

class DelementElement extends Component {

    removeElement = e => {

        e.stopPropagation()

        const { elements, activeElementIndex } = this.props.editor

        const copiedElements = [...elements]

        copiedElements.splice(activeElementIndex, 1)

        this.props.mutateStore({ elements: [...copiedElements], activeElementIndex: null})
    }
    
    render () {

        const {
            WrappedComponent,
            ...restProps
        } = this.props;
       
        return (
            <WrappedComponent
                removeElement={this.removeElement}
                {...restProps}
            />
        )

    }

}

const DelementElementConnected = WithEditor(DelementElement)
const DelementElementMutationConnected = MutateEditor(DelementElementConnected)

const DelementElementHOC = WrappedComponent => props => <DelementElementMutationConnected
    WrappedComponent={WrappedComponent}
    {...props}
/>

export default DelementElementHOC;