import React, { Component } from 'react';

import QueryConnected from './QueryConnected';

import { getEditor } from '../../actions/editor';

class WithEditorComponent extends Component {

    render () {

        const { editor = {} } = this.props.data;
        const { WrappedComponent, WrappedComponentProps } = this.props;

        return <WrappedComponent
            editor={editor}
            {...WrappedComponentProps}
        />

    }

}

const WithEditorComponentConnected = QueryConnected(getEditor)(WithEditorComponent)

const WithEditor = WrappedComponent => props => {
    return <WithEditorComponentConnected
        WrappedComponent={WrappedComponent}
        WrappedComponentProps={props}
    />
}

export default WithEditor