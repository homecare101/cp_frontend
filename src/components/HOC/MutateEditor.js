import React, { Component } from 'react';
import { graphql } from 'react-apollo';

import WithEditor from './WithEditor';

import { updateEditor } from '../../actions/editor';

class MutateEditorComponent extends Component {

    mutateStore = variables => {

        const { editor } = this.props

        let {
            activeElementDims,
            activeElementIndex,
            removeExternalLayers,
            elements,
            resizeEdge,
            isRotating,
            activeRotation,
            currentMousePointer,
            collage,
            selectedPath,
            backgroundColor
        } = variables

        if(!activeElementDims) {
            activeElementDims = editor.activeElementDims
        }

        if(typeof activeElementIndex === 'undefined') {
            activeElementIndex = editor.activeElementIndex
        }

        if(typeof backgroundColor === 'undefined') {
            backgroundColor = editor.backgroundColor
        }


        if(typeof removeExternalLayers === 'undefined') {
            removeExternalLayers = editor.removeExternalLayers
        }

        if(!elements) {
            elements = editor.elements
        }

        if(typeof resizeEdge === 'undefined') {
            resizeEdge = editor.resizeEdge
        }

        if(typeof isRotating === 'undefined') {
            isRotating = editor.isRotating
        }

        if(typeof activeRotation === 'undefined') {
            activeRotation = editor.activeRotation
        }

        if(typeof currentMousePointer === 'undefined') {
            currentMousePointer = editor.currentMousePointer
        }

        if(typeof collage === 'undefined') {
            collage = editor.collage
        }

        if(typeof selectedPath === 'undefined') {
            selectedPath = editor.selectedPath
        }

        this.props.mutate({ variables: {
            elements,
            activeElementIndex,
            removeExternalLayers,
            activeElementDims,
            resizeEdge,
            isRotating,
            activeRotation,
            collage,
            selectedPath,
            backgroundColor,
            currentMousePointer: {...currentMousePointer, __typename: 'currentMousePointer'}
        } })

    }

    render () {

        const { WrappedComponent, WrappedComponentProps } = this.props;

        return <WrappedComponent
            mutateStore={this.mutateStore}
            {...WrappedComponentProps}
        />

    }

}

const MutateEditorConnected = WithEditor(MutateEditorComponent)
const MutateEditorMutationConnected = graphql(updateEditor)(MutateEditorConnected)

const MutateEditor = WrappedComponent => props => {
    return <MutateEditorMutationConnected
        WrappedComponent={WrappedComponent}
        WrappedComponentProps={props}
    />
}

export default MutateEditor