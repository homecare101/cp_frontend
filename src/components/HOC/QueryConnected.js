import React from 'react';
import { Query } from 'react-apollo';

const QueryConnected = query =>
    WrappedComponent =>
        WrappedComponentProps =>
            <Query query={query}>
                {props => {
                    return <WrappedComponent {...props} {...WrappedComponentProps} />
                }}
            </Query>

export default QueryConnected