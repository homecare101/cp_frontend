import React, { Component, Fragment } from 'react';
import { findDimensions } from '../../utils/elements';

export default class AutoDimensionsPath extends Component {

    state = {
        dims: {}
    }

    componentDidMount () {

        const dims = findDimensions(this.groupElement)

        this.setState({
            dims
        })

    }

    // componentDidUpdate (prevProps) {

    //     if ( prevProps.d !== this.props.d ) {

    //         const dims = findDimensions(this.groupElement)

    //         this.setState({
    //             dims
    //         })

    //     }

    // } 

    render () {

        const {
            dims = {}
        } = this.state

        const {
            d = ''
        } = this.props

        return <g ref={ c => this.groupElement = c }>
            {this.props.children(dims)}
        </g>
    }

}
