import React, { Component } from 'react';
// import { graphql } from 'react-apollo'

import ImageUploader from '../Editor/EditorTools/Image/ImageUploader';
import WithEditor from './WithEditor'
import MutateEditor from './MutateEditor'

// import { addElements } from '../../actions/editor';
import { createImage } from '../../utils/elements';

import { removeExistingCollageImage, createCollagePathElement, getResizedImagePath, getEmptyPath } from '../../utils/elements'

import { Collages, API_URL } from '../../utils/constants'
import { uploadPhoto } from '../../Api';
import { closePopUp } from '../../utils/functions';


class UploadImageComponent extends Component {

    addImagesToCanvas = images => {

        const image = images[0]

        const { editor } = this.props

        let { elements } = editor

        uploadPhoto(image.file)
            .then(
                ({ filename }) => {

                    const dataUrl = `${API_URL}/uploads/${filename}`

                    if ( editor.collage ) {

                        let pathId = editor.selectedPath || ''
            
                        if (pathId) {
            
                            elements = removeExistingCollageImage(editor.selectedPath, elements)
            
                        } else {
            
                            const selectedCollage = Collages.find(collage => collage.id == editor.collage)
                            
                            const emptyPathId = getEmptyPath(selectedCollage, elements)
            
                            pathId = emptyPathId
            
                            if (!emptyPathId) {
                                return
                            }
            
                        }
            
                        const resizedImage = getResizedImagePath(image, pathId)
            
                        const pathElement = createCollagePathElement(pathId, {...image, ...resizedImage, dataUrl})
            
                        const modifiedElements = elements.concat(pathElement)
            
                        this.props.mutateStore({
                            elements: modifiedElements,
                            activeElementIndex: (modifiedElements.length - 1),
                            activeElementDims: {
                                width: resizedImage.width,
                                height: resizedImage.height,
                                __typename: 'activeElementDims'
                            }
                        })
            
                    } else {
            
                        const element = createImage(image.width, image.height, dataUrl)
            
                        this.props.mutateStore({ elements: elements.concat(element) })
            
                    }

                    closePopUp()


                    

                }
            )
        
    }

    render () {

        const {
            WrappedComponent,
            WrappedProps
        } = this.props

        return (
            <ImageUploader style={{ border: 'none' }} onImageUpload={this.addImagesToCanvas}>
                <WrappedComponent
                    {...WrappedProps}
                />
            </ImageUploader>
        )

    }

}

const UploadImageConnected = WithEditor(UploadImageComponent)
const UploadImageMutationConnected = MutateEditor(UploadImageConnected)

const UploadImageHOC = WrappedComponent => props => <UploadImageMutationConnected
    WrappedComponent={WrappedComponent}
    WrappedProps={props}
/>

export default UploadImageHOC;