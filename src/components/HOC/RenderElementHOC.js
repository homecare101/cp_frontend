import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { SvgElement, SvgElementText, SvgMarkupElement } from '../Editor/EditorTools/SvgElement';
import { MovableSvgElement, MovableMarkup, MovableSvgText } from '../Editor/EditorTools/Movable';

const SvgText = SvgElementText()
const SvgImage = SvgMarkupElement()

export const RenderElementHOC = element => {

    switch (element.type) {

        case 'text':
        
            return SvgText
        
        case 'image':

            // let { transform, rotationAngle = 0, rotationWidth = 0, rotationHeight = 0 } = element.props

            // transform = `${transform || ''} rotate(${rotationAngle} ${rotationWidth} ${rotationHeight})`

            // const newProps = {
            //     ...element.props,
            //     transform
            // }

            // const SvgImage = SvgMarkupElement(ReactDOMServer.renderToStaticMarkup(<image {...newProps} />), true)
            return SvgImage

    }

}

const MovableText = MovableSvgText()
const MovableImage = MovableMarkup()

export const RenderMovableElementHOC = element => {

    switch (element.type) {

        case 'text':
            return MovableText
        
        case 'image':
            return MovableImage

    }

}