import React from 'react'
import { Container, Row, Col, Button } from 'reactstrap';
import './footer.css'
import { Link } from 'react-router-dom';


class Footer extends React.Component {
    render() {
        return (
            <Container fluid>
                <Row>
                    <Col md={12} className="footer-main">
                        <div className="footer-out">
                            <Col md={3}>
                                <span className="footer-logo">
                                    Logo
                                </span>
                            </Col>
                            <Col md={3}>
                                <div className="footer-text">
                                    <div>
                                        <Button color="link">Design your case</Button>
                                    </div>
                                    <div>
                                        <Button color="link">Our stock</Button>
                                    </div>
                                    <div>
                                      <Link to="/contactus"><Button color="link">Contact us</Button></Link>
                                    </div>
                                    <div>
                                      <Link to="/aboutus"> <Button color="link">About us</Button></Link> 
                                    </div>
                                </div>
                            </Col>
                            <Col md={3}>
                                <div className="footer-text">
                                    <div>
                                        <Button color="link">Account</Button>
                                    </div>
                                    <div>
                                        <Button color="link">Terms & Conditions</Button>
                                    </div>
                                    <div>
                                        <Button color="link">Your privacy</Button>
                                    </div>
                                    <div>
                                        <Button color="link">Gift Cards</Button>
                                    </div>
                                </div>
                            </Col>
                            <Col md={3} className="footer-logo-right">
                                <span>
                                    Copyright 2018 © Custom Park
                                </span>
                            </Col>
                            {/* <Col sm={12} md={{ size: 8, offset: 2 }}>
                                <img src="bitmap.png" className="footer-image"></img>
                            </Col> */}
                        </div>
                    </Col>
                </Row>
            </Container>
        )
    }
}
export default Footer