import React from 'react'
import './MobileList.css'
import { Col, Row } from 'reactstrap';
import { Link } from 'react-router-dom'
import { withRouter } from 'react-router';
import { withApollo } from 'react-apollo';
import ReactDOM from 'react-dom';

class MobileList extends React.Component {
    constructor() {
        super()
    }
    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    };
    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    };
    onImageClick(id) {
        // this.props.history.push(`/oneCase/` + id)
        this.props.caseId(id)
    }
    scrolled(event) {
        if (ReactDOM.findDOMNode(this).offsetHeight + ReactDOM.findDOMNode(this).scrollTop == ReactDOM.findDOMNode(this).scrollHeight) {
            this.props.changePageNumber(this.props.page)
        }
    }
    render() {
        return (
            <div className="mobileConHeight" id="scrollStock" onScroll={this.scrolled.bind(this)}>
                <Row className="mobileCon">
                    {this.props.data && this.props.data.length > 0 ? this.props.data.map((val, index) => {
                        return (
                            <div key={index} className="stock-data" onClick={this.onImageClick.bind(this, val._id)}>
                                <div className="stock-img-out">
                                    <img src={val.preview} className="stock-image" />
                                </div>
                                <div className="title-out">
                                    <p className="stock-img-title">{val.name}</p>
                                    <p className="stock-img-title-price">19,000원</p>
                                </div>
                            </div>
                        )
                    }) :
                    <div className="noDataContainerOut">
                        <h2 className="noDataStyle">데이터가 없습니다</h2>
                    </div>
                    }
                </Row>
            </div>
        )
    }
}
export default withRouter(withApollo(MobileList));