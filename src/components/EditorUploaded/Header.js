import React from 'react'; 
import './Uploaded.css';

class Header extends React.Component {
  render() {
    return (
      <div>
         <div className="logo">
     		   <h3>Logo</h3>
            <ul>
               <li><a href="">Upload photo</a></li>
               <li><a href="">Collage</a></li>
               <li><a href="">Material</a></li>
               <li><a href="">Text</a></li>
               <li><a href="">Background</a></li>
               <li><a href="">Cliparts</a></li>
            </ul>
        </div>
      </div>
    );
  }
}
export default Header;