import React from 'react'
import './multidropdown.css'

class MultiDropdown extends React.Component {
    constructor() {
        super()
        this.toggle = this.toggle.bind(this);
        this.state = {
            title: 'Select',
            showList: false,
            data: [
                {
                    brand: 'Iphone',
                    model: [
                        {
                            name: 'iphone4'
                        },
                        {
                            name: 'iphone5'
                        },
                        {
                            name: 'iphone6'
                        }
                    ]
                }, {
                    brand: 'Samsung',
                    model: [
                        {
                            name: 's7'
                        },
                        {
                            name: 's5'
                        },
                        {
                            name: 'galaxy'
                        }
                    ]
                }, {
                    brand: 'Nokia',
                    model: [
                        {
                            name: '3310'
                        },
                        {
                            name: 'n72'
                        },
                        {
                            name: '5510'
                        }
                    ]
                }
            ]
        }
    }
    onSelectmodel(data) {
        this.setState({ title: data, showList: false })
    }
    toggle() {
        var { showList } = this.state.showList
        this.setState(prevState => ({
            showList: !prevState.showList
        }));
    }
    render() {
        var { showList } = this.state.showList
        return (
            <div>
                <ul class="top-level-menu" onMouseLeave={()=>{this.setState({showList:false})}}>
                    <li>
                        <p onClick={this.toggle} className="title">{this.state.title}</p>
                        {this.state.showList ?
                            <ul class="second-level-menu" onMouseLeave={()=>{this.setState({showList:false})}}>
                                {this.state.data.map((val) => {
                                    return (
                                        <li>
                                            <a>{val.brand}</a>
                                            <ul class="third-level-menu">
                                                {val.model.map((item) => {
                                                    return (
                                                        <li><a onClick={this.onSelectmodel.bind(this, item.name)}>{item.name}</a></li>
                                                    )
                                                })}
                                            </ul>
                                        </li>
                                    )
                                })}
                            </ul>
                            : null}
                    </li>
                </ul>
            </div>
        )
    }
}
export default MultiDropdown