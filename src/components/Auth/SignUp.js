import React from 'react';
import { Button, Input, Form, FormGroup } from 'reactstrap';
import { withRouter } from 'react-router';
import './Auth.css';
import { IsValidForm } from '../common/validation'
import { signUpMutation } from '../../Graphql/Mutations/auth';
import { ApolloProvider, Query, graphql, compose } from 'react-apollo';
import { showLoader, stopLoader } from '../../utils/functions';
import { checkCartMutation } from '../../Graphql/Mutations/cart';
import { setToast } from '../../actions/editor';
import { AuthModalConsumer } from './AuthModalProvider/AuthModalProvider';

class SignUp extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			signUpForm: {
				confirmPassword: '',
				email: '',
				password: '',
			},
			errors: {},
			errorInd: false,
			errorRes:''
		};
		this.handleChange = this.handleChange.bind(this);
	}
	changeFormType(type) {
		this.props.changeFormType(type)
	}
	handleChange(key, event) {
		let value = event.target.value;
		let { signUpForm } = this.state
		signUpForm[key] = value
		this.setState({ signUpForm });
	}
	signUp(form) {
		let fields = ['email', 'password', 'confirmPassword']
		let formValidation = IsValidForm(fields, this.state.signUpForm)
		this.setState({ errors: formValidation.errors })
		if (formValidation.validate) {
			const { password, confirmPassword } = this.state.signUpForm
			if (password !== confirmPassword) {
				let { errors, errorInd } = this.state
				this.state.errors['confirmPassword'] = "Password and Confirm Password doesn't match"
				this.setState({ errors, errorInd: true })
			} else {
				showLoader()
				this.setState({ errorInd: false })
				let { email, password, confirmPassword } = this.state.signUpForm
				this.props.signUp({
						variables: {
							email: email,
							password: password,
						},
					}).then(response => {
						stopLoader()
						localStorage.setItem('token', response.data.signup.token)
						localStorage.setItem('id', response.data.signup._id)
						this.props.createCart({
							variables: {
								userId: response.data.signup._id,
							},
						}).then(res => {
							this.props.mutate({ variables: { type: 'success', message: "Account Has been Created" } })
							let cartdata = JSON.stringify(res.data.checkCart)
							localStorage.setItem('cartData', cartdata)
							stopLoader()
							this.props.closeModal()
						})
						.catch(error => {
							console.log(error, "error")
							let errorText = { ...error }
							if (errorText !== {}) {
							this.props.setToast({
								variables: { type: 'error', message: errorText.graphQLErrors[0].message }
							})
						}
						})
					
					}).catch(error => {
						stopLoader()
						let errorText = { ...error }
						console.log(errorText)
						if (errorText !== {}) {
							this.setState({ errorRes: errorText.graphQLErrors[0].message, errorInd: true })
						}
					})
			}
		} else {
			this.setState({ errorInd: true })
			// this.refs.toast.show('Some field are missing')
		}
	}
	handleFocus(key) {
		if (this.state.errorInd) {
			let { errors } = this.state;
			errors[key] = ''
			this.setState({ errors , errorRes:'' });
		}
	}
	render() {
		let { email, password, confirmPassword } = this.state.signUpForm
		const { errorInd, errors, errorRes } = this.state
		return (
			<div className="signup-modal-container">
				<div className="signup-modal-title"><span>가입하기</span></div>
				<div className="signup-modal-contentMain">
					<FormGroup>
						<Input onFocus={this.handleFocus.bind(this, 'email')} className="signUpInputBox" type="email" name="email" value={email} onChange={this.handleChange.bind(this, 'email')} placeholder="이메일" />
						{errorInd && <p className="hasError">
							{errors.email}
						</p>}
					</FormGroup>
					<FormGroup>
						<Input onFocus={this.handleFocus.bind(this, 'password')} className="signUpInputBox" type="password" name="password" value={password} onChange={this.handleChange.bind(this, 'password')} placeholder="비밀번호" />
						{errorInd && <p className="hasError">
							{errors.password}
						</p>}
					</FormGroup>
					<FormGroup>
						<Input onFocus={this.handleFocus.bind(this, 'confirmPassword')} className="signUpInputBox" type="password" name="confirmPassword" value={confirmPassword} onChange={this.handleChange.bind(this, 'confirmPassword')} placeholder="비밀번호 재입력" />
						{errorInd && <p className="hasErrorConfirm">
							{errors.confirmPassword}
						</p>}
						{errorInd && <p className="hasError">
							{errorRes}
						</p>}
					</FormGroup>
					<Button onClick={this.signUp.bind(this)} className="btnSignUpMain">로그인</Button>
				</div>
				<div className='signup-footer'>
					<div className='signup-lower-link'>
						<Button color="link" className="btnSignUpOptions" onClick={this.changeFormType.bind(this, 'Login')} ><u>로그인</u></Button>
					</div>
					<div className='signup-lower-link'>
						<Button color="link" className="btnSignUpOptions" onClick={this.changeFormType.bind(this, 'ForgotPassword')}><u>비밀번호 변경</u></Button>
					</div>
				</div>
			</div>
		)
	}
}

const ToastMutationConnected = graphql(setToast)(SignUp)

export default withRouter(compose(
	graphql(signUpMutation, { name: 'signUp' }),
	graphql( checkCartMutation, { name:'createCart' }),

)(ToastMutationConnected));


