import React, { Component, createContext } from 'react';
import { partial } from '../../../utils/functions';
import Main from '../Main';

const { Provider, Consumer } = createContext('ModalProvider');

export class AuthModalProvider extends Component {

    state = {
        open: false,
        formType: 'Login'
    }

    _toggleModal = open => this.setState({ open })

    _openModal = partial(this._toggleModal, true)
    _closeModal = partial(this._toggleModal, false)

    _setFormType = formType => this.setState({ formType })

    render () {

        const {
            open,
            formType
        } = this.state

        const {
            children
        } = this.props

        return (
            <Provider
                value={{
                    open,
                    formType,
                    openModal: this._openModal,
                    closeModal: this._closeModal,
                    setFormType: this._setFormType
                }}
            >
                {children}
            </Provider>
        )

    }

}

export class AuthModalConsumer extends Component {

    render () {

        const { children } = this.props

        return (
            <Consumer>
                {children}
            </Consumer>
        )

    }

}

export class AuthModal extends Component {

    onCloseModal = closeModal => () => {
        closeModal()
        
        if (this.props.closeModal) {
            this.props.closeModal()
        }

    }

    render () {

        return (

            <AuthModalConsumer>
                {
                    ({ open, closeModal, formType, setFormType }) =>
                        <Main setFormType={setFormType} formType={formType} toggleModal={open} closeModal={this.onCloseModal(closeModal)} />
                }
            </AuthModalConsumer>

        )

    }

}