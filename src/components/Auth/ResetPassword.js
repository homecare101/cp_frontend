import React, { Component } from 'react';
import { Button, Input, Form, FormGroup } from 'reactstrap';
import './Auth.css';
import { IsValidForm } from '../common/validation'
import { compose, graphql } from 'react-apollo';
import { resetMutaion } from '../../Graphql/Mutations/auth';
import { withRouter } from 'react-router';
import qs from 'querystring'

class ResetPassword extends Component {
	constructor(props) {
		super(props);
		this.state = {
			resetPassForm: {
				password: '',
				confirmPassword: ''
			},
			errorInd: false,
			errors: {},
			successMessage: '',
			errorRes: '',
			verficationCode: null
		};
	}

	componentDidMount () {
		const parsed = qs.parse(window.location.search);
		const verficationCode = parsed['?verificationCode']

        if (verficationCode) {
            this.setState({ verficationCode })
        }
	}

	changeFormType(type) {
		this.props.changeFormType(type)
	}
	handleFocus(key) {
		if (this.state.errorInd) {
			let { errors } = this.state;
			errors[key] = ''
			this.setState({ errors, errorRes: '' });
		}
	}
	resetPassword(form) {
		let fields = ['password', 'confirmPassword']
		let formValidation = IsValidForm(fields, this.state.resetPassForm)
		this.setState({ errors: formValidation.errors })
		if (formValidation.validate) {
			const { password, confirmPassword } = this.state.resetPassForm
			if (password !== confirmPassword) {
				let { errors, errorInd } = this.state
				this.state.errors['confirmPassword'] = "Password and Confirm Password doesn't match"
				this.setState({ errors, errorInd: true })
			} else {
				this.setState({ errorInd: false })
				this.props.resetPassword({
					variables: {
						verificationCode: this.state.verficationCode,
						password: password
					},
				}).then(res => {
					this.setState({ successMessage: res.data.resetPassword.message })
					setTimeout(() => {
						this.props.toggle()
						this.setState({ successMessage: '' })
						this.props.history.push('/')
					}, 2000);
				}).catch(error => {
					console.log({ ...error })
					let errorText = { ...error }
					if (errorText !== {}) {
						this.setState({ errorRes: errorText.graphQLErrors[0].message, errorInd: true })
					}
				})
			}
		} else {
			this.setState({ errorInd: true })
		}
		let { password, confirmPassword } = this.state.resetPassForm
	}
	handleChange(key, event) {
		let value = event.target.value;
		let { resetPassForm } = this.state
		resetPassForm[key] = value
		this.setState({ resetPassForm });
	}
	render() {
		let { password, confirmPassword } = this.state.resetPassForm
		const { errorInd, errors, successMessage, errorRes } = this.state

		return (
			<div className="signup-modal-container">
				<div className="signup-modal-title"><span>Reset Password</span></div>
				<div className="signup-modal-content">
					<Form>
						<FormGroup>
							<Input type="password" name="password"  className="loginTextBox" value={password} onChange={this.handleChange.bind(this, 'password')} placeholder="Password" onFocus={this.handleFocus.bind(this, 'password')} />
							{errorInd && <p className="hasError">
								{errors.password}
							</p>}
						</FormGroup>
						<FormGroup>
							<Input type="password" name="confirmPassword"  className="loginTextBox" value={confirmPassword} onChange={this.handleChange.bind(this, 'confirmPassword')} placeholder="Confirm Password" onFocus={this.handleFocus.bind(this, 'confirmPassword')} />
							{errorInd && <p className="hasErrorConfirm">
								{errors.confirmPassword}
							</p>}
							{successMessage !== '' && <p className="hasSuccess">
								{successMessage}
							</p>}
							{errorInd && errorRes && <p className="hasError">
								{errorRes}
							</p>}
						</FormGroup>
						<Button  className="btnLoginMain" onClick={this.resetPassword.bind(this)} block>Reset Password</Button>
					</Form>
				</div>
				<div className='signup-footer'>
					<div className='signup-lower-link'>
						<Button color="link" className="btnOptionsMore" onClick={this.changeFormType.bind(this, 'Login')} ><u>로그인</u></Button>
					</div>
					<div className='signup-lower-link'>
						<Button color="link" className="btnOptionsMore" onClick={this.changeFormType.bind(this, 'SignUp')}><u>가입하기</u></Button>
					</div>
				</div>
			</div>
		)
	}
}

export default withRouter(compose(
	graphql(resetMutaion, { name: 'resetPassword' })
)(ResetPassword));
