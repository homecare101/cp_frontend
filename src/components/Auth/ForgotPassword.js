import React, { Component } from 'react';
import { Button, Input, Form, FormGroup } from 'reactstrap';
import './Auth.css';
import { withRouter } from 'react-router';
import { IsValidForm } from '../common/validation';
import { compose, graphql } from 'react-apollo';
import { forgotMutaion } from '../../Graphql/Mutations/auth';
import { showLoader, stopLoader } from '../../utils/functions';
class ForgotPassword extends Component {
	constructor(props) {
		super(props);
		this.state = {
			email: '',
			errorInd: false,
			errors: {},
			successMessage: '',
			errorRes: '',
			errorInd: false
		};
	}
	handleChange(key, event) {
		let value = event.target.value;
		let { email } = this.state
		email = value
		this.setState({ email });
	}
	forgotPass() {
		let fields = ['email']
		let formValidation = IsValidForm(fields, this.state)
		this.setState({ errors: formValidation.errors })
		if (formValidation.validate) {
			showLoader()
			this.setState({ errorInd: false })
			let { email } = this.state
			this.props.forgotPassword({
				variables: {
					email: email,
				},
			}).then(res => {
				stopLoader()
				this.setState({ successMessage: res.data.forgotPassword.message })
				setTimeout(() => {
					this.props.toggle()
					this.setState({ successMessage: '' })
					// this.props.history.push('/')
				}, 2000);
				this.props.closeModal()
				this.props.history.push('/')
			})
				.catch(error => {
				stopLoader()
					let errorText = { ...error }
					if (errorText !== {}) {
						this.setState({ errorRes: errorText.graphQLErrors[0].message, errorInd: true })
					}
				})
			//	this.props.toggle()
			//	})
		} else {
			this.setState({ errorInd: true })
		}
	}
	changeFormType(type) {
		this.props.changeFormType(type)
	}
	handleFocus(key) {
		if (this.state.errorInd) {
			let { errors } = this.state;
			errors[key] = ''
			this.setState({ errors });
		}
	}
	render() {
		let { email } = this.state
		const { errorInd, errors, successMessage, errorRes } = this.state
		return (
			<div className="forgot-modal-container">
				<div className="signup-modal-title"><span>비밀번호 변경</span></div>
				<div className="forgot-modal-content">
						<Input  onFocus={this.handleFocus.bind(this, 'email')} className="forgotInput" type="email" name="email" value={email} onChange={this.handleChange.bind(this, 'email')} placeholder="이메일" />
							{errorInd && <p className="hasError">
								{errors.email}
							</p>}
							{errorInd && errorRes && <p className="hasError">
								{errorRes}
							</p>}
							{successMessage !== '' && <p className="hasSuccess">
								{successMessage}
							</p>}
						<Button onClick={this.forgotPass.bind(this)} className="btnForgot">비밀번호 변경</Button>
				</div>
				<div className='forgot-footer'>
					<div className='forgot-lower-link-first'>
						<Button color="link" className="btnForgotOptions" onClick={this.changeFormType.bind(this, 'Login')}><u>로그인</u></Button>
					</div>
					<div className='forgot-lower-link-second'>
						<Button color="link" className="btnForgotOptions" onClick={this.changeFormType.bind(this, 'SignUp')}><u>가입하기</u></Button>
					</div>
				</div>
			</div>
		)
	}
}
export default withRouter(compose(
	graphql(forgotMutaion, { name: 'forgotPassword' })
)(ForgotPassword));
