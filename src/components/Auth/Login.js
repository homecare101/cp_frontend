import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { Button, Input, Form, FormGroup, Col } from 'reactstrap';
import './Auth.css';
import { IsValidForm } from '../common/validation'
import FacebookLogin from 'react-facebook-login';
import KakaoLogin from 'react-kakao-login'
import { withApollo, compose, graphql, Query } from 'react-apollo';
import { showLoader, stopLoader } from '../../utils/functions';
import { checkCartMutation } from '../../Graphql/Mutations/cart';
import { setToast } from '../../actions/editor';
import { loginMutation, socialLoginMutation } from '../../Graphql/Query/auth';

class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {
			modal: false,
			loginForm: {
				email: '',
				password: ''
			},
			errorInd: false,
			errors: {},
			errorRes: '',
			fbBtn: true,
		};
		this.handleChange = this.handleChange.bind(this);
	}
	toggle() {
		this.props.toggle()
	}

	changeFormType(type) {
		this.props.changeFormType(type)
	}
	handleChange(key, event) {
		let value = event.target.value;
		let { loginForm } = this.state
		loginForm[key] = value
		this.setState({ loginForm });
	}
	handleFocus(key) {
		if (this.state.errorInd) {
			let { errors } = this.state;
			errors[key] = ''
			this.setState({ errors, errorRes: '' });
		}
	}
	login() {
		let fields = ['email', 'password']
		let formValidation = IsValidForm(fields, this.state.loginForm)
		this.setState({ errors: formValidation.errors })
		if (formValidation.validate) {
			showLoader()
			this.setState({ errorInd: false })
			let { email, password } = this.state.loginForm
			this.props.client.query({
				query: loginMutation,
				variables: {
					email: email,
					password: password,
				}
			}).then(response => {
				stopLoader()
				localStorage.setItem('token', response.data.login.token)
				localStorage.setItem('id', response.data.login._id)
				localStorage.setItem('userRole', response.data.login.role)
				// create cart
				this.props.createCart({
					variables: {
						userId: response.data.login._id
					},
				}).then(res => {
					let cartdata = JSON.stringify(res.data.checkCart)
					localStorage.setItem('cartData', cartdata)
					this.props.closeModal()
					if (response.data.login.role === 'Admin') {
						this.props.history.push('/admin')
					}
					this.props.setToast({ variables: { type: 'success', message: "Login successfully" } })
				})
					.catch(error => {
						console.log(error, "error 	")
						let errorText = { ...error }
						if (errorText !== {}) {
							this.props.setToast({
								variables: { type: 'error', message: "Some error occur please try again later" }
							})
						}
					})
			}).catch(error => {
				stopLoader()
				let errorText = { ...error }
				if (errorText !== {}) {
					this.setState({ errorRes: errorText.graphQLErrors[0].message, errorInd: true })
				}
			})
		} else {
			this.setState({ errorInd: true })
		}
	}
	responseFacebook(data) {
		if (!this.state.fbBtn) {
			showLoader()
			this.props.client.query({
				query: socialLoginMutation,
				variables: {
					provider: 'facebook',
					providerId: data.id,
					firstName: data.name,
					email: data.email
				}
			}).then((response) => {
				stopLoader()
				localStorage.setItem('token', response.data.socialLogin.token)
				localStorage.setItem('id', response.data.socialLogin._id)
				localStorage.setItem('userRole', 'User')
				/////cart
				this.props.createCart({
					variables: {
						userId: response.data.socialLogin._id,
					},
				}).then(res => {
					let cartdata = JSON.stringify(res.data.checkCart)
					localStorage.setItem('cartData', cartdata)
					this.props.closeModal()
				})
				.catch(error => {
					stopLoader()
					console.log(error, "error")
					let errorText = { ...error }
					if (errorText !== {}) {
						this.props.setToast({
							variables: { type: 'error',   message: "Some error occur please try again later" }
						})
					}
				})
			})
		}
	}
	responseKakao = (data) => {
		showLoader()
		this.props.client.query({
			query: socialLoginMutation,
			variables: {
				provider: 'Kakao',
				providerId: data.profile.id,
				email: data.profile.kakao_account.email
			}
		}).then((response) => {
			stopLoader()
			console.log(response.data.socialLogin,"09099")
			localStorage.setItem('token', response.data.socialLogin.token)
			localStorage.setItem('id', response.data.socialLogin._id)
			localStorage.setItem('userRole', 'User')
			/////cart
			this.props.createCart({
				variables: {
					userId: response.data.socialLogin._id,
				},
			}).then(res => {
				console.log(res,"resss")
				let cartdata = JSON.stringify(res.data.checkCart)
				localStorage.setItem('cartData', cartdata)
				this.props.closeModal()
			})
			.catch(error => {
				stopLoader()
				console.log(error, "error")
				let errorText = { ...error }
				if (errorText !== {}) {
					this.props.setToast({
						variables: { type: 'error', message: "Some error occur please try again later" }
					})
				}
			})
		})
	}
	fbLogin() {
		this.setState({ fbBtn: false })
	}
	render() {
		let { email, password } = this.state.loginForm
		const { errorInd, errors, errorRes } = this.state
		return (
			<div className="login-modal-container">
				<div className="login-modal-title"><span>로그인</span></div>
				<div className="signup-modal-content">
					<Form>
						<FormGroup>
							<Input onFocus={this.handleFocus.bind(this, 'email')} className="loginTextBox" type="email" name="email" value={email} onChange={this.handleChange.bind(this, 'email')} placeholder="이메일" />
							{errorInd && <p className="hasError">
								{errors.email}
							</p>}
						</FormGroup>
						<FormGroup>
							<Input onFocus={this.handleFocus.bind(this, 'password')} className="loginTextBox" type="password" name="password" value={password} onChange={this.handleChange.bind(this, 'password')} placeholder="비밀번호" />
							{errorInd && <p className="hasError">
								{errors.password}
							</p>}
							{errorInd && errorRes && <p className="hasError">
								{errorRes}
							</p>}
						</FormGroup>
						<Button onClick={this.login.bind(this)} className="btnLoginMain" block>로그인</Button>
					</Form>
				</div>

				<div className='signup-footer'>
					<div className='signup-lower-link'>
						<Button color="link" className="btnOptionsMore" onClick={this.changeFormType.bind(this, 'SignUp')} ><u>가입하기</u></Button>
					</div>
					<div className='signup-lower-link'>
						<Button color="link" className="btnOptionsMore" onClick={this.changeFormType.bind(this, 'ForgotPassword')}><u>비밀번호 변경</u></Button>
					</div>
				</div>

				<div className="borderMain">
					<span>or</span>
				</div>


				<div className="login-social">
						<FacebookLogin
							appId="2223097681273076"
							autoLoad={true}
							onClick={this.fbLogin.bind(this)}
							fields="name,email,picture"
							callback={this.responseFacebook.bind(this)}
							cssClass="facebook-button"
							icon={<img className="facebookIcon" src="/fb@3x.png" />}
							textButton="페이스북 로그인"
						/>
						<KakaoLogin
							jsKey="58f740d966f8b850c550f04613177818"
							onSuccess={(res) => { this.responseKakao(res) }}
							getProfile={true}
							className="kakao-button"
							buttonText="카카오 로그인"
						/>
				</div>
			</div>
		)
	}
}
export default withRouter(compose(
	withApollo,
	[graphql(checkCartMutation, { name: 'createCart' }),
	graphql(setToast, { name: 'setToast' }),]
)(Login))



////d6a40e2838e0b416068d9eb43f68772b kakaoooo