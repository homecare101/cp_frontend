import React, { Component } from 'react';
import { withRouter } from 'react-router';
import SignUp from '../../components/Auth/SignUp'
import Login from '../../components/Auth/Login'
import ForgotPassword from '../../components/Auth/ForgotPassword'
import ResetPassword from '../../components/Auth/ResetPassword'
import {
    Modal,
} from 'reactstrap';

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            token: '',
            // modal: true,
            formType: 'Login',
            verficationCode: '',
            isOpenNav: false,
            CartOpen: false,
            label:'logo',
            showCart:true
        };
    }
    toggle() {
        this.setState({ isOpenNav: !this.state.isOpenNav });
    }
    loginToggle() {
        this.props.closeModal(false)
    }
    changeFormType(type) {
        this.props.setFormType(type)
    }
    renderForm() {
        if (this.props.formType == 'Login') {
            return <Login changeFormType={this.changeFormType.bind(this)} closeModal={this.props.closeModal} toggle={this.loginToggle.bind(this)} />
        } else if (this.props.formType == 'SignUp') {
            return <SignUp changeFormType={this.changeFormType.bind(this)} closeModal={this.props.closeModal}  toggle={this.loginToggle.bind(this)} />
        } else if (this.props.formType == 'ForgotPassword') {
            return <ForgotPassword changeFormType={this.changeFormType.bind(this)} closeModal={this.props.closeModal}  toggle={this.loginToggle.bind(this)} />
        } else if (this.props.formType == 'ResetPassword') {
            return <ResetPassword changeFormType={this.changeFormType.bind(this)} closeModal={this.props.closeModal}  toggle={this.loginToggle.bind(this)} />
        }
    }
    componentWillMount() {
        let { token } = this.state
        token = localStorage.getItem('token')
        if (token != '') {
            this.setState({ token })
        }
        if (this.props.label) {
            this.setState({label:"Admin Pannel", showCart:false})
        }
    }
    render() {
        return (
            <div>
                <Modal isOpen={this.props.toggleModal} centered className="home-auth-modal">
                    <div>
                        <div className='home-modal-close'>
                            <button type="button" onClick={this.loginToggle.bind(this)} className="home-modal-close-buttonMain" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                      {  this.renderForm() }
                    </div>
                </Modal>
            </div>
        )
    }
}
export default withRouter((Main));
