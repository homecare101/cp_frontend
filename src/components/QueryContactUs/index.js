
import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import './QueryContactUs.css'
class QueryContactUs extends Component {
    render() {
        return (
            <Container fluid>
                <Row>
                    <Col md={12} className="no-padding">
                        <div className="aboutus-bottom-con">
                            <h1 className="aboutus-con-heading">Have questions?</h1>
                            <div className="aboutus-block">
                                <Col md={4}>
                                    <div className="aboutus-block-content">
                                        <div className="aboutus-icon-out"><i className="zmdi zmdi-phone aboutus-icon"></i></div>
                                        <p>033 456 7890</p>
                                        <p><u>Callback</u></p>
                                    </div>
                                </Col>
                                <Col md={4}>
                                    <div className="aboutus-block-content">
                                        <div className="aboutus-icon-out"><i className="zmdi zmdi-instagram aboutus-icon"></i></div>
                                        <p>Follow us</p>
                                        <p>#customPark</p>
                                    </div>
                                </Col>
                                <Col md={4}>
                                    <div className="aboutus-block-content">
                                        <div className="aboutus-icon-out"><i className="zmdi zmdi-email-open aboutus-icon"></i></div>
                                        <p>Email us</p>
                                        <p><u>hello@custompark.com</u></p>
                                    </div>
                                </Col>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
        )
    }

}
export default QueryContactUs