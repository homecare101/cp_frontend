import React from 'react'
import './MenuOption.css'
import { Button, Container, Row, Col, FormGroup, Label, Input } from 'reactstrap';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom'
import { getMaterialsQuery, getModelsQuery, getThemesQuery, getColorsQuery, getBrandsQuery } from '../../Graphql/Query/stocks';

class MenuDrawer extends React.Component {
    constructor() {
        super()
        this.state = {
            materials: [],
            models: [],
            themes: [],
            colors: [],
            brands: [],
            items: [],
            SelectedColorId:''
        }
    }
    componentDidMount() {
        ///////materials
        this.props.client.query({
            query: getMaterialsQuery,
        }).then(res => {
            let materialData = res.data.getMaterials.list
            this.setState({ materials: materialData })
        })
            .catch(error => {
                console.log(error, "error")
            })
        /////model
        this.props.client.query({
            query: getModelsQuery,
        }).then(res => {
            let modelsData = res.data.getModels.list
            this.setState({ models: modelsData })
        })
            .catch(error => {
                console.log(error, "error")
            })
        /////themes
        this.props.client.query({
            query: getThemesQuery,
        }).then(res => {
            let themedata = res.data.getThemes.list
            this.setState({ themes: themedata })
        })
            .catch(error => {
                console.log(error, "error")
            })
        /////color
        this.props.client.query({
            query: getColorsQuery,
        }).then(res => {
            let colorsData = res.data.getColors.list
            this.setState({ colors: colorsData })
        })
            .catch(error => {
                console.log(error, "error")
            })
        /////brands
        this.props.client.query({
            query: getBrandsQuery,
        }).then(res => {
            let brandData = res.data.getBrands.list
            this.setState({ brands: brandData })
        })
            .catch(error => {
                console.log(error, "error")
            })
    }
    changeFilter(type, event) {
        this.props.changeFilter(type, event)
    }
    clickFilter(type, value) {
        this.props.clickFilter(type, value)
    }
    onSelectColor(id){
        this.setState({ SelectedColorId:id })
    }
    render() {
        const { materials, models, themes, colors, brands } = this.state
        return (
            <div className="menu-form-content-out">
                <FormGroup className="menu-form-content">
                    <div className="stock-heading-icon-out">
                        <div className="stock-heading-icon"><img src="./majic.png" className="case-image" /></div>
                        <Link to='/editor'><span><u>나만의 케이스 디자인 하기</u></span></Link>
                    </div>
                    <Label>제조사</Label>
                    <Input type="select" name="selectMulti" className="menu-dropdown" value={this.state.selectedBrand} onChange={this.changeFilter.bind(this, 'brand')}>
                        <option default value="">All</option>
                        {brands.length > 0 ?
                            brands.map((val, index) => {
                                return <option value={val.name}>{val.name}</option>
                            })
                            :
                            <option disabled>No brands available</option>
                        }
                    </Input>
                    <Label>모델명</Label>
                    <Input type="select" name="selectMulti" className="menu-dropdown" value={this.state.selectedModel} onChange={this.changeFilter.bind(this, 'model')}>
                        <option default value="">All</option>
                        {models.length > 0 ?
                            models.map((val, index) => {
                                return <option>{val.name}</option>
                            })
                            :
                            <option disabled>No models available</option>
                        }
                    </Input>
                    <Label>케이스 종류</Label>
                    <Input type="select" name="selectMulti" className="menu-dropdown" value={this.state.selectedModel} onChange={this.changeFilter.bind(this, 'material')}>
                        <option default value="">All</option>
                        {materials.length > 0 ?
                            materials.map((val, index) => {
                                return <option>{val.name}</option>
                            })
                            :
                            <option disabled>No Material available</option>
                        }
                    </Input>
                </FormGroup>
                <FormGroup className="menu-form-color">
                    <Label>색상</Label>
                    <div className="menu-form-color">
                        {
                            colors.map((item, index) => {
                                return (
                                    <label className="radio-out" onClick={this.onSelectColor.bind(this, item._id)}>
                                        <input className="radio" type="radio" name="question_1_2" value="black" onClick={this.clickFilter.bind(this, 'color', item.value)} />
                                        { this.state.SelectedColorId == item._id ? 
                                            
                                       <div className="colorOutMain">
                                            <p className="colorOutTick">&#x2714;</p>
                                           <span style={{ backgroundColor: item.code, display: 'block', width: 24, height: 24, borderRadius: '100%'}}></span>
                                       </div> 
                                        : 
                                        <span style={{ backgroundColor: item.code, display: 'block', width: 25, height: 25, borderRadius: '100%' }}></span>
                                        }
                                    </label>
                                )
                            })
                        }
                    </div>
                </FormGroup>
                <h2 className="headingThemes">테마 필터</h2>
                <div className="stock-list">
                    {
                        themes.map((item, index) => {
                            return (
                                <Row onClick={this.clickFilter.bind(this, 'theme', item.name)}>
                                <div className="stock-checkbok-out">
                                    <label className="diplayRowThemes">
                                        <input className="checkbox" type="checkbox" name="vehicle1" value="Bike" />
                                        <p className="stock-tick-out">{item.name}</p>
                                    </label>
                                    <div className="stock-float-right">
                                        <p>1</p>
                                    </div>
                                </div>
                                </Row>
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}
export default withRouter(withApollo(MenuDrawer));