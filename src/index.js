import React from 'react';
import { render } from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import App from './containers/App';
import client from './client'

import './styles/global.css'

const target = document.querySelector('#root')

render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  target
)