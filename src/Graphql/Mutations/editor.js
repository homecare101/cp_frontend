import gql from "graphql-tag";

export const editorMutaion = gql`
  mutation saveCase(
    $caseData: String,
    $name: String,
    $userId: String,
    $preview: String,
    $count: Int,
    $price: Int,
    $material:String,
    $model:String,
    $outlineSvgData: String
  ) {
    saveCase(
        caseData: $caseData,
        name: $name,
        userId: $userId,
        preview: $preview
        count: $count
        price: $price
        material: $material
        model: $model
        outlineSvgData: $outlineSvgData
    ){
       caseData
       name
       preview
       count
       price
       model
       material
    }
  }
`;



export const editCaseMutation = gql`
  mutation editCase(
    $caseId: String,
    $caseData: String,
    $name: String,
    $userId: String,
    $preview: String,
    $material:String,
    $model:String,
    $outlineSvgData: String
  ) {
    editCase(
        caseId: $caseId
        caseData: $caseData,
        name: $name,
        userId: $userId,
        preview: $preview
        material: $material
        model: $model
        outlineSvgData: $outlineSvgData
    ){
      message
    }
  }
`;


