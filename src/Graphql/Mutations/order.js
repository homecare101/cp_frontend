import gql from "graphql-tag";

export const saveOrderMutation = gql`
  mutation saveOrder(
    $status: String
    $userId: String,
    $firstName: String,
    $lastName: String,
    $mobileNumber: String,
    $email: String,
    $orderNotes: String,
    $createdAt: String,
    $cartId: String,
    $extraAddress: String,
    $fullAddress: String,
    $zonecode: String,
    $extraDetails: String,
    $subTotal: String
    $items: [CaseToSend!]!
  ) {
    saveOrder(
        userId: $userId,
        status: $status,
        firstName: $firstName,
        lastName: $lastName,
        mobileNumber: $mobileNumber,
        email: $email,
        orderNotes: $orderNotes,  
        createdAt: $createdAt,
        cartId: $cartId,
        extraAddress: $extraAddress,
        fullAddress: $fullAddress,
        zonecode: $zonecode,
        extraDetails: $extraDetails,
        subTotal: $subTotal
        items: $items
        ){
           message
    }
  }
`;
