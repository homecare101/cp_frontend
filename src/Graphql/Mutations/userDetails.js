import gql from "graphql-tag";

export const addressMutaion = gql`
  mutation addAddress(
    $userId: String,
    $extraAddress: String,
    $fullAddress: String,
    $zonecode: String,
  ) {
    addAddress(
        userId: $userId,
        extraAddress: $extraAddress,
        fullAddress: $fullAddress,
        zonecode: $zonecode,
       ){
          _id
          userId
          extraAddress
          fullAddress
          zonecode
    }
  }
`;

export const editAddressMutation = gql`
mutation editAddress(
  $addressId: String
  $userId: String,
  $extraAddress: String,
  $fullAddress: String,
  $zonecode: String,
) {
  editAddress(
      userId: $userId,
      addressId: $addressId,
      extraAddress: $extraAddress,
      fullAddress: $fullAddress,
      zonecode: $zonecode,
     ){
      message
  }
}
`;

export const addPaymentMutaion = gql`
  mutation addPaymentMethod(
    $userId: String,
    $accNo: String,
    $date: String,
    $selected: Boolean,
  ) {
    addPaymentMethod(
        userId: $userId,
        accNo: $accNo,
        date: $date,
        selected: $selected,
    ){
        _id
        userId
        accNo
        date
        selected
    }
  }
`;

export const addToCartMutaion = gql`
  mutation addToCart(
    $userId: String,
    $items: [CaseToSend!]!,
    $subTotal: Int,
    $cartId: String
  ) { 
    addToCart(
        userId: $userId,
        items: $items,
        subTotal: $subTotal,
        cartId: $cartId
    ){
        message
    }
  }
`;

export const editUserDetailsMutation = gql`
mutation editUser(
      $userId: String
      $email: String
      $lastName: String
      $firstName: String
      $userName: String
      $gender: String
      $phone: String

    ) {
      editUser(
        userId: $userId
        email: $email
        lastName: $lastName
        firstName: $firstName
        userName: $userName
        gender: $gender
        phone: $phone
    ){
        _id
        email
        firstName
        lastName
        provider
        address
        userName
        gender
        phone
        providerId
        role
        token
    }
  }
`;
export const changeUserPasswordMutation = gql`
mutation changeUserPassword(
      $userId: String
      $password: String
      $newPassword : String

    ) {
      changeUserPassword(
        userId: $userId
        password: $password
        newPassword: $newPassword
    ){
       message
    }
  }
`;