import gql from "graphql-tag";

export const editStockCaseMutation = gql`
  mutation editStockCase(
        $userId: String
        $caseId : String
        $count: Int
        $price: Int
        $name: String
        $preview: String
        $brand: String
        $model: String
        $material: String
        $colors: [colorToSend!]!
        $theme: String
  ) {
    editStockCase(
            userId: $userId
            caseId: $caseId
            count: $count
            price: $price
            name: $name
            preview: $preview
            brand: $brand
            model: $model
            material: $material
            colors: $colors
            theme: $theme

    ){
        message
    }
  }
`;

export const addStockMutation = gql`
  mutation addStockCase(
        $userId: String
        $count: Int
        $price: Int
        $name: String
        $preview: String
        $brand: String
        $model: String
        $material: String
        $colors: [colorToSend!]!
        $theme: String
  ) {
    addStockCase(
            userId: $userId
            count: $count
            price: $price
            name: $name
            preview: $preview
            brand: $brand
            model: $model
            material: $material
            colors: $colors
            theme: $theme

    ){
        message
    }
  }
`;