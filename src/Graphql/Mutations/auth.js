import gql from "graphql-tag";

export const signUpMutation = gql`
  mutation signup(
    $email: String,
    $password: String,
    $lastName: String,
    $firstName: String,
  ) {
    signup(
        email: $email,
        password: $password,
        lastName: $lastName,
        firstName: $firstName,
    ){
      email
      token
      _id
      role
    }
  }
`;


export const forgotMutaion = gql`
  mutation forgotPassword(
    $email: String
  ) {
    forgotPassword(
        email: $email,
    ){
      message
    }
  }
`;

export const resetMutaion = gql`
  mutation resetPassword(
    $verificationCode: String
    $password: String
  ) {
    resetPassword(
      verificationCode: $verificationCode
      password: $password
    ){
      message
    }
  }
`;
export const contactUsMutaion = gql`
  mutation contactUs(
    $name: String
    $email: String
    $message: String
  ) {
    contactUs(
      name: $name
      email: $email
      message: $message

    ){
      message
    }
  }
`;