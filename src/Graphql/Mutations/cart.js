import gql from "graphql-tag";


export const checkCartMutation = gql`
  mutation checkCart(
    $userId: String,
  ) {
    checkCart(
        userId: $userId,
    ){
        _id
        subTotal
        items{
          _id
          preview
          name
          count
          caseData
          price
          brand
          model
          material
          theme
          outlineSvgData
        }
        userId

    }
  }
`;
