import gql from "graphql-tag";

export const getAddressMutation = gql`
query getAddress(
  $userId: String,
  ) {
    getAddress(
      userId: $userId,
  ){
  addresses{ 
      _id
      userId
      fullAddress
      zonecode
      extraAddress
    }
  }
}
`;

export const getPaymentMutation = gql`
  query getPaymentMethod(
    $userId: String,
    ) {
      getPaymentMethod(
        userId: $userId,
    ){
      paymentMethod{ 
        _id
        userId
        accNo
        date
        selected
    }
    }
  }
`;



export const deletePaymentMutation = gql`
  query deletePaymentMethod(
    $paymentId: String,
    $userId: String,
    ) {
      deletePaymentMethod(
        paymentId: $paymentId,
        userId: $userId,
    ){
      message
    }
  }
`;

export const deleteAddressQuery = gql`
  query deleteAddress(
    $addressId: String,
    $userId: String,
    ) {
      deleteAddress(
        addressId: $addressId,
        userId: $userId,
    ){
      message
    }
  }
`;

export const getOrderDataMutation = gql`
  query getOrderData(
      $userId: String,
    ) {
      getOrderData(
        userId: $userId,
    ){
      order{
          _id
          status
          userId
          firstName
          lastName
          mobileNumber
          email
          orderNotes
          createdAt
          cartId
          subTotal
          extraAddress
          fullAddress
          zonecode
          extraDetails
          items{
            _id
            preview
            name
            count
            caseData
          }
        }
    }
  }
`;


export const getCartDataMutation = gql`
  query getCartData(
      $userId: String,
    ) {
      getCartData(
        userId: $userId,
    ){
        _id
        subTotal
        items{
            _id
            preview
            name
            count
            caseData
          }
        userId
    }
  }
`;


export const selectPrimaryCardQuery = gql`
  query selectPrimaryCard(
      $userId: String,
      $cardId: String,
    ) {
      selectPrimaryCard(
        userId: $userId,
        cardId: $cardId,
    ){
        message
    }
  }
`;

export const getUserByIdQuery = gql`
  query getUserById(
      $userId: String
    ) {
      getUserById(
        userId: $userId
    ){
        _id
        email
        firstName
        lastName
        provider
        address
        userName
        gender
        phone
        providerId
        role
      
    }
  }
`;