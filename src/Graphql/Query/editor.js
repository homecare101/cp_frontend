import gql from "graphql-tag";

export const getCaseMutation = gql`
  query getUserCases(
    $userId: String,
  ) {
    getUserCases(
        userId: $userId,
    ){
      cases{
        caseData
        name
        preview
        _id
        count
        price
        brand
        model
        material
        theme
        outlineSvgData
       }
    }
  }
`;


export const getCaseDataMutation = gql`
  query getUserCasesById(
    $userId: String,
    $caseId: String,
  ) {
    getUserCasesById(
        userId: $userId,
        caseId: $caseId,
    ){
        caseData
        name
        preview
        _id
    }
  }
`;