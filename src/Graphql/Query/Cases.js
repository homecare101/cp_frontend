import gql from "graphql-tag";

export const getCaseModelQuery = gql`
query getCaseModel {
    getCaseModel{
            list{
                _id
                brand
                model
                transform
                height
                width
                image
                shadowImage
                path
                activePathBack
                activePathMain
          }
    }
  }
`;