import gql from "graphql-tag";

export const getAllUsersQuery = gql`
  query getAllUsers(
    $userId: String,
  ) {
    getAllUsers(
      userId: $userId,
  ){
      usersList{
        _id
        email
        firstName
        lastName
        provider
        address
        providerId
        role
        token
      }
    }
  }
`;

export const loginMutation = gql`
  query login(
    $email: String,
    $password: String,
  ) {
    login(
      email: $email,
      password: $password,
  ){
        email
        token
        _id
        role
    }
  }
`;

export const socialLoginMutation = gql`
  query socialLogin(
    $firstName: String
    $lastName: String
    $provider: String
    $providerId: String
    $email: String
  ) {
    socialLogin(
      firstName: $firstName,
      lastName: $lastName,
      provider: $provider,
      providerId: $providerId
      email: $email
  ){
        firstName
        email
        token
        _id
    }
  }
`;