import gql from "graphql-tag";

export const getMaterialsQuery = gql`
query getMaterials {
  getMaterials{
          list{
          name
        }
  }
}
`;
export const getModelsQuery = gql`
query getModels {
  getModels{
          list{
          name
        }
  }
}
`;
export const getBrandsQuery = gql`
query getBrands {
  getBrands{
          list{
          name
        }
  }
}
`;
export const getColorsQuery = gql`
query getColors {
  getColors{
          list{
            _id
            label
            value
            code
        }
  }
}
`;
export const getThemesQuery = gql`
query getThemes {
  getThemes{
          list{
          name
        }
  }
}
`;
export const getStockCasesQuery = gql`
query getStockCases(
    $brand: String,
    $model: String,
    $material: String,
    $colors: String,
    $theme: String, 
    $page: Int
) {
  getStockCases(
      brand: $brand,
      model: $model,
      material: $material,
      colors: $colors,
      theme: $theme 
      page: $page 
  ){
      cases{
          _id
          name
          preview
          count
          price
          brand
          model
          material
          colors {
            _id
            label
            value
          }
          theme
        }
        total
  }
}
`;

export const getStockCasesByIdQuery = gql`
query getStockCasesById(
    $caseId: String
) {
  getStockCasesById(
      caseId: $caseId,  
  ){
      _id
      name
      preview
      count
      price
      brand
      model
      material
      colors {
        _id
        label
        value
      }
      theme
  }
}
`;

export const deleteStockCase = gql`
query deleteStockCase(
    $caseId: String
    $userId: String
) {
  deleteStockCase(
      caseId: $caseId,  
      userId: $userId
  ){
    message
  }
}
`;

