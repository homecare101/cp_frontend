import gql from "graphql-tag";

export const changeOrderStatusQuery = gql`
query changeOrderStatus(
    $userId: String
    $orderId: String
    $status: String
) {
  changeOrderStatus(
      userId: $userId
      orderId: $orderId
      status: $status
  ){
    message
  } 
}
`;

export const getAllOrdersQuery = gql`
query getAllOrders(
$userId: String
) {
getAllOrders(
  userId: $userId
){
order{
    _id
    status
    userId
    firstName
    lastName
    extraAddress
    fullAddress
    zonecode
    extraDetails
    mobileNumber
    email
    orderNotes
    createdAt
    cartId
    subTotal
    items{
        caseData
        count
        price
        name
        preview
        _id
        brand
        model
        material
        theme
        outlineSvgData
        colors{
              _id
              label
              value
              }
    }
  }
} 
}
`;