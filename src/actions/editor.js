import gql from "graphql-tag";

export const getSelectedTab = gql`
  query GetSelectedTab {
    selectedTabID @client
  }
`;

export const setSelectedTab = gql`
  mutation setSeletectedTab($selectedTabID: String!) {
    setSeletectedTab(selectedTabID: $selectedTabID) @client
  }
`;

export const getEditor = gql`
  query GetEditor {
    editor @client {
      height,
      width,
      isMouseDown,
      activeElementIndex,
      removeExternalLayers,
      activeElementDims {
          width,
          height,
      },
      offset {
          x,
          y,
      },
      resizeCoords {
          x,
          y,
      },
      resizeEdge,
      elements {
        id,
        props,
        type,
        position {
          x,
          y
        }
      },
      backgroundColor,
      model,
      materials,
      collage,
      selectedPath,
      isRotating,
      currentMousePointer {
        x,
        y
      },
      activeRotation
    }
  }
`;

export const addElements = gql`
  mutation addElements($elements: Array!) {
    addElements(elements: $elements) @client
  }
`;

export const setToast = gql`
  mutation setToast($type: String!, $showToast: String!, $message: String!) {
    setToast (
    type: $type,
    showToast: $showToast,
    message: $message
    ) @client
  }
`;

export const setBackgroundColor = gql`
  mutation setBackgroundColor($backgroundColor: String!) {
    setBackgroundColor(backgroundColor: $backgroundColor) @client
  }
`;


export const updateElements = gql`
  mutation updateElements($elements: Array!, $activeElementIndex: String!,$removeExternalLayers: Boolean!, $activeElementDims: Object!, $resizeEdge: String!) {
    updateElements (
      elements: $elements,
      activeElementIndex: $activeElementIndex,
      removeExternalLayers: $removeExternalLayers,
      activeElementDims: $activeElementDims,
      resizeEdge: $resizeEdge
    ) @client
  }
`;

export const updateModel = gql`
  mutation updateModel($model: String!) {
    updateModel(model: $model) @client
  }
`;

export const updateMaterial = gql`
  mutation updateMaterial($materials: String!) {
    updateMaterial(materials: $materials) @client
  }
`;

export const updateSelectable = gql`
  mutation updateSelectable($activeElementIndex: String!,$removeExternalLayers: String!, $activeElementDims: Object!, $offset: Object!) {
    updateSelectable(activeElementIndex: $activeElementIndex, activeElementDims: $activeElementDims, offset: $offset) @client
  }
`

export const updateResizeMode = gql`
  mutation updateResizeMode($resizeCoords: Object!, $resizeEdge: String!) {
    updateResizeMode(resizeCoords: $resizeCoords, resizeEdge: $resizeEdge) @client
  }
`

export const updateEditor = gql`
  mutation updateEditor(
    $elements: Array!,
    $activeElementIndex: String!,
    $removeExternalLayers: Boolean!,
    $activeElementDims: Object!,
    $resizeEdge: String!,
    $isRotating: Bool!,
    $currentMousePointer: Object!,
    $collage: String!,
    $selectedPath: String!,
    $activeRotation: Bool!,
    $backgroundColor: String!
    $materials: String!
  ) {
    updateEditor (
      elements: $elements,
      activeElementIndex: $activeElementIndex,
      removeExternalLayers: $removeExternalLayers,
      activeElementDims: $activeElementDims,
      resizeEdge: $resizeEdge,
      isRotating: $isRotating,
      currentMousePointer: $currentMousePointer,
      collage: $collage,
      selectedPath: $selectedPath,
      activeRotation: $activeRotation,
      backgroundColor: $backgroundColor
      materials: $materials
    ) @client
  }
`
export const updateCollage = gql`
  mutation updateCollage($collage: String!) {
    updateCollage(collage: $collage) @client
  }
`

export const getCliparts = gql`
  query GetCliparts {
    cliparts {
      src
      id
    }
  }
`
export const getCollages = gql`
  query GetCollages {
    collages {
      id
      paths {
        id
        d
      }
    }
  }
`
export const getMaterials = gql`
  query GetMaterials {
    materials {
      id
      title
    }
  }
`

export const getPhoneModel = gql`
  query GetPhoneModel {
    cases{
      model
      transform
      height
      width
      image
      shadowImage
      path
  }
  }
  
`

