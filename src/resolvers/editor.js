import { getEditor } from "../actions/editor";
import { Cases, Collages } from "../utils/constants";
export const editorDefaults = {
  selectedTabID: '',
  editor: {
    __typename: 'editor',
    height: 500,
    width: 325,
    isMouseDown: false,
    activeElementIndex: null,
    removeExternalLayers: false,
    activeElementDims: {
        __typename: 'activeElementDims',
        width: 0,
        height: 0
    },
    offset: {
      __typename: 'offset',
      x: 0,
      y: 0
    },
    resizeCoords: {
      __typename: 'resizeCoords',
      x: 0,
      y: 0
    },
    resizeEdge: '',
    elements: [],
    backgroundColor: '#fff',
    model: Cases[0].model,
    collage: '',
    selectedPath: '',
    materials:'Material#1',
    isRotating: false,
    currentMousePointer: {
      x: 0,
      y: 0,
      __typename: 'currentMousePointer'
    },
    activeRotation:false
  }
}

export const editorResolvers = {

  Mutation: {
    setSeletectedTab: (_, { selectedTabID }, { cache }) => {
      cache.writeData({ data: { selectedTabID } });
      return null;
    },
    setBackgroundColor: (_, { backgroundColor }, { cache }) => {
      cache.writeData({ data: { editor: { backgroundColor, __typename: 'editor' } } });
      return null;
    },
    updateElements: (_, { elements, activeElementIndex = null, removeExternalLayers = false, activeElementDims = editorDefaults.activeElementDims, resizeEdge }, { cache }) => {
      cache.writeData({ data: { editor: { elements, activeElementIndex: activeElementIndex, removeExternalLayers: removeExternalLayers, activeElementDims, resizeEdge, __typename: 'editor' } } });
      return elements;
    },

    setToast: (_, { type, showToast, message }, { cache }) => {
      document.getElementById("toastText").innerHTML = message;
      document.getElementById('toastshow').classList.add('tosteMian')
      document.getElementById('toastshow').classList.add(type)
      setTimeout(function(){ document.getElementById('toastshow').classList.remove('tosteMian') }, 3000);
    }, 

    updateEditor: (_, newData, { cache }) => {
      cache.writeData({ data: { editor: { ...newData, __typename: 'editor' } } });
      return null
    },
    addElements: (_, { elements }, { cache }) => {
      const cacheEditor = cache.readQuery({ query: getEditor })
      cache.writeData({ data: { editor: { elements: cacheEditor.editor.elements.concat(elements), activeElementIndex: cacheEditor.editor.elements.length, __typename: 'editor' } } });
      return elements;
    },
    updateModel: (_, { model }, { cache }) => {

      cache.writeData({ data: { editor: { model, __typename: 'editor' } } });
      return null;
    },
    updateMaterial: (_, { materials }, { cache }) => {

      cache.writeData({ data: { editor: { materials, __typename: 'editor' } } });
      return null;
    },

    updateResizeMode: (_, { resizeCoords, resizeEdge }, { cache }) => {

      cache.writeData({ data: { editor: { resizeCoords, resizeEdge, __typename: 'editor' } } });
      return null;
    
    },
    updateCollage: (_, { collage }, { cache }) => {

      cache.writeData({ data: { editor: { collage, __typename: 'editor' } } });
      return null;
    },

    updateSelectable:  (_, { 
      activeElementIndex = null,
      removeExternalLayers = false,
      activeElementDims = {},
      offset = {}
    }, { cache }) => {
      cache.writeData({ data: { editor: {
        activeElementIndex,
        removeExternalLayers,
        activeElementDims: {
          ...activeElementDims,
          __typename: 'activeElementDims'
        },
        offset: {
          ...offset,
          __typename: 'offset'
        },
        __typename: 'editor' } } });
      return null;
    }
  }

  

}
