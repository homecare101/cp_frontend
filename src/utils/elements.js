import React from 'react';
import ReactDOMServer from 'react-dom/server';

import { returnIfNotValidPositiveNumber, calculateAspectRatioFit } from "./functions";
import { activeElementId, svgContainerID, activeElementID, activeElementGroupID } from "./constants";

export const createElement = (props = {}, position = { x: 0, y: 0 }, type) => {

    const stringifiedProps = JSON.stringify(props);

    const id = new Date().getTime()
    
    return {
        id,
        position: {
            ...position,
            __typename: 'ElementPosition'
        },
        props: stringifiedProps,
        type,
        __typename: 'Element'
    }

}

export const getCenter = (width = 0 , height = 0) => {

    const imgDims = findDimensions(document.getElementById('simpleImage'))

    const phoneImageCoords = convertCoordsToSVGCoords(imgDims.x, imgDims.y)

    const newPosition = {
        x: phoneImageCoords.x + (imgDims.width / 2) - (width / 2),
        y: phoneImageCoords.y + (imgDims.height / 2)  - (height / 2)
    }

    return newPosition
}

export const getSVGCenter = (width = 0 , height = 0) => {

    const imgDims = findDimensions(document.getElementById('simpleImage'))

    const phoneImageCoords = convertCoordsToSVGCoords(imgDims.x, imgDims.y)

    const newPosition = {
        x: phoneImageCoords.x + (width),
        y: phoneImageCoords.y + (height)
    }

    return newPosition

}

export const fileToDataUrl = file =>
    new Promise (
        (resolve, reject) => {

            if (!file) {

                return reject('file is undefined')

            }

            const fileReader = new FileReader()

            fileReader.addEventListener("load", function () {

                let image = new Image()
                image.src = fileReader.result

                image.onload = function () {
                    resolve({
                        dataUrl: fileReader.result,
                        file,
                        width: this.width,
                        height: this.height
                    })
                }

            }, false);

            fileReader.readAsDataURL(file)

        }
    )

export const shiftIndex = (arr, baseIndex, targetIndex) => {

    const newArr = [...arr]

    if(newArr[baseIndex] && newArr[targetIndex]) {
        const temp = newArr[targetIndex]
        newArr[targetIndex] = newArr[baseIndex]
        newArr[baseIndex] = temp

        return {
            updatedELements: newArr,
            updatedIndex: targetIndex
        }
    }

    return {
        updatedELements: newArr,
        updatedIndex: baseIndex
    }

}

export const updateOrder = (elements, direction, index) => {
    if(direction === 'front') {
        return shiftIndex(elements, index, index + 1)
    } else if ( direction === 'back') {
        return shiftIndex(elements, index, index - 1)
    }

    return {
        updatedELements: elements,
        updatedIndex: index
    }
}

export const findDimensions = element => {
    return element ? element.getBoundingClientRect() : {};
}

export const updateElementProps = (element, propsToUpdate) => {

    const { props } = element;

    let parsedProps = JSON.parse(props)

    parsedProps = {
        ...parsedProps,
        ...propsToUpdate
    } 

    const stringifiedProps = JSON.stringify(parsedProps)
    
    return {
        ...element,
        props: stringifiedProps
    }

}

export const getEdgeCordinates = (width, height) => {

    return {
        topLeft: {
            x: -10,
            y: -10,
            cursor:'nw-resize'
        },
        topRight: {
            x: width,
            y: -10,
            cursor:'ne-resize'
        },
        bottomLeft: {
            x: -10,
            y: height,
            cursor:'sw-resize'
        },
        bottomRight: {
            x: width,
            y: height,
            cursor:'se-resize'
        },
    }

}

export const toSvgPosition = ({x, y}) => `translate(${x}, ${y})`

// resize utits

export const updateWidthFromLeft = (previousCoords, newCoords, width) => {

    let newWidth = width

    if(previousCoords.x < newCoords.x) {

        newWidth = returnIfNotValidPositiveNumber(newWidth + (previousCoords.x - newCoords.x), newWidth)

    }

    if(previousCoords.x > newCoords.x) {

        newWidth = returnIfNotValidPositiveNumber( newWidth + (previousCoords.x - newCoords.x), newWidth)

    }

    return newWidth

}

export const updateWidthFromRight = (previousCoords, newCoords, width) => {

    let newWidth = width

    if(previousCoords.x < newCoords.x) {

        newWidth = returnIfNotValidPositiveNumber(newWidth + (newCoords.x - previousCoords.x), newWidth)

    }

    if(previousCoords.x > newCoords.x) {

        newWidth = returnIfNotValidPositiveNumber( newWidth + (newCoords.x - previousCoords.x), newWidth)

    }

    return newWidth

}

export const updateHeightFromTop = (previousCoords, newCoords, height) => {

    let newHeight = height

    if(previousCoords.y < newCoords.y) {

        newHeight = returnIfNotValidPositiveNumber( newHeight + (previousCoords.y - newCoords.y), newHeight)
    
    }
    
    if(previousCoords.y > newCoords.y) {
    
        newHeight = returnIfNotValidPositiveNumber( newHeight + (previousCoords.y - newCoords.y), newHeight)
    
    }

    return newHeight

}

export const updateHeightFromBottom = (previousCoords, newCoords, height) => {

    let newHeight = height

    if(previousCoords.y < newCoords.y) {

        newHeight = returnIfNotValidPositiveNumber( newHeight + (newCoords.y - previousCoords.y), newHeight)

    }

    if(previousCoords.y > newCoords.y) {

        newHeight = returnIfNotValidPositiveNumber( newHeight + (newCoords.y - previousCoords.y), newHeight)

    }

    return newHeight

}

export const getSVGContainerDims = () => findDimensions(document.getElementById(svgContainerID))
export const getActiveElementDims = () => findDimensions(document.getElementById(activeElementID))
export const getActiveGroupDims = () => findDimensions(document.getElementById(activeElementGroupID))

export const convertCoordsToSVGCoords = (x = 0, y = 0) => {
    const svgContainer = document.getElementById('svg-container')

    if(!svgContainer) {
        return {
            x: 0,
            y: 0
        }
    }
        
    let svgPoints = svgContainer.createSVGPoint()

    svgPoints.x = x
    svgPoints.y = y

    return svgPoints.matrixTransform(svgContainer.getScreenCTM().inverse())

}

export const createImage = (width, height, dataUrl, props) => {

    const pathDims = findDimensions(document.getElementById('phoneModelPath'))

    const dims = calculateAspectRatioFit(width, height, pathDims.width, pathDims.height)

    let position = getCenter(dims.width, dims.height)

    const element = createElement({xlinkHref: dataUrl, width: dims.width, height: dims.height, ...props}, position, 'image')

    return element

}

export const getElementCenter = element => {

    const elementDims = findDimensions(element)

    const elementCoords = convertCoordsToSVGCoords(elementDims.x, elementDims.y)

    const newPosition = {
        x: elementCoords.x + (elementDims.width / 2),
        y: elementCoords.y + (elementDims.height / 2)
    }

    return newPosition

}

export const findImageUsingClipPath = (clipPath, elements) => {

    return elements.findIndex(
        element => {
        
            if (element.type === 'image') {

                const parsedProps = JSON.parse(element.props)

                if ( parsedProps.clipPath ) {

                    if ( clipPath === parsedProps.clipPath ) {

                        return true

                    } else {
                        return false
                    }

                } else {
                    return false
                }

            }

            return false

        }
    )

}

// export const createCollageImage = (width, height, dataUrl, collageId, props) => {

//     const pathDims = findDimensions(document.getElementById(id))

//     const dims = calculateAspectRatioFit(width, height, pathDims.width, pathDims.height)

//     let position = getCenter(dims.width, dims.height)

//     const element = createElement({href: dataUrl, width: dims.width, height: dims.height, ...props}, position, 'image')

//     return element

// }

export const removeExistingCollageImage = (pathId, elements) => {
    let existingImageIndex = findImageUsingClipPath(pathId, elements)

    let modifiedElements = elements;

    if ( existingImageIndex > -1 ) {
        modifiedElements = modifiedElements.filter(
            (element, index) => index !== existingImageIndex
        )

        return modifiedElements
    }

    return elements
}

export const getResizedImagePath = ({ width, height }, id) => {

    const selectedPathNode = document.getElementById(`collage-path-${id}`)

    const pathDims = findDimensions(selectedPathNode)

    const dims = calculateAspectRatioFit(width, height, pathDims.width, pathDims.height)

    return dims

}

export const createCollagePathElement = (selectedPath, { width, height, dataUrl }) => {

    const selectedPathNode = document.getElementById(`collage-path-${selectedPath}`)

    let position = {
        x: 0,
        y: 0
    }

    const pathCenter = getElementCenter(selectedPathNode)

    position = {
        x: pathCenter.x - width / 2,
        y: pathCenter.y - height / 2,
    }

    const element = createElement({xlinkHref: dataUrl, width, height, clipPath: selectedPath}, position, 'image')

    return element

}

export const getEmptyPath = ({ paths = [] }, elements) => {

    for ( let i = 0; i < paths.length; i++) {

        let path = paths[i];

        let existingImageIndex = findImageUsingClipPath(path.id, elements)

        if (existingImageIndex == -1) {
            return path.id
        }

    }

    return null

}

export const isElementExist = (id, elements) => {
    let existingImageIndex = findImageUsingClipPath(id, elements)

    let elementExist = false

    if ( existingImageIndex > -1 ) {
        elementExist = true
    }

    return elementExist
}

export const textToTspan = textString => {
    const textArr = textString.split('\n')

    const tspanList = textArr.map(
        text => {

            const newProps = { children: text, x: 0, dy: "1.2em" }

            const Element = ReactDOMServer.renderToStaticMarkup(<tspan {...newProps} />)

            return Element

        }
    )

    return tspanList.join('')
}

export const getScale = () => {

    let scale = 1

    if (window.innerWidth <= 640) {
        scale = 0.8
    }

    return scale;
    
}