import { resolve } from "url";

export const partial = (func, ...firstArguments) => {
    return (...laterArguments) => func(...firstArguments, ...laterArguments)
}

export const calculateAspectRatioFit = (srcWidth, srcHeight, maxWidth, maxHeight) => {

    let ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

    return { width: srcWidth*ratio, height: srcHeight*ratio };

}

// if valid positive number return it else return fallback
export const returnIfNotValidPositiveNumber = (number, fallBack) => {

    if(number > -1) {
        return number
    }

    return fallBack

}

export const getImageDims = src => {

    return new Promise (
        (resolve, reject) => {

            let image = new Image()
            image.src = src

            image.onload = function () {
                resolve({
                    width: this.width,
                    height: this.height
                })
            }

        }
    )

}

export const showLoader = () => document.getElementById('loader').classList.add('active')
export const stopLoader = () => document.getElementById('loader').classList.remove('active')

export const closePopUp = () => {
    document.getElementById('MobilePopupTab').classList.remove("active")
    document.getElementById('mobile-tool-list').classList.remove("close-mobile-tab")
    document.getElementById('material-model-dropdown').classList.remove("display-none")
    document.getElementById('mobile-tab-close').classList.remove("display-block")
}