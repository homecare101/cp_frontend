import React from 'react';
import Editor from '../../views/Editor'
import Home from '../Home'
import EditorUploader from '../../components/EditorUploaded/EditorUploader'
import { Route, Switch,BrowserRouter,Redirect } from 'react-router-dom';
import Dashboard from '../Dashboard';
import Contactus from '../Contactus';
import AboutUs from '../AboutUs';
import Stock from '../Stock';
// import OneCase from '../oneCase';
import UserDetails from '../UserDetails';
import UserOrder from '../UserDetails/UserOrder';
import ShowCart from '../Cart/ShowCart';
import CheckOut from '../Cart/ConfirmOrder';
import Admin from '../Admin';
import AddCase from '../Admin/AddCase';
import ReactLoading from 'react-loading';
import { AuthModalProvider, AuthModal, AuthModalConsumer } from '../../components/Auth/AuthModalProvider/AuthModalProvider'
import "./index.css"
class ShowModal extends React.Component {

	componentDidMount () {
      this.showModal()
	}

	showModal () {
		this.props.setFormType('Login')
		this.props.openModal()
    }
    render () {
        return null
    }

}

const checkAuth = () =>{
  const token = localStorage.getItem('token');
  if(token){
    return true
  } else {
    return false
  }
}

const checkAdminAuth = () =>{
  const token = localStorage.getItem('token');
  const userRole = localStorage.getItem('userRole');
  if(token && userRole === "Admin"){
    return true
  } else {
    return false
  }
}

const AuthRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    checkAuth() ? (
      <Component {...props}/>
    ) : (
      <AuthModalConsumer>
          {
            ({ openModal, setFormType }) =>
            <div>
              <Redirect to={{ pathname: '/'}} />
              <ShowModal openModal={openModal} setFormType={setFormType} />
            </div>
            }
			</AuthModalConsumer>
      // 
    )
  )}/>
)

const AdminAuthRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    checkAdminAuth() ? (
      <Component {...props}/>
    ) : (
      <AuthModalConsumer>
          {
            ({ openModal, setFormType }) =>
            <div>
              <Redirect to={{ pathname: '/'}} />
              <ShowModal openModal={openModal} setFormType={setFormType} />
            </div>
            }
			</AuthModalConsumer>
      // 
    )
  )}/>
)
export default class App extends React.Component {

  render() {
    return (
      <div>
        <div id={'loader'} className="loader">
					<ReactLoading type='spin' color='#fff' height={60} width={60} />
				</div>  
        <div id='toastshow' className="tosteClose">
            <p id="toastText">
                demoText
            </p>
        </div>      
        <AuthModalProvider>
          <BrowserRouter>
            <div>
              <AuthModal />
              <Switch>
                <Route exact path="/" render={props => <Home {...props} />} />
                <Route exact path="/editor" component={Editor} />
                <AuthRoute exact path="/editor/:caseId" component={Editor} />
                <Route exact path="/editorUploader" render={props => <EditorUploader {...props} />} />
                <Route exact path="/home" render={props => <Home {...props} />} />
                <Route exact path="/contactus" render={props => <Contactus {...props} />} />
                <Route exact path="/aboutus" render={props => <AboutUs {...props} />} />
                <Route exact path="/stock" render={props => <Stock {...props} />} />
                {/* <Route exact path="/oneCase/:caseId" render={props => <OneCase {...props} />} /> */}
                <AuthRoute path="/userDetails" component= {UserDetails} />
                <AuthRoute path="/orders" component= {UserOrder} />
                <AuthRoute path="/showCart" component= {ShowCart} />
                <AuthRoute path="/checkOut" component= {CheckOut} />
                <AdminAuthRoute path="/admin" component= {Admin}/>
              </Switch> 
            </div>
          </BrowserRouter>
        </AuthModalProvider>
      </div>
    );
  }
}