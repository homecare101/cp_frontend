import React, { Component } from 'react';
import { Button, Container, Row, Col, FormGroup, Label, Input } from 'reactstrap';
import ReactLoading from 'react-loading';
import Header from '../../components/Header';
import LightFooter from '../../components/LightFooter';
import QueryContactUs from '../../components/QueryContactUs';
import Slider from "react-slick";
import './Home.css'
import { Link } from 'react-router-dom'

class Home extends Component {
	constructor(props) {
		super(props);
		this.toggle = this.toggle.bind(this);
		this.state = {
			dropdownOpen: false,
			slides: 6
		};
	}
	componentDidMount(event) {
		this.handleResize()
		window.addEventListener('resize', () => {
			this.handleResize()
		});
	}
	handleResize() {
		if (window.innerWidth < 1120  && window.innerWidth > 810) {
			this.setState({ slides: 4 })
		}else if (window.innerWidth <= 810 && window.innerWidth > 660) {
			this.setState({ slides: 3 })
		} else if (window.innerWidth <= 660 && window.innerWidth > 500) {
			this.setState({ slides: 2 })
		} else if (window.innerWidth <= 500) {
			this.setState({ slides: 1 })
		} else {
			this.setState({ slides: 6 })
		}
	}
	toggle() {
		this.setState({
			dropdownOpen: !this.state.dropdownOpen
		});
	}
	handleGoToEditor(){
		this.props.history.push(`/editor`)
	}

	render() {
		const settings = {
			dots: true,
			infinite: true,
			speed: 500,
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
		};
		const settings2 = {
			dots: false,
			infinite: true,
			speed: 500,
			slidesToShow: this.state.slides,
			slidesToScroll: 1,
			arrows: true,
		};
		return (
			<div>
				<div className="bgImage">
					<Header />
					<Container fluid>
						<Row className="bgImageContainer">
							<Col md={12}>
								<Col md={6} sm={12} xs={12} className="noPadding">
									<div className="home-heading-con">
										<h1>오직 나만의 <span>커스톰</span>  케이스</h1>
										<p>사진, 그림, 문구 또는 클립아트를 새겨보세요.</p>
										<Button onClick={this.handleGoToEditor.bind(this)} className="BtnHomeMainOut">
											<img src="/magicpen.png" className="iconMagicPen"/>
											무료 디자인 하기
										</Button>
									</div>
								</Col>
								<Col md={6} sm={6} xs={12} className="imageContainerMain">
									<div className="imageMainMobile">
										<img className="imageAll" src='./mainIphoneHome.png'/>
									</div>
								</Col>
							</Col>
						</Row>
						<div className="home-cases-heading">
							<h1>커스텀파크 자체디자인 둘러보기</h1>
						</div>
					</Container>
				</div>
				<div className="circleBackgroundContainer">
					<div className="circleBackground">
						<Container fluid>
							<Row>
								<Col md={12} className="home-slick-img">
									<Slider {...settings2} className="home-slick-slider">
										<div className="phoneSliderText">
											<div className="phoneImgAll">
												<img src="phoneImg1.jpg" className="imageAll" />
											</div>
											<h5>iPhone X</h5>
											<p>Riley</p>
										</div>
										<div className="phoneSliderText">
											<div className="phoneImgAll">
												<img src="phoneImg1.jpg" className="imageAll" />
											</div>
											<h5>iPhone 8</h5>
											<p>Baby</p>
										</div>
										<div className="phoneSliderText">
											<div className="phoneImgAll">
												<img src="phoneImg1.jpg" className="imageAll" />
											</div>
											<h5>iPhone 7</h5>
											<p>Emma</p>
										</div>
										<div className="phoneSliderText">
											<div className="phoneImgAll">
												<img src="phoneImg1.jpg" className="imageAll" />
											</div>
											<h5>iPhone 6s</h5>
											<p>Buddy</p>
										</div>
										<div className="phoneSliderText">
											<div className="phoneImgAll">
												<img src="phoneImg1.jpg" className="imageAll" />
											</div>
											<h5>iPhone X</h5>
											<p>Bailey</p>
										</div>
										<div className="phoneSliderText">
											<div className="phoneImgAll">
												<img src="phoneImg1.jpg" className="imageAll" />
											</div>
											<h5>iPhone X</h5>
											<p>Riley</p>
										</div>
										<div className="phoneSliderText">
											<div className="phoneImgAll">
												<img src="phoneImg1.jpg" className="imageAll" />
											</div>
											<h5>iPhone X</h5>
											<p>Riley</p>
										</div>
										<div className="phoneSliderText">
											<div className="phoneImgAll">
												<img src="phoneImg1.jpg" className="imageAll" />
											</div>
											<h5>iPhone X</h5>
											<p>Riley</p>
										</div>
									</Slider>
									<div className="sliderButton">
										<Button>전체 디자인 보기</Button>
									</div>
								</Col>
							</Row>
						</Container>
						<Container fluid className="zindexAll">
							<Row>
								<div className="home-form">
									<h1>디자인 하기</h1>
									<Col md={12} className="home-form-out">
										<Col md={5} sm={12} xs={12}  className="home-form-inner">
											<div>
												<FormGroup className="home-form-content">
													<Label>제조사</Label>
													<Input type="select" name="selectMulti" className="home-dropdown">
														<option>Apple</option>
														<option>Samsung</option>
														<option>Nokia</option>
														<option>Oppo</option>
														<option>MI</option>
													</Input>
													<Label>모델명</Label>
													<Input type="select" name="selectMulti" className="home-dropdown">
														<option>Iphone7</option>
														<option>Iphone6</option>
														<option>Iphone5</option>
														<option>Iphone4</option>
														<option>IphoneX</option>
													</Input>
													<Label>케이스 종류</Label>
													<Input type="select" name="selectMulti" className="home-dropdown">
														<option>Glossy</option>
														<option>Rain</option>
														<option>Cloud</option>
														<option>Spring</option>
														<option>Dark</option>
													</Input>
													<Link to="/editor">
														<Button className="designBtn">지금 디자인하기</Button>
													</Link>
												</FormGroup>
											</div>
										</Col>
										<Col md={2} sm={2} xs={2} className="arrowOutCon">
											<div className="arrowOut">
												<svg xmlns="http://www.w3.org/2000/svg" width="9" height="15" viewBox="0 0 9 15">
											    	<path fill="none" fill-rule="evenodd" stroke="#434243" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1.448 1.558l6.3 6.3-6.133 6.134"/>
												</svg>
											</div>
										</Col>	
										<Col md={5} sm={5} xs={12} className="mobile_image_out">
											<div className="mobile_image">
												<img src="/design.png" />
											</div>
										</Col>
									</Col>
								</div>
							</Row>
						</Container>
						{/* <Container fluid classNames="zindexAll"> */}
							<Row className="no-margin-main">
								<Col md={12} className="home-detail-con">
									<Col sm={4} className="detailIconCon alineCenterHome paddingLeft20">
										<div className="home-detail-con-main alineCenterHome">
											<img src="/icon11.png" className="imageAll" />
										</div>
										<p>무료배송</p>
									</Col>
									<Col sm={4} className="detailIconCon alineCenterHome">
										<div className="home-detail-con-main alineCenterHome">
											<img src="/icon12.png" className="imageAll" />
										</div>
										<p>최고의 퀄리티</p>
									</Col>
									<Col sm={4} className="detailIconCon alineCenterHome">
										<div className="home-detail-con-main alineCenterHome">
											<img src="/icon13.png" className="imageAll" />
										</div>
										<p>빠른 A/S</p>
									</Col>
								</Col>
							</Row>
						{/* </Container> */}
					</div>
				</div>
				<Container fluid className="sliderCon ">
					<Slider  {...settings}  >
						<div className="home-slider">
							<div className="home-slider-image">
								<img src="/egg.png" className="imageAll"/>
							</div>
							<div className="home-slider-image-con zindexAll">
								<h1>고객 리뷰</h1>
								<div className="home-slider-image-main">
									<img src="/string.png" className="imageAll"/>
								</div>
								<p>“정말 마술 같아요!!! 저만의 케이스를 순식간에 만들었어요. 다른 디자인으로 추가주문 할거같아요”</p>
								<label>Jin Lee</label>
							</div>
						</div>
						<div className="home-slider">
							<div className="home-slider-image">
								<img src="/egg.png" className="imageAll"/>
							</div>
							<div className="home-slider-image-con zindexAll">
								<h1>고객 리뷰</h1>
								<div className="home-slider-image-main">
									<img src="/string.png" className="imageAll"/>
								</div>
								<p>“정말 마술 같아요!!! 저만의 케이스를 순식간에 만들었어요. 다른 디자인으로 추가주문 할거같아요”</p>
								<label>Jin Lee</label>
							</div>
						</div>
					</Slider>
				</Container>
				<Container fluid classNames="zindexAll">
					<Row className="marginBottom100">
						<Col md={12} className="home-detail-con-bottom-out">
							<h1>무엇이든 물어보세요</h1>
							<div className="home-detail-con-bottom">
								<Col sm={4} className="alineCenterHome mb40">
									<div className="home-contact-con-main alineCenterHome">
										<div className="home-contact-iconOut"> 
											<div className="home-contact-iconOut-main1"> 
												<img src="/phoneIcon.png" className="imageAll" />
											</div>
										</div>
										<p>전화하기</p>
										<label>+82 67 0923 5692</label>
										<h6><u>Callback</u></h6>
									</div>
								</Col>
								<Col sm={4} className="alineCenterHome mb40">
									<div className="home-contact-con-main alineCenterHome">
										<div className="home-contact-iconOut2"> 
											<div className="home-contact-iconOut-main2"> 
												<img src="/email.png" className="imageAll"/>
											</div>
										</div>
										<p>이메일하기</p>
										<label><u>hello@custompark.com</u></label>
									</div>
								</Col>
								<Col sm={4} className="alineCenterHome mb40">
									<div className="home-contact-con-main alineCenterHome">
										<div className="home-contact-iconOut3"> 
											<div className="home-contact-iconOut-main3"> 
												<img src="/insta.png" className="imageAll"/>
											</div>
										</div>
										<p>팔로우하기</p>
										<label>#custompark</label>
									</div>
								</Col>
							</div>

						</Col>
					</Row>
				</Container>
				<LightFooter />
			</div>
		)
	}
}

export default Home