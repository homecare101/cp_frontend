import React from 'react'
import Header from '../../components/Header';
import {  Input, Container, Row, Col } from 'reactstrap';
import './cart.css'
import { withRouter } from 'react-router';
import { graphql, compose, withApollo } from 'react-apollo';
import CartTotal from '../../components/CartMenu/CartTotal';
import { checkCartMutation } from '../../Graphql/Mutations/cart';
import { showLoader, stopLoader } from '../../utils/functions';
import ReactLoading from 'react-loading';
import { addToCartMutaion } from '../../Graphql/Mutations/userDetails';
import { setToast } from '../../actions/editor';
import LightFooter from '../../components/LightFooter';


class ShowCart extends React.Component {
    constructor() {
        super()
        this.state = {
            caseNo: '2',
            data: []
        }
    }
    componentDidMount() {
        showLoader()
        var userId = localStorage.getItem('id')
        this.props.createCart({
            variables: {
                userId: userId
            },
        }).then(res => {
            let cartData = res.data.checkCart.items
            this.setState({ data: cartData })
            stopLoader()
        })
        .catch(error => {
            stopLoader()
            console.log(error, "error")
            let errorText = { ...error }
            if (errorText !== {}) {
                this.props.setToast({
                    variables: { type: 'error', message: errorText.graphQLErrors[0].message }
                })
            }
        })
    }
    handleChange(id, event) {
        showLoader()
        let userID = localStorage.getItem('id')
        let cartData = JSON.parse(localStorage.getItem('cartData'))
            cartData = JSON.parse(JSON.stringify(cartData))
        let cartItems = this.state.data
        let indexCheck = cartItems.map(function(e) { return e._id; }).indexOf(id);
        cartItems[indexCheck].count = event.target.value
        let newCart = []
        if(cartItems.length > 0){
            cartItems.forEach((item, key) => {
                newCart.push({caseData: item.caseData,
                    caseData: item.caseData,
                    count: item.count,
                    name: item.name,
                    preview: item.preview,
                    price: item.price,
                    _id: item._id,
                    brand: item.brand,
                    colors: item.colors,
                    material: item.material,
                    model: item.model,
                    theme: item.theme,
                    outlineSvgData: item.outlineSvgData
                })
            });
        }
        this.props.addCart({
            variables: {
                userId: userID,
                subTotal: 0,
                cartId : cartData._id,
                items: newCart
            },
        }).then(res => {
            stopLoader()
            cartData.items = newCart
            this.setState({data: newCart})
            localStorage.setItem('cartData' , JSON.stringify(cartData))
        })
        .catch(error => {
            stopLoader()
            console.log(error)
            let errorText = { ...error }
            if (errorText !== {}) {
            this.props.setToast({
                variables: { type: 'error', message: errorText.graphQLErrors[0].message }
            })
        }
        })
    }
    onDeleteCart(id){
        let userID = localStorage.getItem('id')
        let cartData = JSON.parse(localStorage.getItem('cartData'))
            cartData = JSON.parse(JSON.stringify(cartData))
        let cartItems = this.state.data
        let indexCheck = cartItems.map(function(e) { return e._id; }).indexOf(id);
        cartItems.splice(indexCheck, 1)

        let newCart = []
        if(cartItems.length > 0){
            cartItems.forEach((item, key) => {
                newCart.push({caseData: item.caseData,
                    caseData: item.caseData,
                    count: item.count,
                    name: item.name,
                    preview: item.preview,
                    price: item.price,
                    _id: item._id,
                    brand: item.brand,
                    colors: item.colors,
                    material: item.material,
                    model: item.model,
                    theme: item.theme,
                    outlineSvgData: item.outlineSvgData
                })
            });
        }
        this.props.addCart({
            variables: {
                userId: userID,
                subTotal: 0,
                cartId : cartData._id,
                items: newCart
            },
        }).then(res => {
            this.props.setToast({
                variables: { type: 'success', message: "Item Deleted From Cart" }
            })
            cartData.items = newCart
            this.setState({data: newCart})
            localStorage.setItem('cartData' , JSON.stringify(cartData))
        })
        .catch(error => {
            let errorText = { ...error }
            if (errorText !== {}) {
            this.props.setToast({
                variables: { type: 'error', message: errorText.graphQLErrors[0].message }
            })
        }
        })
    }
    render() {
        let { caseNo } = this.state
        let subTotal = 0
        return (
            <div>
                <div id={'loader'} className="loader">
					<ReactLoading type='spin' color='#fff' height={60} width={60} />
				</div>      
                <Header />
                <Container fluid>
                    <Col md={8} sm={12} xs={12}>
                        <Row>
                            <Col md={12} className="cart-heading-out">
                                <p>Checkout</p>
                            </Col>
                        </Row>
                        <Row className="cart-row-heading">
                            <Col md={7} xs={6}>
                                <p>Products</p>
                            </Col>
                            <Col md={2} xs={2}>
                                <p>Price</p>
                            </Col>
                            <Col md={3} xs={3}>
                                <p>Total Amount</p>
                            </Col>
                        </Row>
                        {this.state.data.length > 0 ? this.state.data.map((val, index) => {
                            return (
                                <Row className="cart-row-data" key={index}>
                                    <Col md={7} xs={6}>
                                        <Row className="aligncenter">
                                            <Col md={9} xs={8} className="displayflex aligncenter noPaddingImage">
                                                <Col md={4} xs={12}>
                                                        <img src={val.preview} style= {{width: '45px'}}/>
                                                </Col>
                                                <Col md={8} xs={12}>
                                                    <div className="cart-device-name">
                                                        <p>{val.name}</p>
                                                        <p className="onEditCursor"><u>Edit</u></p>
                                                    </div>
                                                </Col>
                                            </Col>
                                            <Col md={3} xs={4}>
                                                <Input type="number" name="select" value={val.count} className="check-checkbox" onChange={this.handleChange.bind(this, val._id)}></Input>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col md={2} xs={2} className="cart-price">
                                        <p>{val.price}</p>
                                    </Col>
                                    <Col md={2} xs={2} className="cart-price-total">
                                        <p>{ val.price * val.count }</p>
                                    </Col>
                                    <Col md={1} xs={1}>
                                        <div className="icon-cross" onClick={this.onDeleteCart.bind(this, val._id)}><i class="fa fa-close icon"></i></div>
                                    </Col>
                                    <div className="barLine"></div>
                                    <p style={{display : 'none'}}>{subTotal = subTotal + (val.price * val.count)}</p>
                                </Row>  
                            )
                        }): "Cart is empty"}
                    </Col>
                    <Col md={4} sm={12} xs={12} className="widthCartTotal">
                        <CartTotal subTotal = {subTotal}/>
                    </Col>
                </Container>
                <LightFooter />
            </div>
        )
    }
}
export default withRouter(compose(
    withApollo,[
    graphql(checkCartMutation, { name: 'createCart' }),
    graphql( addToCartMutaion, { name:'addCart' }),
    graphql( setToast, { name:'setToast' })]
)(ShowCart))