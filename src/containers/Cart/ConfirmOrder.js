import React from 'react'
import Header from '../../components/Header';
import { Button, Input, Form, FormGroup, Container, Row, Col, Label, Modal } from 'reactstrap';
import './cart.css'
import { withRouter } from 'react-router';
import { graphql, compose, withApollo } from 'react-apollo';
import { IsValidForm } from '../../components/common/validation';
import { checkCartMutation } from '../../Graphql/Mutations/cart';
import { setToast } from '../../actions/editor';
import { showLoader, stopLoader } from '../../utils/functions';
import { addressMutaion } from '../../Graphql/Mutations/userDetails';
import { saveOrderMutation } from '../../Graphql/Mutations/order';
import BootPay from "bootpay-js"
import { getAddressMutation, getUserByIdQuery } from '../../Graphql/Query/userDetails';
import LightFooter from '../../components/LightFooter';
import { mapValuesLimit } from 'async';

var daum = window.daum;
var IMP = window.IMP; // 생략가능
IMP.init('iamport');

class CheckOut extends React.Component {
    constructor() {
        super()
        this.state = {
            addAddress: false,
            data: [],
            addData: [],
            selectedAdrs: null,
            checked: false,
            checkedMain: false,
            btnDisable: true,
            errors: {},
            errorInd: false,
            modal: false,
            btnDisable: false,
            addressMain: null,
            addressText: '',
            address: {
                fname: '',
                lname: '',
                phone: '',
                email: '',
                notes: '',
                extraAddress: '',
                fullAddress: '',
                zonecode: '',
                extraDetails: ''
            },
            newAddress: {
                extraAddress: '',
                fullAddress: '',
                zonecode: '',
                extraDetails: ''
            }
        }
    }
    handleChangeAddress(key, event) {
        let { newAddress } = this.state
        newAddress[key] = event.target.value
        this.setState({ newAddress, errorInd: false })
    }

    handleChange(key, event) {
        let value = event.target.value;
        let { address } = this.state
        address[key] = value
        this.setState({ address });
    }
    componentDidMount() {
        showLoader()
        var userId = localStorage.getItem('id')
        this.props.createCart({
            variables: {
                userId: userId
            },
        }).then(res => {
            stopLoader()
            let cartData = res.data.checkCart.items
            this.setState({ data: cartData })
        })
            .catch(error => {
                stopLoader()
                console.log(error, "error")
                let errorText = { ...error }
                if (errorText !== {}) {
                    this.props.setToast({
                        variables: { type: 'error', message: errorText.graphQLErrors[0].message }
                    })
                }
            })
        this.getAddresses()
        this.userDataDetails()
    }
    userDataDetails(){
        var userId = localStorage.getItem('id')
        this.props.client.query({
            query: getUserByIdQuery,
            fetchPolicy: "network-only",
            variables: {
                userId: userId,
            },
        }).then(res => {
            let userDetails = JSON.parse(JSON.stringify(res.data.getUserById))
            let { address } = this.state
            address.email = userDetails.email 
            address.fname = userDetails.firstName
            this.setState({ address })
            stopLoader()
        })
        .catch(error => {
            stopLoader()
            console.log(error)
        })
    }

    getAddresses() {
        var userId = localStorage.getItem('id')
        this.props.client.query({
            query: getAddressMutation,
            fetchPolicy: "network-only",
            variables: {
                userId: userId,
            },
        }).then(res => {
            var responce = res.data.getAddress.addresses
            responce = JSON.parse(JSON.stringify(responce))
            this.setState({ addData: responce })
            stopLoader()
        })
            .catch(error => {
                stopLoader()
                console.log(error)
            })
    }

    confirmPayment(subtotal) {
        let cartData = localStorage.getItem('cartData')
        cartData = JSON.parse(cartData)
        cartData = JSON.parse(JSON.stringify(cartData))
        let cartItems = cartData.items
        let newCart = []
        let userID = localStorage.getItem('id')
        if (cartItems.length >= 1) {
            cartItems.forEach((item, key) => {
                if (item._id === null) {
                } else {
                    let existanceCheck = newCart.map(function (e) { return e._id; }).indexOf(item._id);
                    if (existanceCheck === -1) {
                        newCart.push({
                            caseData: item.caseData,
                            count: item.count,
                            name: item.name,
                            preview: item.preview,
                            price: item.price,
                            _id: item._id,
                            brand: item.brand,
                            colors: item.colors,
                            material: item.material,
                            model: item.model,
                            theme: item.theme,
                            outlineSvgData: item.outlineSvgData
                        })
                    } else {
                        let count = newCart[existanceCheck].count
                        count = count + 1
                        newCart[existanceCheck].count = count
                    }

                }
            });
        } else {
            this.props.setToast({
                variables: { type: 'error', message: "Cart is empty Please select Items" }
            })
        }
        let { fname, lname, phone, notes, email, zonecode, fullAddress, extraAddress, extraDetails } = this.state.address
        let date = new Date();
        const monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        let fulldate = date.getDate() + ' ' + monthNames[date.getMonth()] + ' ' + date.getFullYear()

        if (this.state.address.fullAddress === '') {
            this.props.setToast({
                variables: { type: 'error', message: "Please Select Proper Address" }
            })
        }
        else {
            if (!this.state.checkedMain) {
                this.props.setToast({
                    variables: { type: 'error', message: "Please Accept terms and conditions" }
                })
            }
            else {
                let fields = ['fname', 'lname', 'email', 'phone']
                let formValidation = IsValidForm(fields, this.state.address)
                this.setState({ errors: formValidation.errors })
                if (formValidation.validate) {
                    let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                    if (reg.test(email) == false) {
                        this.props.setToast({
                            variables: { type: 'error', message: 'Invalid Email Address' }
                        })
                    }
                    else {
                        BootPay.request({
                            price: 1000, //  amount to pay  
                            application_id: '5c1889f2b6d49c67fabf6f06',
                            name: 'iPhone', //  item name,  
                            phone: '01032197334',
                            order_id: "1",
                            pg: 'payapp',
                            method: 'card',
                            show_agree_window: 1, //  Whether to open billing agreement window 1 - Floating, 0 - Do not float  

                            user_info: { //  customer information purchased (statistics or customer information requested by PG company)  
                                email: email,
                                phone: phone,
                                area: fullAddress
                            }
                        }).error((data) => {
                            ///////   // This  function is called when the payment fails.
                            var msg = " Payment error:  " + JSON.stringify(data);
                            this.props.setToast({
                                variables: { type: 'error', message: "There is some issue. Try agin later" }
                            })
                        }).cancel((data) => {
                            /////    // This  function is called when the cancel button is pressed while making payment in the payment window.
                            var msg = " Payment canceled:  " + JSON.stringify(data);
                            this.props.setToast({
                                variables: { type: 'error', message: "Payment Cancelled" }
                            })
                        }).confirm((data) => {
                        }).done((data) => {
                            ////This  function is called when payment is complete.
                            this.props.saveOrder({
                                variables: {
                                    userId: userID,
                                    cartId: cartData._id,
                                    status: 'Completed',
                                    firstName: fname,
                                    lastName: lname,
                                    mobileNumber: phone,
                                    email: email,
                                    orderNotes: notes,
                                    createdAt: fulldate,
                                    extraAddress: extraAddress,
                                    fullAddress: fullAddress,
                                    zonecode: zonecode,
                                    extraDetails: extraDetails,
                                    subTotal: subtotal,
                                    items: newCart,
                                },
                            }).then(res => {
                                this.props.setToast({
                                    variables: { type: 'success', message: "Your Order is Placed" }
                                })
                                this.props.history.push(`/userDetails/orders`)
                            })
                                .catch(error => {
                                    stopLoader()
                                    let errorText = { ...error }
                                    if (errorText !== {}) {
                                        this.props.setToast({
                                            variables: { type: 'error', message: errorText.graphQLErrors[0].message }
                                        })
                                    }
                                })
                        }).ready((data) => {
                            // This  is the function that is called when the virtual account number is a number (issued).
                        });
                    }
                } else {
                    this.props.setToast({
                        variables: { type: 'error', message: "Please Fill All Fields First" }
                    })
                }
            }
        }
    }
    onEditCart() {
        this.props.history.push(`/showCart`)
    }
    selectAddresss(addressData) {
        let { address } = this.state
        address.fullAddress = addressData.fullAddress
        address.extraAddress = addressData.extraAddress
        address.zonecode = addressData.zonecode
        address.extraDetails = addressData.extraDetails
        this.setState({ selectedAdrs: addressData._id, address })
    }

    addressDetails() {
        let { newAddress } = this.state
        daum.postcode.load(() => {
            new daum.Postcode({
                onComplete: ((data) => {
                    let addr = '';
                    let extraAddr = '';
                    if (data.userSelectedType === 'R') {
                        addr = data.roadAddress;
                    } else {
                        addr = data.jibunAddress;
                    }
                    if (data.userSelectedType === 'R') {
                        if (data.bname !== '' && /[동|로|가]$/g.test(data.bname)) {
                            extraAddr += data.bname;
                        }
                        if (data.buildingName !== '' && data.apartment === 'Y') {
                            extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                        }
                        if (extraAddr !== '') {
                            extraAddr = extraAddr;
                        }
                        newAddress.extraAddress = extraAddr
                        this.setState({ newAddress, errorInd: false })

                    } else {
                        document.getElementById("sample6_extraAddress").value = ''
                    }
                    newAddress.zonecode = data.zonecode
                    newAddress.fullAddress = addr
                    this.setState({ newAddress, errorInd: false })
                    document.getElementById("sample6_detailAddress").focus()
                })
            }).open();
        })
    }
    onCheckBoxClick() {
        let { checked } = this.state
        this.setState({ checked: !checked })
    }
    onClickSubmitTerms() {
        let { checkedMain } = this.state
        this.setState({ checkedMain: !checkedMain })
    }

    onAddNew() {
        this.setState({ modal: !this.state.modal, isEdit: false })
    }
    addAddress() {
        let fields = ['extraAddress', 'fullAddress', 'zonecode']
        let { newAddress, address } = this.state
        let formValidation = IsValidForm(fields, this.state.newAddress)
        this.setState({ errors: formValidation.errors })
        if (formValidation.validate) {
            address.fullAddress = newAddress.fullAddress
            address.extraAddress = newAddress.extraAddress
            address.extraDetails = newAddress.extraDetails
            address.zonecode = newAddress.zonecode
            this.setState({ address, modal: false })
            if (this.state.checked) {
                showLoader()
                let { fullAddress, extraAddress, zonecode, extraDetails } = this.state.newAddress
                let userId = localStorage.getItem('id')
                this.props.userNewAddress({
                    variables: {
                        userId: userId,
                        extraAddress: extraAddress,
                        fullAddress: fullAddress,
                        zonecode: zonecode,
                        extraDetails: extraDetails
                    },
                }).then(res => {
                    this.getAddresses()
                    this.props.setToast({
                        variables: { type: 'success', message: "Address Added Succesfully" }
                    })
                })
                    .catch(error => {
                        console.log(error, "error")
                    })
            }
        } else {
            this.setState({ errorInd: true })
        }
    }
    render() {
        let subtotal = 0
        const { errorInd, errors } = this.state
        let { zonecode, fullAddress, extraAddress, extraDetails } = this.state.newAddress
        let { email, fname } = this.state.address
        return (
            <div>
                <Header />
                <Container fluid>
                    <Col md={8} sm={12} xs={12}>
                        <Row>
                            <Col md={12} className="check-heading-out">
                                <p>Shipping details</p>
                            </Col>
                        </Row>
                        <Form className="inputStyle">
                            <Row form>
                                <Col md={5}>
                                    <FormGroup>
                                        <Label>First name</Label>
                                        <Input type="text" name="fname" value={fname} onChange={this.handleChange.bind(this, 'fname')} />
                                        {errorInd && <p className="hasError">
                                            {errors.message}
                                        </p>}
                                    </FormGroup>
                                </Col>
                                <Col md={7}>
                                    <FormGroup>
                                        <Label>Last name</Label>
                                        <Input type="text" name="lname" onChange={this.handleChange.bind(this, 'lname')} />
                                        {errorInd && <p className="hasError">
                                            {errors.message}
                                        </p>}
                                    </FormGroup>
                                </Col>
                            </Row>

                            <Row form>
                                <Col md={5}>
                                    <FormGroup>
                                        <Label>Mobile number</Label>
                                        <Input type="number" name="phone" onChange={this.handleChange.bind(this, 'phone')} />
                                        {errorInd && <p className="hasError">
                                            {errors.message}
                                        </p>}
                                    </FormGroup>
                                </Col>
                                <Col md={7}>
                                    <FormGroup>
                                        <Label>Email</Label>
                                        <Input type="text" name="email" value={email} onChange={this.handleChange.bind(this, 'email')} />
                                        {errorInd && <p className="hasError">
                                            {errors.message}
                                        </p>}
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12}>
                                    <FormGroup>
                                        <Label>Order notes</Label>
                                        <Input type="text" name="notes" className="orderCheckout" onChange={this.handleChange.bind(this, 'notes')} />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12} xs={12} className="cartCheckBox">
                                    <Input type="checkbox" onClick={this.onCheckBoxClick.bind(this)} />
                                    <Label>Please Check to Save Address</Label>
                                </Col>
                            </Row>
                            <Button onClick={this.onAddNew.bind(this, null)} className="user-address-btn-main">Find address</Button>

                            {/* modal */}

                            <Modal isOpen={this.state.modal} centered className="home-auth-modal">
                                <div className="user-address-modal">
                                    <div className='home-modal-close'>
                                        <button type="button" onClick={this.onAddNew.bind(this)} className="home-modal-close-button" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div className="address-modalOpen">
                                        <h1>Please Add New Address</h1>
                                        <Input className="input-optional" type="text" value={zonecode} placeholder="우편번호" onChange={this.handleChangeAddress.bind(this, 'zonecode')} />
                                        {errorInd && <p className="hasError">
                                            {errors.zonecode}
                                        </p>}
                                        <Button className="add-user-address-btn-main" onClick={this.addressDetails.bind(this)}>우편번호 찾기</Button>
                                        <Input className="input-optional" type="text" value={fullAddress} placeholder="주소" onChange={this.handleChangeAddress.bind(this, 'fullAddress')} />
                                        {errorInd && <p className="hasError">
                                            {errors.fullAddress}
                                        </p>}
                                        <Input className="input-optional" type="text" value={extraAddress} placeholder="참고항목" onChange={this.handleChangeAddress.bind(this, 'extraAddress')} />
                                        {errorInd && <p className="hasError">
                                            {errors.extraAddress}
                                        </p>}
                                        <Input className="input-optional" type="text" id="sample6_detailAddress" placeholder="상세주소 (optional)" value={extraDetails} onChange={this.handleChangeAddress.bind(this, 'extraDetails')} />
                                        <Button className="add-user-address-btn-main" onClick={this.addAddress.bind(this)} >Save</Button>
                                    </div>
                                </div>
                            </Modal>


                            {/* ADDRESS SECTION */}

                            {this.state.addAddress ? null :
                                <div className="cart-order-list-padding-address">
                                    {this.state.addData.map((val, index) => {
                                        return (
                                            <div key={index} onClick={this.selectAddresss.bind(this, val)}>
                                                <Row>
                                                    <div className={this.state.selectedAdrs === val._id ? "cart_address_section selectedAddress" : "cart_address_section"}>
                                                        <h3>Address</h3>
                                                        <ul className="cart-user-address-data">
                                                            <li>
                                                                <span>Jibun Address:</span><p>{val.fullAddress}</p>
                                                            </li>
                                                            <li>
                                                                <span>Zone code:</span><p>{val.zonecode}</p>
                                                            </li>
                                                            <li>
                                                                <span>Building Name:</span><p>{val.extraAddress}</p>
                                                            </li>
                                                            <li>
                                                                <span>Details :</span><p>{val.extraDetails}</p>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </Row>
                                            </div>
                                        )
                                    })}
                                </div>
                            }
                        </Form>
                    </Col>

                    {/* check out section */}

                    <Col md={4} sm={6} xs={12}>
                        <div className="checkout-back">
                            <Container fluid>
                                <Row>
                                    <div className="total-heading-out">
                                        <p>Cart Total</p>
                                    </div>
                                </Row>
                                <Row className="checkout-text">
                                    <Col md={6} xs={6} className="flex-start nopadding">
                                        <p>Product</p>
                                    </Col>
                                    <Col md={6} xs={6} className="flex-end">
                                        <p>Total</p>
                                    </Col>
                                </Row>
                                {this.state.data.map((val, index) => {
                                    return (
                                        <Row className="title-text" key={index}>
                                            <Col md={6} xs={6} className="flex-start nopadding">
                                                <p>{val.name}</p>
                                            </Col>
                                            <Col md={6} xs={6} className="flex-end">
                                                <label>${val.price * val.count}</label>
                                            </Col>
                                            <p style={{ display: 'none' }}>{subtotal = subtotal + (val.price * val.count)}</p>
                                        </Row>
                                    )
                                })}
                                <Row><div className="checkoutline"></div></Row>
                                <Row className="label-text alineitemcenter">
                                    <Col md={6} xs={6} className="nopadding alineitemcenter">
                                        <p>Subtotal</p>
                                    </Col>
                                    <Col md={6} xs={6} className="flex-end">
                                        <label>${subtotal}</label>
                                    </Col>
                                </Row>
                                <Row><div className="checkoutline"></div></Row>
                                <Row className="label-text alineitemcenter">
                                    <Col md={6} xs={6} className="nopadding">
                                        <p>Shipping</p>
                                    </Col>
                                    <Col md={6} xs={6} className="flex-end">
                                        <label>$14.00</label>
                                    </Col>
                                </Row>
                                <Row className="deliveryDate">
                                    <p>Express delivery 3-5 days</p>
                                </Row>

                                <Row><div className="checkoutline"></div></Row>

                                <Row className="label-text alineitemcenter">
                                    <Col md={6} xs={6} className="nopadding">
                                        <p>Total</p>
                                    </Col>
                                    <Col md={6} xs={6} className="flex-end">
                                        <label className="totalPrice">${subtotal + 14}</label>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={12} className="edit-order nopadding">
                                        <label onClick={this.onEditCart.bind(this)}><u>Edit order</u></label>
                                    </Col>
                                </Row>
                                <Row className="title-text">
                                    <Input type="checkbox" checked={this.state.checkedMain} className="checkBox" onClick={this.onClickSubmitTerms.bind(this)} />
                                    <p> I’ve read and accept the terms & conditions and the Privacy Policy</p>
                                </Row>
                                <Col md={12} className="alineCenter nopadding">
                                    <Button className="checkoutBtn" onClick={this.confirmPayment.bind(this, subtotal + 14)}>Confirm order and pay</Button>
                                </Col>
                            </Container>
                        </div>
                    </Col>
                </Container>
                <LightFooter />
            </div>
        )
    }
}
export default withRouter(compose(
    withApollo, [
        graphql(checkCartMutation, { name: 'createCart' }),
        graphql(addressMutaion, { name: 'userNewAddress' }),
        graphql(saveOrderMutation, { name: 'saveOrder' }),
        graphql(setToast, { name: 'setToast' })]
)(CheckOut))
