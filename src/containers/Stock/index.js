import React from 'react'
import Header from '../../components/Header'
import LightFooter from '../../components/LightFooter'
import { Button, Container, Row, Col, FormGroup, Label, Input } from 'reactstrap';
import './stock.css'
import MenuDrawer from '../../components/MenuOptions';
import MobileList from '../../components/MobileList';
import { withApollo, compose, graphql } from 'react-apollo';
import { withRouter } from 'react-router';
import { addToCartMutaion } from '../../Graphql/Mutations/userDetails';
import { setToast } from '../../actions/editor';
import { stopLoader, showLoader } from '../../utils/functions';
import { getMaterialsQuery, getBrandsQuery, getStockCasesQuery, getStockCasesByIdQuery } from '../../Graphql/Query/stocks';

class Stock extends React.Component {
    constructor() {
        super()
        this.state = {
            toggelbtn: false,
            toogleMenu: true,
            toggelbtnclose: false,
            oneCase: '',
            materials: [],
            models: [],
            themes: [],
            colors: [],
            brands: [],
            data: [],
            brandToShow: [],
            materialToShow: [],
            caseDetail: {},
            activeTabs: 1,
            showOnecCase: false,
            totalData: 0,
            page: 1, selectedBrand: "", selectedModel: "", selectedMaterial: "", selectedColor: "", selectedTheme: []
        }
    }
    componentDidMount(event) {
        let { materials, models } = this.state
        this.handleResize()
        window.addEventListener('resize', () => {
            this.handleResize()
        });

        ///////materials
        this.props.client.query({
            query: getMaterialsQuery,
        }).then(res => {
            let materialData = res.data.getMaterials.list
            this.setState({ materialToShow: materialData })
        })
            .catch(error => {
                console.log(error, "error")
            })

        /////brands
        this.props.client.query({
            query: getBrandsQuery,
        }).then(res => {
            let brandData = res.data.getBrands.list
            this.setState({ brandToShow: brandData })
        })
            .catch(error => {
                console.log(error, "error")
            })

        this.getStocks()
    }
    changePageNumber(page) {
        let { totalData } = this.state
        if (this.state.page * 10 < parseInt(totalData)) {
            this.setState({ page: page + 1 }, () => {
                this.getStocks('paginate')
            })
        }
    }

    changeFilter(type, event) {
        switch (type) {
            case 'brand':
                this.setState({ selectedBrand: event.target.value }, () => {
                    this.getStocks('filter')
                })
                break;
            case 'model':
                this.setState({ selectedModel: event.target.value }, () => {
                    this.getStocks('filter')
                })
                break;
            default:
                break;
        }
    }

    clickFilter(type, value) {
        switch (type) {
            case 'theme':
                let { selectedTheme } = this.state
                let valueToPop = selectedTheme.indexOf(value)
                if (valueToPop == -1) {
                    selectedTheme.push(value)
                    this.setState({ selectedTheme })
                }
                else {
                    selectedTheme.splice(valueToPop, 1)
                    this.setState({ selectedTheme })
                }
                this.setState({ selectedTheme }, () => {
                    this.getStocks('filter')
                })
                break;
            case 'color':
                this.setState({ selectedColor: value }, () => {
                    this.getStocks('filter')
                })
                break;
            case 'material':
                this.setState({ selectedMaterial: value }, () => {
                    this.getStocks('filter')
                })
                break;
            default:
                break;
        }
    }

    getStocks(type) {
        showLoader()
        let { selectedMaterial, selectedModel, selectedTheme, selectedColor, selectedBrand, page } = this.state
        this.setState({ showOnecCase: false })
        let filters = {}
        if (selectedBrand) {
            filters['brand'] = selectedBrand
        }
        if (selectedModel) {
            filters['model'] = selectedModel
        }
        if (selectedMaterial) {
            filters['material'] = selectedMaterial
        }
        if (selectedColor) {
            filters['colors'] = selectedColor
        }
        if (selectedTheme.length > 0) {
            filters['theme'] = JSON.stringify(selectedTheme)
        }
        if (page) {
            filters['page'] = page
        }
        if (type === 'filter') {
            filters['page'] = 1
            this.setState({ page: 1 })
        }
        this.props.client.query({
            query: getStockCasesQuery,
            fetchPolicy: "network-only",
            variables: filters
        }).then(res => {
            let StockData = res.data.getStockCases
            if (type === 'filter') {
                this.setState({ data: StockData.cases, totalData: StockData.total })
            } else {
                let dataToload = JSON.parse(JSON.stringify(this.state.data))
                if (StockData.cases.length > 0) {
                    StockData.cases.map(caseData => {
                        dataToload.push(caseData)
                    })
                }
                this.setState({ data: dataToload, totalData: StockData.total })
            }
            stopLoader()
        })
            .catch(error => {
                stopLoader()
                this.props.setToast({ variables: { type: 'error', message: "Some error occur please try again later" } })
                console.log(error, "error")
            })

    }

    handleResize() {
        if (window.innerWidth < 767) {
            this.setState({ toggelbtn: true, toogleMenu: false })
        }
        if (window.innerWidth > 768) {
            this.setState({ toggleMenu: true, toggelbtn: false, toggelbtnclose: false })
        }
    }
    toggleOpenMenu() {
        this.setState({ toogleMenu: true, toggelbtn: false, toggelbtnclose: true })
    }
    toggleCloseMEnu() {
        this.setState({ toogleMenu: false, toggelbtn: true, toggelbtnclose: false })
    }
    openCaseDesign(params) {
        showLoader()
        this.props.client.query({
            query: getStockCasesByIdQuery,
            fetchPolicy: "network-only",
            variables: {
                caseId: params
            }
        }).then(res => {
            let StockCaseData = res.data.getStockCasesById
            this.setState({ caseDetail: StockCaseData, showOnecCase: true })
            stopLoader()
        })
            .catch(error => {
                stopLoader()
                console.log(error, "error")
            })
    }
    addToCart() {
        let cartData = localStorage.getItem('cartData')
        let { data } = this.state
        if (!(cartData)) {
            this.props.setToast({
                variables: { type: 'error', message: "Please login First" }
            })
        } else {
            cartData = JSON.parse(cartData)
            cartData = JSON.parse(JSON.stringify(cartData))
            let cartItems = cartData.items
            cartItems.push(this.state.caseDetail)
            let newCart = []
            let userID = localStorage.getItem('id')
            if (cartItems.length >= 1) {
                cartItems.forEach((item, key) => {
                    if (item._id === null) {
                    } else {
                        let existanceCheck = newCart.map(function (e) { return e._id; }).indexOf(item._id);
                        if (existanceCheck === -1) {
                            newCart.push({
                                caseData: item.caseData,
                                count: item.count,
                                name: item.name,
                                preview: item.preview,
                                price: item.price,
                                _id: item._id,
                                brand: item.brand,
                                colors: item.colors,
                                material: item.material,
                                model: item.model,
                                theme: item.theme,
                                outlineSvgData: item.outlineSvgData
                            })
                        } else {
                            let count = newCart[existanceCheck].count
                            count = count + 1
                            newCart[existanceCheck].count = count
                        }

                    }
                });
            } else {
                newCart.push({
                    caseData: data.caseData,
                    count: data.count,
                    name: data.name,
                    preview: data.preview,
                    price: data.price,
                    _id: data._id,
                    brand: data.brand,
                    colors: data.colors,
                    material: data.material,
                    model: data.model,
                    theme: data.theme,
                    outlineSvgData: data.outlineSvgData
                })
            }
            this.props.addCart({
                variables: {
                    userId: userID,
                    subTotal: 0,
                    cartId: cartData._id,
                    items: newCart
                },
            }).then(res => {
                this.props.setToast({
                    variables: { type: 'success', message: "Item Added To Cart" }
                })
                cartData.items = newCart
                localStorage.setItem('cartData', JSON.stringify(cartData))
            })
                .catch(error => {
                    console.log(error, "error")
                    let errorText = { ...error }
                    if (errorText !== {}) {
                        this.props.setToast({
                            variables: { type: 'error', message: 'Some error occure please try again' }
                        })
                    }
                })
        }
    }
    onActiveTabs(id) {
        console.log(id)
        this.setState({ activeTabs: id })
    }
    render() {
        let { caseDetail } = this.state
        let { brandToShow, materialToShow } = this.state
        return (
            <div>
                <Header />
                <div className="stock-headin stock_main" >
                    <Container fluid className="no-padding">
                        <Row>
                            <div className="stock-flex-row">
                                <div className="stock-btn-toggle">
                                    {this.state.toggelbtn ? <Button className="toggleButton" outline onClick={this.toggleOpenMenu.bind(this)}>
                                        <div>
                                            <div class="bar1"></div>
                                            <div class="bar2"></div>
                                            <div class="bar3"></div>
                                        </div>
                                    </Button> : null}
                                    {this.state.toggelbtnclose ? <Button className="toggleButton" outline onClick={this.toggleCloseMEnu.bind(this)}>
                                        <i class="fa fa-close"></i>
                                    </Button> : null}
                                </div>

                            </div>
                            {
                                !this.state.showOnecCase ?
                                    <Col md={12}>
                                        <Row className="stock-heading-out">
                                            <div className="stock-heading-left">
                                                <h1>케이스 상점</h1>
                                            </div>
                                            <div className="stock-heading-right">
                                                <p className={this.state.activeTabs == 1 ? "colorBlue" : null} onClick={this.onActiveTabs.bind(this, 1)}>New </p>
                                                <p> | </p>
                                                <p className={this.state.activeTabs == 2 ? "colorBlue" : null} onClick={this.onActiveTabs.bind(this, 2)}> Popular</p>
                                            </div>
                                        </Row>
                                    </Col>
                                    : null
                            }
                        </Row>
                    </Container>
                </div>
                <div className="stock_main">
                    <Container fluid>
                        <Row>
                            <Col md={12}>
                                <Row>
                                    <Col md={3}>
                                        <div className={this.state.toogleMenu ? "stock-display-none activeMenu" : "stock-display-none"}>
                                            <MenuDrawer
                                                materials={this.state.materials}
                                                models={this.state.models}
                                                themes={this.state.themes}
                                                colors={this.state.colors}
                                                brands={this.state.brands}
                                                clickFilter={this.clickFilter.bind(this)}
                                                changeFilter={this.changeFilter.bind(this)}
                                            />
                                        </div>
                                    </Col>
                                    <Col md={9} className="case-container">
                                        {this.state.showOnecCase ?
                                            <Row className="selectedCaseCon">
                                                <div className="case-pic">
                                                    <div className="case-pic-out">
                                                        <img src={caseDetail.preview} className="case-image" />
                                                    </div>
                                                </div>
                                                <div className="case-margin-top">
                                                    <div>
                                                        <div className="case-title">
                                                            <p className="case-heading">{caseDetail.name}</p>
                                                            <p className="case-phone-name">{caseDetail.model} case</p>
                                                        </div>
                                                        <p className="case-price">${caseDetail.price}원</p>
                                                    </div>
                                                    <Label>제조사 선택</Label>
                                                    <Input type="select" name="selectMulti" className="menu-dropdown" value={this.state.selectedModel}>
                                                        <option default value="">All</option>
                                                        {brandToShow.length > 0 ?
                                                            brandToShow.map((val, index) => {
                                                                return <option>{val.name}</option>
                                                            })
                                                            :
                                                            <option disabled>No models available</option>
                                                        }
                                                    </Input>
                                                    <Label>케이스 종류</Label>
                                                    <Input type="select" name="selectMulti" className="menu-dropdown" value={this.state.selectedMaterial}>
                                                        <option default value="">All</option>
                                                        {materialToShow.length > 0 ?
                                                            materialToShow.map((val, index) => {
                                                                return <option>{val.name}</option>
                                                            })
                                                            :
                                                            <option disabled>No Material available</option>
                                                        }
                                                    </Input>
                                                    <div className="case-form-btn">
                                                        <Button onClick={this.addToCart.bind(this)}>장바구니 담기</Button>
                                                    </div>
                                                </div>
                                            </Row>
                                            : null}
                                        <MobileList data={this.state.data} caseId={this.openCaseDesign.bind(this)} changePageNumber={this.changePageNumber.bind(this)} page={this.state.page} />
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Container>
                </div>
                <LightFooter />
            </div>
        )
    }
}
export default withRouter(compose(
    withApollo, [
        graphql(addToCartMutaion, { name: 'addCart' }),
        graphql(setToast, { name: 'setToast' })
    ]
)(Stock))