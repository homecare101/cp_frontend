import React from 'react'
import Header from '../../components/Header';
import { Button, Input, Form, FormGroup, ListGroupItem, Container, Row, Col, Label, Collapse, CardBody, Card } from 'reactstrap';
import './admin.css'

class Collage extends React.Component {
    constructor() {
        super()
        this.state={
            data:[
                {_id:1, image:'/case-app-collages/collage1.png'},
                {_id:2, image:'/case-app-collages/collage2.png'},
                {_id:3, image:'/case-app-collages/collage3.png'},
                {_id:4, image:'/case-app-collages/collage4.png'},
                {_id:5, image:'/case-app-collages/collage5.png'},
                {_id:6, image:'/case-app-collages/collage6.png'},
                {_id:7, image:'/case-app-collages/collage7.png'},
                {_id:8, image:'/case-app-collages/collage8.png'},
                {_id:9, image:'/case-app-collages/collage9.png'},
                {_id:10, image:'/case-app-collages/collage10.png'},
                {_id:11, image:'/case-app-collages/collage11.png'},
                {_id:12, image:'/case-app-collages/collage12.png'},
                {_id:13, image:'/case-app-collages/collage13.png'},
                {_id:14, image:'/case-app-collages/collage14.png'},
                
        ]}
    }
    render() {
        return (
            <div>
                <Container fluid className="MainCon">
                   <Row>
                    <Col md={12} className="modelHeading">
                        <p>Editor / Collages</p>
                    </Col>
                    </Row>
                    <Row className="headingMain">
                        <Col md={6} sm={6} xs={12}>
                            <p>Manage Collages</p>
                        </Col>
                        <Col md={6} sm={6} xs={12} className="flexEnd">
                            <Button className="headingBtn">Add new Collage</Button>
                        </Col>
                    </Row>
                    <Row>
                        {
                            this.state.data.map((val, index) => {
                                return(
                                    <div className="collageFlexWrap">
                                        <div className="collageImg">
                                            <img src={val.image} className="colagePicture" />
                                        </div>
                                    </div>

                                )
                            })
                        }

                    </Row>

                </Container>  
            </div>
        )
    }
}
export default Collage;