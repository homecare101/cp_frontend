import React from 'react'
import Header from '../../components/Header';
import { Button, Input, Form, FormGroup, ListGroupItem, Container, Row, Col, Label, Collapse, CardBody, Card } from 'reactstrap';
import './admin.css'

class EditCase extends React.Component {
    constructor() {
        super()
    }
    render() {
        return (
            <Container fluid className="MainCon">
                <Row>
                    <Col md={4} xs={12}>
                        <Row>
                            <Col md={12} className="modelHeading">
                                <p>Editor / Models / Create new model  </p>
                            </Col>
                        </Row>
                        <Row className="headingMain">
                            <Col md={12} sm={12} xs={12}>
                                <p>New model</p>
                            </Col>
                        </Row>
                        <Label className="labelNameMobile">Name</Label>
                        <Input type="select" name="selectMulti" className="manage-dropdown mgb">
                            <option>Apple</option>
                            <option>Samsung</option>
                            <option>Nokia</option>
                            <option>Oppo</option>
                            <option>MI</option>
                        </Input>
                        <Label className="labelNameMobile">Category</Label>
                        <Input type="select" name="selectMulti" className="manage-dropdown mgb">
                            <option>Iphone7</option>
                            <option>Iphone6</option>
                            <option>Iphone5</option>
                            <option>Iphone4</option>
                            <option>IphoneX</option>
                        </Input>
                        <Label className="labelNameMobile">Price</Label>
                        <Input type="text" name="selectMulti" className="manage-dropdown mgb">
                        </Input>
                        <Col md={8}>
                            <Button className="deleteCase">
                                Save Changes
                            </Button>
                        </Col>
                    </Col>
                    <Col md={8}  className="phonePosition">
                        <div className="phonePositionOut">
                            <Label className="labelNameMobile">Case</Label>
                            <div className="editCaseImgout">
                                <Label className="labelNameMobile">Upload Case</Label>
                            </div>
                        </div>

                    </Col>
                </Row>
            </Container>
        )
    }
}
export default EditCase;