import React from 'react'
import Header from '../../components/Header';
import { Button, ListGroupItem, Container, Row, Col, Label, Collapse, CardBody, Card } from 'reactstrap';
import './admin.css'
import ManageModel from './ManageModel';
import AddCase from './AddCase';
import Materials from './Materials';
import Collage from './Collage';
import ClipArts from './ClipArts';
import { Route } from 'react-router-dom';
import AdminStocks from './AdminStocks';
import AllUsers from './AllUsers';
import { Link } from 'react-router-dom'
import AllOrders from './AllOrders';
import LightFooter from '../../components/LightFooter';

class Admin extends React.Component {
    constructor() {
        super()
        this.state = {
            collapse: true,
            collapse2: false,
            toggelbtn: false,
            toogleMenu: true,
            toggelbtnclose: false,
            selectedTab: ''
        };
    }
    componentDidMount(event) {
        this.handleResize()
        window.addEventListener('resize', () => {
            this.handleResize()
        });
    }
    handleResize() {
        if (window.innerWidth < 767) {
            this.setState({ toggelbtn: true, toogleMenu: false })
        }
        if (window.innerWidth > 768) {
            this.setState({ toggleMenu: true, toggelbtn: false, toggelbtnclose: false })
        }
    }
    toggleOpenMenu() {
        this.setState({ toogleMenu: true, toggelbtn: false, toggelbtnclose: true })
    }
    toggleCloseMEnu() {
        this.setState({ toogleMenu: false, toggelbtn: true, toggelbtnclose: false })
    }
    toggle() {
        this.setState({ collapse: !this.state.collapse, collapse2: false });
    }
    toggle2() {
        const {match} = this.props
        this.props.history.push(`${match.url}/stocks`)
        this.setState({ collapse2: !this.state.collapse2, collapse: false });
    }

    handleClick(id) {
        const {match} = this.props
        this.setState({ selectedTab: id })
        switch (id) {
            case 1:
                return this.props.history.push(`${match.url}/models`)
                break;
            case 2:
                return this.props.history.push(`${match.url}/collages`)
                break;
            case 3:
                return this.props.history.push(`${match.url}/materials`)
                break;
            case 4:
                return this.props.history.push(`${match.url}/cliparts`)
                break;
        }
    }
    render() {
        var { selectedTab, collapse, collapse2 } = this.state
        const {match} = this.props
        return (
            <div>
                <Header label={true} />
                <div className="admin-btn-toggle">
                    {this.state.toggelbtn ? <Button className="toggelbtncross" outline onClick={this.toggleOpenMenu.bind(this)}>
                        <div>
                            <div class="bar1"></div>
                            <div class="bar2"></div>
                            <div class="bar3"></div>
                        </div>
                    </Button> : null}
                    {this.state.toggelbtnclose ? <Button className="toggelbtncross" outline onClick={this.toggleCloseMEnu.bind(this)}>
                        <i class="fa fa-close"></i>
                    </Button> : null}
                </div>
                <Container fluid>
                    <Row>
                        <div className={this.state.toogleMenu ? "admin-display-none activeMenu nopadding rightBorder" : "admin-display-none nopadding rightBorder"}>
                            <div className="admin-drop-text">
                                <div onClick={this.toggle.bind(this)} className="arrowIcon">
                                    <p className="externalBtn" >Editor</p>
                                    <div className="iconArrow">
                                        {collapse ?
                                            <i class="fa fa-angle-up"></i> :
                                            <i class="fa fa-angle-down"></i>}
                                    </div>
                                </div>
                                <Collapse isOpen={this.state.collapse} className="internalBtn">
                                    <ListGroupItem onClick={this.handleClick.bind(this, 1)} className={this.state.selectedTab == 1 ? "adminTabMenu" : "admintabMenuClose"} >Models</ListGroupItem>
                                    <ListGroupItem onClick={this.handleClick.bind(this, 2)} className={this.state.selectedTab == 2 ? "adminTabMenu" : "admintabMenuClose"} >Collages</ListGroupItem>
                                    <ListGroupItem onClick={this.handleClick.bind(this, 3)} className={this.state.selectedTab == 3 ? "adminTabMenu" : "admintabMenuClose"} >Materials</ListGroupItem>
                                    <ListGroupItem onClick={this.handleClick.bind(this, 4)} className={this.state.selectedTab == 4 ? "adminTabMenu" : "admintabMenuClose"} >Cliparts</ListGroupItem>
                                </Collapse>
                            </div>
                            <div className="admin-drop-text">
                                <div onClick={this.toggle2.bind(this)} className="arrowIcon">
                                    <p className="externalBtn" >Stock</p>
                                </div>
                            </div>
                            <Link to="/admin/users">
                                <div className="admin-drop-text">
                                    <p className="externalBtn">User</p>
                                </div>
                            </Link>
                            <Link to="/admin/orders">
                                <div className="admin-drop-text">
                                    <p className="externalBtn">Orders</p>
                                </div>
                            </Link>
                            </div>
                        <Col md={10}>
                            {/* {this.returnComponent()} */}
                            <div>
                                <Route
                                    exact
                                        path={`${match.url}/models`}
                                        component={ManageModel}
                                    />
                                <Route
                                        path={`${match.url}/collages`}
                                        component={Collage}
                                    />
                                <Route
                                        path={`${match.url}/materials`}
                                        component={Materials}
                                    />
                                <Route
                                        path={`${match.url}/cliparts`}
                                        component={ClipArts}
                                    />
                                <Route
                                        exact
                                        path={`${match.url}/addCase`}
                                        component={AddCase}
                                    />
                                <Route
                                        exact
                                        path={`${match.url}/editCase/:caseId`}
                                        component={AddCase}
                                    />
                                <Route
                                        path={`${match.url}/stocks`}
                                        component={AdminStocks}
                                    />
                                <Route
                                        path={`${match.url}/users`}
                                        component={AllUsers}
                                    />
                                <Route
                                        path={`${match.url}/orders`}
                                        component={AllOrders}
                                    />
                            </div>
                        </Col>
                    </Row>
                </Container>
                <LightFooter />
            </div>
        )
    }
}
export default Admin;