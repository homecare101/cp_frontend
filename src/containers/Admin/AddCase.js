import React from 'react'
import Header from '../../components/Header';
import { Button, Input, Form, FormGroup, ListGroupItem, Container, Row, Col, Label, Collapse, CardBody, Card } from 'reactstrap';
import './admin.css'
import { IsValidForm } from '../../components/common/validation';
import { compose, graphql, withApollo } from 'react-apollo';
import { withRouter } from 'react-router';
import { addStockMutation, editStockCaseMutation } from '../../Graphql/Mutations/stock';
import { setToast } from '../../actions/editor';
import Select from 'react-select';
import { showLoader, stopLoader } from '../../utils/functions';
import { getMaterialsQuery, getModelsQuery, getThemesQuery, getColorsQuery, getBrandsQuery, getStockCasesByIdQuery } from '../../Graphql/Query/stocks';
class AddCase extends React.Component {
    constructor() {
        super()
        this.state = {
            imageSelected: false,
            preview: '',
            stockCases: {
                name: '',
                price: '',
                material: 'material2',
                model: 'S3',
                theme: 'rain',
                color: [],
                brand: 'MI',
                ProductDetail: ''
            },
            errorInd: false,
            errorImage: false,
            error: {},
            materials: [],
            models: [],
            themes: [],
            colors: [],
            brands: [],
            onEditable: false,
            selectedOption: null
        }
    }

    componentWillMount() {
        let { stockCases, preview } = this.state

        let caseId = this.props.match.params.caseId

        if (caseId) {
            this.setState({ onEditable: true })
        }

        this.props.client.query({
            query: getStockCasesByIdQuery,
            variables: {
                caseId: caseId
            }
        }).then(res => {
            stockCases = JSON.parse(JSON.stringify(res.data.getStockCasesById))
            preview = JSON.parse(JSON.stringify(res.data.getStockCasesById.preview))
            this.setState({ stockCases, preview ,imageSelected: true, selectedOption : stockCases.colors})
        })
            .catch(error => {
                console.log(error, "error")
            })

        ///////materials
        this.props.client.query({
            query: getMaterialsQuery,
        }).then(res => {
            let materialData = res.data.getMaterials.list
            this.setState({ materials: materialData })
        })
            .catch(error => {
                console.log(error, "error")
            })
        /////model
        this.props.client.query({
            query: getModelsQuery,
        }).then(res => {
            let modelsData = res.data.getModels.list
            this.setState({ models: modelsData })
        })
            .catch(error => {
                console.log(error, "error")
            })
        /////themes
        this.props.client.query({
            query: getThemesQuery,
        }).then(res => {
            let themedata = res.data.getThemes.list
            this.setState({ themes: themedata })
        })
            .catch(error => {
                console.log(error, "error")
            })
        /////color
        this.props.client.query({
            query: getColorsQuery,
        }).then(res => {
            let colorsData = res.data.getColors.list
            this.setState({ colors: colorsData })
        })
            .catch(error => {
                console.log(error, "error")
            })
        /////brands
        this.props.client.query({
            query: getBrandsQuery,
        }).then(res => {
            let brandData = res.data.getBrands.list
            this.setState({ brands: brandData })
        })
            .catch(error => {
                console.log(error, "error")
            })
    }
    handleChange(key, event) {
        let value = event.target.value;
        let { stockCases } = this.state
        stockCases[key] = value
        this.setState({ stockCases });
    }
    handleChangeColor = (selectedOption) => {
        let {stockCases} = this.state
        let colorArray =[]
        selectedOption.forEach((val,key) => {
            colorArray.push({
                _id: val._id,
                label: val.label,
                value: val.value
            })
        });
        stockCases.color = colorArray
        this.setState({ stockCases, selectedOption});
      }
    handleImage(event) {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                this.setState({ preview: e.target.result })
            };
            reader.readAsDataURL(event.target.files[0])
        }
        this.setState({ imageSelected: true })
    }
    onClickBtn() {
        let { preview } = this.state
        let fields = ['name', 'price']
        let formValidation = IsValidForm(fields, this.state.stockCases)

        if (formValidation.validate) {
            if (preview === '') {
                this.setState({ errorImage: true, errors: formValidation.errors })
            } else {
                this.setState({ errorInd: false })
                let { name, price, material, model, theme, color, brand } = this.state.stockCases
                let userId = localStorage.getItem('id')
                showLoader()
                this.props.addStock({
                    variables: {
                        name: name,
                        price: price,
                        preview: preview,
                        material: material,
                        model: model,
                        theme: theme,
                        colors: color,
                        count: 1,
                        userId: userId,
                        brand: brand

                    }
                }).then(response => {
                    stopLoader()
                    this.props.setToast({
                        variables: { type: 'success', message: "New Case Added" }
                    })
                    const { match } = this.props
                    this.props.history.push(`/admin/stocks`)
                }).catch(error => {
                    console.log(error, "error")
                    this.props.setToast({
                        variables: { type: 'error', message: "Payload Too Large" }
                    })
                    stopLoader()
                })
            }
        } else {
            stopLoader()
            this.setState({ errorInd: true, errors: formValidation.errors })
        }
    }
    onEditClick(){
        let { preview } = this.state
        let caseId = this.props.match.params.caseId
        let fields = ['name', 'price']
        let formValidation = IsValidForm(fields, this.state.stockCases)

        if (formValidation.validate) {
            if (preview === '') {
                this.setState({ errorImage: true, errors: formValidation.errors })
            } else {
                this.setState({ errorInd: false })
                let { name, price, material, model, theme, color, brand } = this.state.stockCases
                let userId = localStorage.getItem('id')
                this.props.editStock({
                    variables: {
                        name: name,
                        price: price,
                        preview: preview,
                        material: material,
                        model: model,
                        theme: theme,
                        colors: color,
                        count: 1,
                        userId: userId,
                        brand: brand,
                        caseId: caseId,
                        userId: userId
                    }
                }).then(response => {
                    this.props.setToast({
                        variables: { type: 'success', message: "Case edited Added" }
                    })
                    const { match } = this.props
                    this.props.history.push(`/admin/stocks`)
                }).catch(error => {
                    console.log(error, "error 	")
                    this.props.setToast({
                        variables: { type: 'error', message: "Payload Too Large" }
                    })
                    stopLoader()
                })
            }
        } else {
            this.setState({ errorInd: true, errors: formValidation.errors })
        }
    }
    optionClicked(optionsList) {
        this.setState({ multiSelect: optionsList });
    }
    selectedBadgeClicked(optionsList) {
            this.setState({ multiSelect: optionsList });
    }
    render() {
        let { errorInd, errors, errorImage } = this.state
        let { material, model, theme, color, brand, name, price } = this.state.stockCases
        const { materials, models, themes, colors, brands, selectedOption} = this.state
        return (
            <Container fluid className="MainCon">
                <Row>
                    <Col md={4} xs={12}>
                        <Row>
                            <Col md={12} className="modelHeading">
                                <p>Admin</p>
                            </Col>
                        </Row>

                        <Row className="headingMain">
                            <Col md={12} sm={12} xs={12}>
                                <p>Add New Case</p>
                            </Col>
                        </Row>

                        <div className="mgb">
                            <Label className="labelNameMobile">Name</Label>
                            <Input type="text" name="name" className="manage-dropdown" value={name} onChange={this.handleChange.bind(this, 'name')}></Input>
                            {errorInd && <p className="addCaseHasError">
                                {errors.name}
                            </p>}
                        </div>

                        <Label className="labelNameMobile">Brand</Label>
                        <Input type="select" name="selectMulti" className="menu-dropdown" value={brand} onChange={this.handleChange.bind(this, 'brand')}>
                            {brands.length > 0 ?
                                brands.map((val, index) => {
                                    return <option>{val.name}</option>
                                })
                                :
                                <option disabled>No Brands available</option>
                            }
                        </Input>

                        <Label className="labelNameMobile">Model</Label>
                        <Input type="select" name="selectMulti" className="menu-dropdown" value={model} onChange={this.handleChange.bind(this, 'model')}>
                            {models.length > 0 ?
                                models.map((val, index) => {
                                    return <option value={val.name}>{val.name}</option>
                                })
                                :
                                <option disabled>No models available</option>
                            }
                        </Input>

                        <Label className="labelNameMobile">Material</Label>
                        <Input type="select" name="selectMulti" className="menu-dropdown" value={material} onChange={this.handleChange.bind(this, 'material')}>
                            {materials.length > 0 ?
                                materials.map((val, index) => {
                                    return <option>{val.name}</option>
                                })
                                :
                                <option disabled>No Material available</option>
                            }
                        </Input>

                        <Label className="labelNameMobile">Color</Label>
                        <Select
                            className="menu-dropdown"
                            value={selectedOption}
                            onChange={this.handleChangeColor}
                            options={colors}
                            isMulti = {true}
                        />
                        
                        
                        <Label className="labelNameMobile">Themes</Label>
                        <Input type="select" name="selectMulti" className="menu-dropdown" value={theme} onChange={this.handleChange.bind(this, 'theme')}>
                            {themes.length > 0 ?
                                themes.map((val, index) => {
                                    return <option>{val.name}</option>
                                })
                                :
                                <option disabled>No Themes available</option>
                            }
                        </Input>
                        <div className="mgb">
                            <Label className="labelNameMobile">Product Detail</Label>
                            <Input type="text" name="Product Detail" className="manage-dropdown" value={price} onChange={this.handleChange.bind(this, 'Product Detail')}>
                            </Input>
                            {errorInd && <p className="addCaseHasError">
                                {errors.name}
                            </p>}
                        </div>
                        <div className="mgb">
                            <Label className="labelNameMobile">Price</Label>
                            <Input type="text" name="price" className="manage-dropdown" value={price} onChange={this.handleChange.bind(this, 'price')}>
                            </Input>
                            {errorInd && <p className="addCaseHasError">
                                {errors.name}
                            </p>}
                        </div>

                        {
                            this.state.onEditable ?
                                <Col md={8}>
                                    <Button className="saveBtnCase" onClick={this.onEditClick.bind(this)}>
                                        Edit
                                    </Button>
                                </Col>
                                :
                                <Col md={8}>
                                    <Button className="saveBtnCase" onClick={this.onClickBtn.bind(this)}>
                                        Save
                              </Button>
                                </Col>
                        }

                    </Col>

                    <Col md={8} className="phonePosition">
                        <div className="phonePositionOut">
                            {this.state.imageSelected ?
                                <div className="imagePreview">
                                    <Label className="editCase" >
                                        <img src={this.state.preview} className="imageFix" />

                                            <Input type="file" name="image" onChange={this.handleImage.bind(this)} accept="image" className="fileInput" />
                                           <p>Upload New case</p> 
                                    </Label>
                                </div>
                                :
                                <div>
                                    <Label className="addNewCaseImageOut">
                                        <Label className="uploadCase" >
                                            <Input type="file" name="image" onChange={this.handleImage.bind(this)} accept="image" className="fileInput" />
                                            Upload case
                                        </Label>
                                    </Label>
                                    {errorImage && <p className="addCaseHasError">
                                        This feild Require
                                    </p>}
                                </div>
                            }
                        </div>
                    </Col>
                </Row>
            </Container>
        )
    }
}
export default withRouter(compose(
    withApollo,
    [graphql(addStockMutation, { name: 'addStock' }),
    graphql(editStockCaseMutation, { name: 'editStock' }),
    graphql(setToast, { name: 'setToast' })]
)(AddCase));