import React from 'react';
import { Table } from 'reactstrap';
import { showLoader, stopLoader } from '../../utils/functions';
import { withRouter } from 'react-router';
import { withApollo, compose, graphql } from 'react-apollo';
import { setToast } from '../../actions/editor';
import { getAllUsersQuery } from '../../Graphql/Query/auth';

class AllUsers extends React.Component {
    constructor() {
        super()
        this.state = {
            users: []
        }
    }
    componentDidMount() {
        showLoader()
        let userId = localStorage.getItem('id')
        this.props.client.query({
            query: getAllUsersQuery,
            fetchPolicy: "network-only",
            variables: {
                userId: userId,
            },
        }).then(res => {
            let Users = res.data.getAllUsers.usersList
            this.setState({ users: Users })
            stopLoader()
        })
            .catch(error => {
                console.log(error, "error")
            })
    }
    render() {
        let { users } = this.state
        return (
            <div className="tableMain">
            <Table >
                <thead>
                    <tr className="tableHeading">
                        <th>#</th>
                        <th>User ID</th>
                        <th>Email</th>
                        <th>Role</th>
                    </tr>
                </thead>
                {users.map((val, index) => {
                    return (
                        <tbody key={index} className="tableBody">
                            <tr className="tableHeading">
                                <th scope="row">{index + 1}</th>
                                <td>{val._id}</td>
                                <td>{val.email}</td>
                                <td>{val.role}</td>
                            </tr>
                        </tbody>
                    )
                })}
            </Table>
            </div>

        )
    }
}
export default withRouter(compose(
    withApollo,
    graphql(setToast, { name: 'setToast' })
)(AllUsers))