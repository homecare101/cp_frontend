import React from 'react'
import { Button, Container, Row, Col, Label, Collapse, CardBody, Card } from 'reactstrap';
import './admin.css'
import { Link } from 'react-router-dom'
import { withApollo, compose, graphql } from 'react-apollo';
import { withRouter } from 'react-router';
import { setToast } from '../../actions/editor';
import { showLoader, stopLoader} from '../../utils/functions';
import { getStockCasesQuery, deleteStockCase } from '../../Graphql/Query/stocks';

class AdminStocks extends React.Component {
    constructor() {
        super()
        this.state = {
            data: [],
            selected: false,
            selectedId: null,
            page:1,
            totalData:0
        }
    }
    componentDidMount(){
        this.getCases()
        window.addEventListener('scroll', this.handleScroll);
    }
     componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    };
    getCases () {
        let { page } = this.state
        showLoader()
        this.props.client.query({
            query: getStockCasesQuery,
            fetchPolicy: "network-only",
                 variables: {
                    page: page,
                },
        }).then(res => {
            let StockData = res.data.getStockCases
            console.log(StockData,"StockDataStockData")
            let dataToload = JSON.parse(JSON.stringify(this.state.data))
            // if( StockData.cases.length > 0 ){
            //     StockData.cases.map(caseData => {
            //         dataToload.push(caseData)
            //     })
            // }
            this.setState({data: StockData.cases, totalData: StockData.total})
            stopLoader()
        })
        .catch(error => {
            console.log(error, "error")
        })
    }
    onAddCase(){
        const {match} = this.props
        this.props.history.push(`/admin/addcase`)
    }
    onImageClick(id){
        this.setState({selectedId:id})
    }
    onDeleteCase(caseId){
        let { data } = this.state 
        showLoader()
        let userId = localStorage.getItem('id')
        this.props.client.query({
            query: deleteStockCase,
            fetchPolicy: "network-only",
            variables: {
               userId: userId,
               caseId: caseId
            }
        }).then(res => {
            this.getCases()
            // console.log(this.state.data, caseId)
            // var index = data.findIndex(val => val._id == caseId)
            // console.log(index)
            // data.splice(index, 1);
            this.props.setToast({
                variables: { type: 'success', message: "Case Deleted Succesfully" }
            })
            stopLoader()
        })
        .catch(error => {
            console.log(error, "error")
        })
    }
    onEditCase(caseId){

    }
    scrolled(event){
        let { page , totalData} = this.state   
        if(this.refs.scrollHeight.offsetHeight + this.refs.scrollHeight.scrollTop == this.refs.scrollHeight.scrollHeight)
        {   
             if(this.state.page * 10 < parseInt(totalData)){
                this.setState({page: page + 1},()=>{
                    this.getCases()
                })
            }
        }
    }

    render() {
        let { showBtns } = this.state
        return (
            <Container fluid className="MainCon">
                <Row>
                    <Col md={12} className="modelHeading">
                        <p>Stocks / Cases</p>
                    </Col>
                </Row>
                <Row className="headingMain">
                    <Col md={6} sm={6} xs={12}>
                        <p>Admin Stocks</p>
                    </Col>
                    <Col md={6} sm={6} xs={12} className="flexEnd">
                        <Button onClick={this.onAddCase.bind(this)} className="headingBtn">Add new cases</Button>
                    </Col>
                </Row>
                <Col md={12} className="mobileList-manage">
                    <div className="mobileConHeight" ref="scrollHeight" onScroll={this.scrolled.bind(this)}>
                        <Row className="modelCon">
                            {this.state.data.map((val, index) => {
                                return (
                                    <Col md={3} sm={4} key={index}>
                                        <div className="model-img-out">
                                            <div className="model-image-out">
                                                    <img src={val.preview} onClick={this.onImageClick.bind(this, val._id)} className={this.state.selectedId === val._id ? "model-image opac-image" : "model-image"}/>
                                                        <div className={this.state.selectedId === val._id ? "displayBlock" : "displayNone"}>
                                                        <Link to={`/admin/editCase/`+val._id}><Button className="imageTextBtnEdit" onClick={this.onEditCase.bind(this, val._id)}>Edit</Button></Link>
                                                            <Button className="imageTextBtn" onClick={this.onDeleteCase.bind(this, val._id)}>Delete</Button>
                                                        </div> 
                                                <p className="model-img-title">{val.name}</p>
                                            </div>
                                        </div>
                                    </Col>
                                )
                            }
                            )}
                        </Row>
                    </div>
                </Col>
            </Container>
        )
    }
}

export default withRouter(compose(
    withApollo,
    graphql(setToast, { name: 'setToast' })
)(AdminStocks))