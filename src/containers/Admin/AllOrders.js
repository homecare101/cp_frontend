import React from 'react';
import { Table } from 'reactstrap';
import { showLoader, stopLoader } from '../../utils/functions';
import { withRouter } from 'react-router';
import { withApollo, compose, graphql } from 'react-apollo';
import { setToast } from '../../actions/editor';
import { Input, Button, Modal } from 'reactstrap'
import OrderDetails from './OrderDetails';
import { changeOrderStatusQuery, getAllOrdersQuery } from '../../Graphql/Query/order';

class AllOrders extends React.Component {
    constructor() {
        super()
        this.state = {
            orders: [],
            status: ['Pending', 'Shipped', 'Delivered', 'Returned', 'Canceled'],
            openModal: false,
            orderToSend: null
        }
    }
    componentDidMount() {
        showLoader()
        this.getOrderData()
    }
    getOrderData() {
        let userId = localStorage.getItem('id')
        this.props.client.query({
            query: getAllOrdersQuery,
            fetchPolicy: "network-only",
            variables: {
                userId: userId,
            },
        }).then(res => {
            let responce = res.data.getAllOrders.order
            responce = JSON.parse(JSON.stringify(responce))
            this.setState({ orders: responce })
            stopLoader()
        })
            .catch(error => {
                console.log(error, "error")
            })
    }
    onSelectStatus(val, event) {
        let status = event.target.value
        let userId = localStorage.getItem('id')
        showLoader()
        this.props.client.query({
            query: changeOrderStatusQuery,
            variables: {
                userId: userId,
                orderId: val._id,
                status: status
            },
        }).then(res => {
            this.getOrderData()
        })
            .catch(error => {
                console.log(error, "error")
            })
    }
    openPdfModal(val) {
        this.setState({ openModal: true, orderToSend: val })
    }
    loginToggle() {
        this.setState({ openModal: false })
    }
    render() {
        let { orders } = this.state
        return (
            <div className="tableMain">
                <Table >
                    <thead>
                        <tr className="tableHeading">
                            <th>#</th>
                            <th>OrderId</th>
                            <th>Order Date</th>
                            <th>User Name</th>
                            <th>Address</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Change Status</th>
                            <th>Preview</th>
                        </tr>
                    </thead>
                    {orders.map((val, index) => {
                        return (
                            <tbody key={index} className="tableBody">
                                <tr className="tableHeading">
                                    <th scope="row">{index + 1}</th>
                                    <td>{val._id}</td>
                                    <td>{val.createdAt}</td>
                                    <td>{val.firstName + ' ' + val.lastName}</td>
                                    <td>{val.fullAddress}</td>
                                    <td>{val.email}</td>
                                    <td>{val.mobileNumber}</td>
                                    <td>
                                        <Input type="select" name="selectMulti" className="menu-dropdown" value={val.status} onChange={this.onSelectStatus.bind(this, val)} >
                                            {
                                                this.state.status.map((val, index) => {
                                                    return <option value={val}>{val}</option>
                                                })
                                            }
                                        </Input>
                                    </td>
                                    <td className="iconDownloadOut"><i className="fa fa-download iconDownloadSize" onClick={this.openPdfModal.bind(this, val)} /></td>
                                </tr>
                            </tbody>
                        )
                    })}
                </Table>
                <Modal isOpen={this.state.openModal} centered className="home-auth-modal">
                    <div>
                        <div className='home-modal-close'>
                            <button type="button" onClick={this.loginToggle.bind(this)} className="home-modal-close-button" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <OrderDetails orderToSend={this.state.orderToSend} />
                    </div>
                </Modal>
            </div>
        )
    }
}
export default withRouter(compose(
    withApollo,
    graphql(setToast, { name: 'setToast' })
)(AllOrders))