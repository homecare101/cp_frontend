import React from 'react'
import Header from '../../components/Header';
import { Button, Input, Form, FormGroup, ListGroupItem, Container, Row, Col, Label, Collapse, CardBody, Card } from 'reactstrap';
import './admin.css'



class ClipArts extends React.Component {
    constructor() {
        super()
        this.state={
            data:[
                {_id:1, image:'https://caseapp.com/images/attachment/2015/03/13/325_thumb.png'},
                {_id:2, image:'https://caseapp.com/images/attachment/2015/03/13/252_thumb.png'},
                {_id:3, image:'https://caseapp.com/images/attachment/2015/05/07/2387_thumb.png'},
                {_id:4, image:'https://caseapp.com/images/attachment/2015/03/13/277_thumb.png'},
                {_id:5, image:'https://caseapp.com/images/attachment/2015/03/13/325_thumb.png'},
                {_id:6, image:'https://caseapp.com/images/attachment/2015/03/13/252_thumb.png'},
                {_id:7, image:'https://caseapp.com/images/attachment/2015/03/13/325_thumb.png'},
                {_id:8, image:'https://caseapp.com/images/attachment/2015/03/13/277_thumb.png'},
                {_id:9, image:'https://caseapp.com/images/attachment/2015/05/07/2387_thumb.png'},
                {_id:10, image:'https://caseapp.com/images/attachment/2015/03/13/325_thumb.png'},
                {_id:11, image:'https://caseapp.com/images/attachment/2015/03/13/277_thumb.png'},
                {_id:12, image:'https://caseapp.com/images/attachment/2015/03/13/252_thumb.png'},
                {_id:13, image:'https://caseapp.com/images/attachment/2015/05/07/2387_thumb.png'},
                {_id:14, image:'https://caseapp.com/images/attachment/2015/03/13/325_thumb.png'},
                {_id:1, image:'https://caseapp.com/images/attachment/2015/03/13/325_thumb.png'},
                {_id:2, image:'https://caseapp.com/images/attachment/2015/03/13/252_thumb.png'},
                {_id:3, image:'https://caseapp.com/images/attachment/2015/05/07/2387_thumb.png'},
                {_id:4, image:'https://caseapp.com/images/attachment/2015/03/13/277_thumb.png'},
                {_id:5, image:'https://caseapp.com/images/attachment/2015/03/13/325_thumb.png'},
                {_id:6, image:'https://caseapp.com/images/attachment/2015/03/13/252_thumb.png'},
                {_id:7, image:'https://caseapp.com/images/attachment/2015/03/13/325_thumb.png'},
                {_id:8, image:'https://caseapp.com/images/attachment/2015/03/13/277_thumb.png'},
                {_id:9, image:'https://caseapp.com/images/attachment/2015/05/07/2387_thumb.png'},
                {_id:10, image:'https://caseapp.com/images/attachment/2015/03/13/325_thumb.png'},
                {_id:11, image:'https://caseapp.com/images/attachment/2015/03/13/277_thumb.png'},
                {_id:12, image:'https://caseapp.com/images/attachment/2015/03/13/252_thumb.png'},
                {_id:13, image:'https://caseapp.com/images/attachment/2015/05/07/2387_thumb.png'},
                {_id:14, image:'https://caseapp.com/images/attachment/2015/03/13/325_thumb.png'},
        ]}
    }
    render() {
        return (
            <div>
                <Container fluid className="MainCon">
                   <Row>
                    <Col md={12} className="modelHeading">
                        <p>Editor / Cliparts</p>
                    </Col>
                    </Row>
                    <Row className="headingMain">
                        <Col md={6} sm={6} xs={12}>
                            <p>Manage Cliparts</p>
                        </Col>
                        <Col md={6} sm={6} xs={12} className="flexEnd">
                            <Button className="headingBtn">Add new Cliparts</Button>
                        </Col>
                    </Row>
                    <Row>
                        {
                            this.state.data.map((val, index) => {
                                return(
                                    <div className="collageFlexWrap">
                                        <div className="clipImg">
                                            <img src={val.image} className="colagePicture" />
                                        </div>
                                    </div>

                                )
                            })
                        }

                    </Row>

                </Container>  
            </div>
        )
    }
}
export default ClipArts;