import React from 'react'
import { Button, Input, Form, FormGroup, ListGroupItem, Container, Row, Col, Label, Collapse, CardBody, Card } from 'reactstrap';
import './admin.css'
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router';
import { getStockCasesQuery } from '../../Graphql/Query/stocks';

class ManageModel extends React.Component {
    constructor() {
        super()
        this.state = {
            data: [],
            selected: false
        }
    }
    componentDidMount(){
        this.props.client.query({
            query: getStockCasesQuery,
            fetchPolicy: "network-only"
        }).then(res => {
            let StockData = res.data.getStockCases.cases
            this.setState({data: StockData})
        })
        .catch(error => {
            console.log(error, "error")
        })
    }
    onImgClick(id) {
        var { selected } = this.state.selected
        this.setState({ selected: !selected })
        // this.props.isOpenEdit(true)        
        const {match} = this.props
        this.props.history.push(`/admin/editcase`)


    }
    onBtnClick(){
        // this.props.isOpen(true)
    //     const {match} = this.props
    //  this.props.history.push(`/admin/addcase`)

    }
    render() {
        return (
            <Container fluid className="MainCon">
                <Row>
                    <Col md={12} className="modelHeading">
                        <p>Editor / Models</p>
                    </Col>
                </Row>
                <Row className="headingMain">
                    <Col md={6} sm={6} xs={12}>
                        <p>Manage models</p>
                    </Col>
                    <Col md={6} sm={6} xs={12} className="flexEnd">
                        <Button onClick={this.onBtnClick.bind(this)} className="headingBtn">Add New Case</Button>
                    </Col>
                </Row>
                <Row>
                    <Col md={4} xs={6}>
                        <Label className="labelNameMobile">Brand</Label>
                        <Input type="select" name="selectMulti" className="manage-dropdown">
                            <option>Apple</option>
                            <option>Samsung</option>
                            <option>Nokia</option>
                            <option>Oppo</option>
                            <option>MI</option>
                        </Input>
                    </Col>
                    <Col md={4} xs={6}>
                        <Label className="labelNameMobile">Model</Label>
                        <Input type="select" name="selectMulti" className="manage-dropdown">
                            <option>Iphone7</option>
                            <option>Iphone6</option>
                            <option>Iphone5</option>
                            <option>Iphone4</option>
                            <option>IphoneX</option>
                        </Input>
                    </Col>
                    <Col md={4} xs={6}></Col>
                </Row>
                <Col md={12} className="mobileList-manage">
                    <Row className="modelCon">
                        {this.state.data.map((val, index) => {
                            return (
                                <div className="model-img-out" key={index}>
                                    <div className={this.state.selected ? "model-image-out active" : "model-image-out"} >
                                        <img onClick={this.onImgClick.bind(this, val._id)} src={val.preview} className="model-image"/>
                                        <p className="model-img-title">{val.name}</p>
                                    </div>
                                </div>
                            )
                        }
                        )}
                    </Row>
                </Col>
            </Container>
        )
    }
}
export default withRouter(withApollo(ManageModel));