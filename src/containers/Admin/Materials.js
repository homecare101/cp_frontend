import React from 'react'
import { Button, Input, Form, FormGroup, ListGroupItem, Container, Row, Col, Label, Collapse, CardBody, Card } from 'reactstrap';
import './admin.css'
import { withRouter } from 'react-router';
import { ApolloProvider, Query, graphql, compose } from 'react-apollo';
import { IsValidForm } from '../../components/common/validation';
import { width } from 'window-size';

class Materials extends React.Component {
    constructor() {
        super()
        this.state={
            data:[
                {_id:1, material:'material #1'},
                {_id:1, material:'material #2'},
                {_id:1, material:'material #3'}
        ]}
    }
    render() {
        return (
            <div>
                <Container fluid className="MainCon">
                   <Row>
                    <Col md={12} className="modelHeading">
                        <p>Editor / Models</p>
                    </Col>
                    </Row>
                    <Row className="headingMain">
                        <Col md={6} sm={6} xs={12}>
                            <p>Manage models</p>
                        </Col>
                        <Col md={6} sm={6} xs={12} className="flexEnd">
                            <Button className="headingBtn">Add new model</Button>
                        </Col>
                    </Row>
                    <Row>
                    {this.state.data.map((val, index) => {
                        return(
                            <Col md={6} key={index} className="materialData">
                                <div className="materialBack">
                                        <Col md={3} xs={12} className="textalinecenter">
                                            <p>{val.material}</p>
                                        </Col>
                                        <Col md={9}  xs={12}>
                                            <Row>
                                                <Col md ={6} xs ={6}>
                                                    <Button>
                                                        <i class="fa fa-pencil" ></i> Rename
                                                    </Button>
                                                </Col>
                                                <Col md={6} xs={6}>
                                                    <Button>
                                                        <i class="fa fa-trash" ></i>    Delete
                                                    </Button>
                                                </Col>
                                            </Row>
                                        </Col>

                                </div>
                            </Col>
                            )
                        })
                    }
                    </Row>
                   
                </Container>  
            </div>
        )
    }
}
export default Materials;