import React from 'react'
import { Button } from 'reactstrap'
import jsPDF from 'jspdf'
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;
pdfMake.fonts = {
    Roboto: {
        normal: 'Roboto-Regular.ttf'
    },
    NanumGothicBold: {
        normal: 'NanumGothicBold.ttf'
    }
}

class OrderDetails extends React.Component {
    constructor(props) {
        super(props)
    }


    makePdf() {
        let orderDetails = this.props.orderToSend
        let casesFinalArray = []
        orderDetails.items.forEach(item => {
            let caseArr = [{
                style: "imageStyle", 
                image: item.preview
            },
            [
                { text: ["Model", " : ", item.model], style: 'anotherStyle' },
                { text: ["Material", " : ", item.material], style: 'anotherStyle' },
            ]]
            casesFinalArray.push(caseArr)
        });

        let docDefinition = {
            content: [
                {
                    layout: 'noBorders',
                    table: {
                        headerRows: 1,
                        widths: ['70%', '30%'],
                        body: casesFinalArray,
                    }
                },
                [
                    { text: ["Address", " : ", orderDetails.fullAddress], style: 'styleAddress', margin: [0, 30, 0, 0] },
                    { text: ["Extra Address", " : ", orderDetails.extraAddress], style: 'styleAddress' },
                    { text: ["ZoneCode", " : ", orderDetails.zonecode], style: 'styleAddress' },
                    { text: ["Mobile", " : ", orderDetails.mobileNumber], style: 'styleAddress' },
                ]
            ],
            styles: {
                header: {
                    font: "NanumGothicBold",
                    fontSize: 22,
                    bold: true
                },
                anotherStyle: {
                    font: "NanumGothicBold",
                    fontSize: 12,
                    alignment: 'left'
                },
                imageStyle: {
                    font: "NanumGothicBold",
                    fontSize: 22,
                    alignment: 'left'
                },
                styleAddress: {
                    font: "NanumGothicBold",
                    fontSize: 22,
                    alignment: 'left',
                },
            }
        }
        return docDefinition
    }
    
    onDownload(address, extAddress) {
        // let doc = new jsPDF();
        // // doc.addFileToVFS('NotoSansCJKjp-Regular.ttf', Base64content);
        // doc.addFont('NotoSansCJKjp-Regular.ttf', 'NotoSansCJKjp', 'normal')
        // doc.setFont('NotoSansCJKjp')

        // let specialElementHandlers = {
        //     '#editor': function (element, renderer) {
        //         return true;
        //     }
        // };
        // let source = document.getElementById("pdfToPrint").innerHTML;
        // doc.fromHTML(source, 15, 15, {
        //     'width': 190,
        //     'elementHandlers': specialElementHandlers
        // },
        //     function (data) {
        //         // doc.text([address,extAddress], 200, 10, {lang: 'SKR'}),
        //         doc.save('sample-page.pdf')
        //     }
        // );

        let docDefinition = this.makePdf()
        pdfMake.createPdf(docDefinition).download();

    }
    render() {
        let orderDetails = this.props.orderToSend
        return (
            <div>
                <div id="pdfToPrint" style={{ overflow: 'scroll', padding: 20, display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
                    <h1 style={{ marginTop: 20 }}>Order Details</h1>
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        {orderDetails.items.map((val, index) => {
                            return (
                                <div style={{ display: 'flex', alignItems: 'center', flexDirection: 'column', marginRight: 20, width: '200px', height: '280px' }}>
                                    <img src={val.preview} style={{ width: '100%', height: '100%' }} />
                                </div>
                            )
                        })}
                    </div>
                    <div>
                        {orderDetails.items.map((val, index) => {
                            return (

                                <div style={{ display: 'flex', flexDirection: 'column', paddingBottom: 20 }}>
                                    <div style={{ display: 'flex', marginTop: -20, flexDirection: 'row' }}><h3>Model-:</h3> <h4>{val.model}</h4></div>
                                    <div style={{ display: 'flex', marginTop: 20, flexDirection: 'row' }}> <h3>Theme-:</h3> <h4>{val.theme}</h4></div>
                                    <div style={{ display: 'flex', marginTop: 20, flexDirection: 'row' }}> <h3>Material-:</h3> <h4>{val.material}</h4></div>
                                </div>
                            )
                        })}
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'column' }}>
                        <div style={{ display: 'flex', marginTop: 20, flexDirection: 'row', fontFamily: 'Noto Sans KR' }}><h3>Full Address-:</h3> <h4>{orderDetails.fullAddress}</h4></div>
                        <div style={{ display: 'flex', marginTop: 20, flexDirection: 'row', fontFamily: 'Noto Sans KR' }}> <h3>Extra Address-:</h3> <h4>{orderDetails.extraAddress}</h4></div>
                        <div style={{ display: 'flex', marginTop: 20, flexDirection: 'row', fontFamily: 'Noto Sans KR' }}> <h3>zonecode-:</h3> <h4>{orderDetails.zonecode}</h4></div>
                        <div style={{ display: 'flex', marginTop: 20, flexDirection: 'row', fontFamily: 'Noto Sans KR' }}> <h3>mobileNumber-:</h3> <h4>{orderDetails.mobileNumber}</h4></div>
                    </div>
                </div>
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                    <Button onClick={this.onDownload.bind(this, orderDetails.fullAddress, orderDetails.extraAddress)} style={{ width: 200, height: 50, marginTop: 20, borderRadius: 30, marginBottom: 30 }}><h3>Download</h3></Button>
                </div>
            </div>
        )
    }
}
export default OrderDetails