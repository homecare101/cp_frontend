import React from 'react'
import { Container, Row, Col, Button, Modal, Input, ListGroup, ListGroupItem } from 'reactstrap';
import './userDetails.css'
import { withRouter } from 'react-router';
import { graphql, compose, withApollo } from 'react-apollo';
import { setToast } from '../../actions/editor';
import { editUserDetailsMutation, changeUserPasswordMutation } from '../../Graphql/Mutations/userDetails';
import { showLoader, stopLoader } from '../../utils/functions';
import { IsValidForm } from '../../components/common/validation';
import { getUserByIdQuery } from '../../Graphql/Query/userDetails';

class UserInfo extends React.Component {
    constructor() {
        super()
        this.state = {
            detailModel: false,
            passwordModel: false,
            userDetails: {
                firstName: '',
                lastName: '',
                phone: '',
                email: '',
                gender: ''
            },
            changePassword: {
                oldPassword: '',
                newPassword: '',
                confirmPassword: ''
            }
        }
    }
    componentDidMount() {
        showLoader()
        let userId = localStorage.getItem('id')

        this.props.client.query({
            query: getUserByIdQuery,
            fetchPolicy: "network-only",
            variables: {
                userId: userId,
            },
        }).then(res => {
            let userDetails = JSON.parse(JSON.stringify(res.data.getUserById))
            this.setState({ userDetails })
            stopLoader()
        })
        .catch(error => {
            stopLoader()
            console.log(error)
        })
    }
    handleChange(key, event) {
        let { userDetails } = this.state
        userDetails[key] = event.target.value
        this.setState({ userDetails })
    }
    handleChangePassword(key, event) {
        let { changePassword } = this.state
        changePassword[key] = event.target.value
        this.setState({ changePassword })
    }
    onEditDetailsModel() {
        let { detailModel , userDetails} = this.state
        userDetails.userName=''
        userDetails.phone=''
        this.setState({ detailModel: !detailModel,userDetails })
        this.componentDidMount()
    }
    changeGender(val) {
        let { userDetails } = this.state
        userDetails.gender = val
        this.setState({ userDetails })
    }
    onChangePasswordModal() {
        let { passwordModel } = this.state
        this.setState({ passwordModel: !passwordModel })
    }
    onEditUserDetails(val) {
        let userId = localStorage.getItem('id')
        let { firstName, lastName, phone, email, gender } = this.state.userDetails
        let fields = [ 'firstName', 'lastName', 'phone', 'email' ]
        let formValidation = IsValidForm(fields, this.state.userDetails)
        console.log(formValidation, formValidation.validate)
        if (formValidation.validate) {
            showLoader()
            this.props.editUserDetails({
                variables: {
                    userId: userId,
                    email: email,
                    firstName: firstName,
                    lastName: lastName,
                    gender: gender,
                    phone: phone,
                },
            }).then(res => {
                this.setState({ modal: false, btnDisable: false })
                this.props.setToast({
                    variables: { type: 'success', message: "User Details Succesfully Added" }
                })
                stopLoader()
                this.setState({ detailModel: false })
            })
            .catch(error => {
                stopLoader()
                console.log(error, "error")
            })
        }else {
            this.props.setToast({
                variables: { type: 'error', message: "Please Fll All Fields" }
            })
        }

    }

    onChangePassword() {
        let userId = localStorage.getItem('id')
        let fields = ['newPassword', 'confirmPassword', 'oldPassword']
        let { newPassword, confirmPassword, oldPassword } = this.state.changePassword
        let formValidation = IsValidForm(fields, this.state.changePassword)
        if (formValidation.validate) {
            if (newPassword === confirmPassword) {
                this.props.changeUserPassword({
                    variables: {
                        userId: userId,
                        password: oldPassword,
                        newPassword: newPassword
                    },
                }).then(res => {
                    console.log(res.data.changeUserPassword.message, "000")
                    this.props.setToast({
                        variables: { type: 'success', message: res.data.changeUserPassword.message }
                    })
                    this.setState({ passwordModel: false })
                })
                    .catch(error => {
                        console.log(error, "error")
                        this.props.setToast({
                            variables: { type: 'error', message: "Old password doesn't match" }
                        })
                    })
            }
            else {
                this.props.setToast({
                    variables: { type: 'error', message: "Password Didn't match" }
                })
            }
        }
        else {
            this.props.setToast({
                variables: { type: 'error', message: "Please Fll All Fields" }
            })
        }

    }

    render() {
        let { detailModel, passwordModel } = this.state
        let { userDetails } = this.state
        return (
            <Container fluid>
                <Row>
                    <Col md={6} sm={12} xs={12} className='no-padding'>
                        <div className="userInfo">
                            <h1>마이페이지</h1>
                        </div>
                        {detailModel ?
                            <div className="userDetailsHeading">
                                <p>이름</p>
                                <Input className="input-UserInfo" type="text" value={userDetails.firstName} placeholder={userDetails.firstName} onChange={this.handleChange.bind(this, 'firstName')} />
                                <p>성</p>
                                <Input className="input-UserInfo" type="text" value={userDetails.lastName} placeholder={userDetails.lastName} onChange={this.handleChange.bind(this, 'lastName')} />
                                <p>전화번호</p>
                                <Input className="input-UserInfo" type="number" value={userDetails.phone} placeholder={userDetails.phone} onChange={this.handleChange.bind(this, 'phone')} />
                                <p>이메일</p>
                                <Input className="input-UserInfo" type="text" value={userDetails.email} placeholder={userDetails.email} onChange={this.handleChange.bind(this, 'email')} />
                                <p>성별</p>
                                <label className="radioInfoGender">
                                    <input type="radio" name="question_1_2" value="남자" onClick={this.changeGender.bind(this, '남자')} />
                                    <span>남자</span>
                                </label>
                                <label className="radioInfoGender">
                                    <input type="radio" name="question_1_2" value="여자" onClick={this.changeGender.bind(this, '여자')} />
                                    <span>여자</span>
                                </label>
                                <Row className="marginZero">
                                    <div>
                                        <Button className="user-Info-save" onClick={this.onEditUserDetails.bind(this)}>저장하기</Button>
                                    </div>
                                    <div>
                                        <Button className="user-Info-edit" onClick={this.onEditDetailsModel.bind(this)}>Cancel</Button>
                                    </div>
                                </Row>
                            </div>
                            :
                            <Row className="marginZero">
                                <div className="userDetailsHeading">
                                <p>이름</p>
                                <p>성이름</p>
                                <p>전화번호</p>
                                <p>이메일</p>
                                <p>성별</p>                                   
                                </div>
                                <div className="userDetails">
                                    <p>{userDetails && userDetails.firstName || 'null'}</p>
                                    <p>{userDetails && userDetails.lastName || 'null'}</p>
                                    <p>{userDetails && userDetails.phone || 'null'}</p>
                                    <p>{userDetails && userDetails.email || 'null'}</p>
                                    <p>{userDetails && userDetails.gender || 'null'}</p>
                                </div>
                            </Row>
                        }
                        {detailModel ? null : <Button className="user-Info-edit-main" onClick={this.onEditDetailsModel.bind(this)}>내 정보 변경</Button>}
                    </Col>
                    <Col md={6} sm={12} xs={12} className="userInfoPassword">
                        <div className="userInfo">
                            <h1>패스워드</h1>
                        </div>
                        {
                            passwordModel ?
                                <div className="passwordModal">
                                    <Input className="input-UserInfo" type="password" placeholder="기존 비밀번호" onChange={this.handleChangePassword.bind(this, 'oldPassword')} />
                                    <Input className="input-UserInfo" type="password" placeholder="새 비밀번호" onChange={this.handleChangePassword.bind(this, 'newPassword')} />
                                    <Input className="input-UserInfo" type="password" placeholder="새 비밀번호 재입력" onChange={this.handleChangePassword.bind(this, 'confirmPassword')} />

                                    <Row className="marginZero">
                                        <div>
                                            <Button className="user-Info-Password-save" onClick={this.onChangePassword.bind(this)}>저장하기</Button>
                                        </div>
                                        <div>
                                            <Button className="user-Info-change-password" onClick={this.onChangePasswordModal.bind(this)}>페스워드 변경</Button>
                                        </div>
                                    </Row>

                                </div> : null
                        }
                        {passwordModel ? null : <Button className="user-Info-change-password-main" onClick={this.onChangePasswordModal.bind(this)}>페스워드 변경</Button>}

                    </Col>
                </Row>
            </Container>
        )
    }

}
export default withRouter(compose(
    withApollo, [
        graphql(setToast, { name: 'setToast' }),
        graphql(editUserDetailsMutation, { name: 'editUserDetails' }),
        graphql(changeUserPasswordMutation, { name: 'changeUserPassword' }),
    ])(UserInfo));
