import React from 'react'
import { Container, Row, Col, Button, ListGroup, ListGroupItem } from 'reactstrap';
import './userDetails.css';
import { ApolloProvider, Query, graphql, compose, withApollo } from 'react-apollo';
import { showLoader, stopLoader } from '../../utils/functions';
import { withRouter } from 'react-router';
import ReactLoading from 'react-loading';
import { getOrderDataMutation } from '../../Graphql/Query/userDetails';

class UserOrder extends React.Component {
    constructor() {
        super()
        this.state = {
            data: []
        }
    }
    componentDidMount() {
        showLoader()
        let userId = localStorage.getItem('id')
        this.props.client.query({
            query: getOrderDataMutation,
            variables: {
                userId: userId,
            },
        }).then(res => {
            let responce = res.data.getOrderData.order
            responce = JSON.parse(JSON.stringify(responce))
            this.setState({ data: responce })
            stopLoader()
        })
        .catch(error => {
            stopLoader()
            console.log(error, "error")
        })
    }
    render() {
        return (
            <div className="order-list-padding-order">
                <div id={'loader'} className="loader">
                    <ReactLoading type='spin' color='#fff' height={60} width={60} />
                </div>
                {this.state.data.map((val, index) => {
                    return (
                        <div key={index}>
                            <Col md={12} className="user-margin-btw">
                                <Row>
                                    <div className="order-list-color">
                                        <Col md={7} sm={7} xs={12} className="user-padding-first">
                                            <Row className="user-content-row padding10">
                                                <div className="width60Text">
                                                    <p className="user-text-color">#{val._id}</p>
                                                </div>
                                                <div className="width40Text">
                                                    <p className="user-text-color-date">{val.createdAt}</p>
                                                </div>
                                            </Row>
                                        </Col>
                                        <Col md={5} sm={5} xs={12} className="user-content-end">
                                            <Row className="width100Text">
                                                <div className="width50Text">
                                                    <p className="user-price">{val.subTotal}원</p>
                                                </div>
                                                <div className="width50Text">
                                                    {val.status === 'Canceled' ?
                                                        <div className="directionRow">
                                                            <div className="IconOutStatus">
                                                                <img src="/cross@3x.png" className="imgAll" />
                                                            </div>
                                                            <p className="user-list-btn-cancel">{val.status}</p>
                                                        </div>
                                                        : null}
                                                    {val.status === 'Completed' ? <div className="directionRow">
                                                        <div className="IconOutStatus">
                                                            <img src="/tickj@3x.png" className="imgAll" />
                                                        </div>
                                                        <p className="user-list-btn-complete">{val.status}</p>
                                                    </div>
                                                        : null}
                                                    {val.status === 'Pending' ? <div className="directionRow">
                                                        <div className="IconOutStatus">
                                                            <img src="/clock@3x.png" className="imgAll" />
                                                        </div>
                                                        <p className="user-list-btn-pending">{val.status}</p>
                                                    </div>
                                                        : null}
                                                </div>
                                            </Row>
                                        </Col>
                                    </div>
                                </Row>
                            </Col>
                        </div>
                    )
                })}
            </div>
        )
    }
}
export default withRouter(withApollo(UserOrder));