import React from 'react'
import { Container, Row, Col, Button, Modal, Input, ListGroup, ListGroupItem } from 'reactstrap';
import './userDetails.css'
import { withRouter } from 'react-router';
import { ApolloProvider, Query, graphql, compose, withApollo } from 'react-apollo';
import { addressMutaion, editAddressMutation } from '../../Graphql/Mutations/userDetails';
import { showLoader, stopLoader } from '../../utils/functions';
import { setToast } from '../../actions/editor';
import { IsValidForm } from '../../components/common/validation';
import UserInfo from './UserInfo';
import { getAddressMutation, deleteAddressQuery } from '../../Graphql/Query/userDetails';

var daum = window.daum;

class UserAddress extends React.Component {
    constructor() {
        super()
        this.state = {
            modal: false,
            editAddress: {},
            addId: '',
            data: [],
            isEdit: false,
            addressMain: null,
            btnDisable: false,
            errorMsg: false,
            errors: null,
            newAddress: {
                fullAddress: '',
                zonecode: '',
                extraAddress: ''
            },
            editCase:false,
            selectedId:null,
        }
    }
    handleChange(key, event) {
        let { newAddress } = this.state
        newAddress[key] = event.target.value
        this.setState({ newAddress, errorMsg: false })
    }
    componentDidMount() {
        showLoader()
        this.getAddresses()
    }
    getAddresses() {
        let userId = localStorage.getItem('id')
        this.props.client.query({
            query: getAddressMutation,
            fetchPolicy: "network-only",
            variables: {
                userId: userId,
            },
        }).then(res => {
            let responce = res.data.getAddress.addresses
            responce = JSON.parse(JSON.stringify(responce))
            this.setState({ data: responce })
            stopLoader()
        })
        .catch(error => {
            stopLoader()
            console.log(error)
        })
    }
    onAddNew() {
       let { newAddress }= this.state
            newAddress.fullAddress=''
            newAddress.zonecode= '',
            newAddress.extraAddress= ''
        this.setState({ modal: !this.state.modal, isEdit: false, editCase:false, newAddress })
    }
    onEditAddress(val){
        let { modal } = this.state
        let { newAddress } = this.state
        newAddress.fullAddress = val.fullAddress
        newAddress.zonecode = val.zonecode
        newAddress.extraAddress = val.extraAddress
        this.setState({ modal:!modal, editCase:true, newAddress, selectedId:val._id  })
    }

    addAddress(id) {
        let { newAddress, editCase, selectedId } = this.state
        let fields = ['fullAddress', 'zonecode']
        let formValidation = IsValidForm(fields, this.state.newAddress)
        this.setState({ errors: formValidation.errors })
        if (formValidation.validate) {
            let { fullAddress, zonecode, extraAddress } = this.state.newAddress
            let userId = localStorage.getItem('id')
            if (editCase) {
                showLoader()
                this.props.editAddressMutation({
                    variables: {
                        addressId: selectedId,
                        userId: userId,
                        fullAddress: fullAddress,
                        zonecode: zonecode,
                        extraAddress: extraAddress
                    },
                }).then(res => {
                    this.setState({ modal: false, btnDisable: false })
                    this.getAddresses()
                    this.props.setToast({
                        variables: { type: 'success', message: "Address Added Succesfully" }
                    })

                })
                .catch(error => {
                    console.log(error, "error")
                })

            }else{
                showLoader()
                this.props.userAddress({
                    variables: {
                        userId: userId,
                        fullAddress: fullAddress,
                        zonecode: zonecode,
                        extraAddress: extraAddress
                    },
                }).then(res => {
                    this.setState({ modal: false, btnDisable: false })
                    this.getAddresses()
                    this.props.setToast({
                        variables: { type: 'success', message: "Address Added Succesfully" }
                    })

                })
                .catch(error => {
                    stopLoader()
                    console.log(error, "error")
                })
            }
        } else {
            this.setState({ errorMsg: true })
        }
    }
    onDeleteAddress(id) {
        let userId = localStorage.getItem('id')
        showLoader()
        this.props.client.query({
            query: deleteAddressQuery,
            variables: {
                addressId: id,
                userId: userId,
            },
        }).then(res => {
            stopLoader()
            let { data } = this.state
            let index = data.findIndex(value => value._id == id)
            data.splice(index, 1)
            this.setState({ data })
            this.props.setToast({
                variables: { type: 'success', message: "Address Deleted Succesfully" }
            })
        })
        .catch(error => {
            stopLoader()
            console.log(error, "error")
        })
    }
    addressDetails() {
        let { newAddress } = this.state
        daum.postcode.load(() => {
            new daum.Postcode({
                onComplete: ((data) => {
                    let addr = '';
                    let extraAddr = '';
                    if (data.userSelectedType === 'R') {
                        addr = data.roadAddress;
                    } else {
                        addr = data.roadAddress;
                    }
                    // if (data.userSelectedType === 'R') {
                    //     if (data.bname !== '' && /[동|로|가]$/g.test(data.bname)) {
                    //         extraAddr += data.bname;
                    //     }
                    //     if (data.buildingName !== '' && data.apartment === 'Y') {
                    //         extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                    //     }
                    //     if (extraAddr !== '') {
                    //         extraAddr = extraAddr;
                    //     }
                    //     newAddress.extraAddress = extraAddr
                    //     this.setState({ newAddress })

                    // } else {
                    //     document.getElementById("sample6_extraAddress").value = ''
                    // }
                    newAddress.zonecode = data.zonecode
                    newAddress.fullAddress = addr
                    this.setState({ newAddress })
                    document.getElementById("sample6_detailAddress").focus()
                })
            }).open();
        })
    }
    render() {
        let { zonecode, fullAddress, extraAddress } = this.state.newAddress
        let { errors, errorMsg } = this.state
        return (
            <div className="saveDesignMain">
                <div className="userInfoMain">
                    <UserInfo />
                </div>
                <div className="order-list-padding-address">
                    <div>
                        <h1>배송 주소</h1>
                    </div>
                    <div className="order-list-address-Row">
                        {this.state.data.map((val, index) => {
                            return (
                                <div key={index} >
                                    <div className="address_section">
                                        <h3>주소 {index + 1}</h3>
                                        <ul className="user-address-data">
                                            <li>
                                                <span>주소 :</span><p>{val.fullAddress}</p>
                                            </li>
                                            <li>
                                                <span>우편번호 :</span><p>{val.zonecode}</p>
                                            </li>
                                            <li>
                                                <span>상세주소 :</span><p>{val.extraAddress}</p>
                                            </li>
                                            <Button className="edit-user-address-btn-main" onClick={this.onEditAddress.bind(this, val)}>주소 변경</Button>
                                            <Button className="delete-user-address-btn-main" onClick={this.onDeleteAddress.bind(this, val._id)}>삭제</Button>
                                        </ul>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </div>
                <Button onClick={this.onAddNew.bind(this)} className="user-address-btn-main">주소 추가</Button>
                <Modal isOpen={this.state.modal} centered className="home-auth-modal">
                    <button type="button" onClick={this.onAddNew.bind(this)}  className="modal-close-buttonMain" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                   <div className="user-address-modal">
                        <div className="address-modalOpen">
                            <h1> {this.state.editCase ? '주소변경' : 'Please Add New Address'}</h1>
                            <Input className="input-optional" type="text" value={zonecode} placeholder="우편번호" onChange={this.handleChange.bind(this, 'zonecode')} />
                            {errorMsg && <p className="hasError">
                                {errors.zonecode}
                            </p>}
                            <Button className="add-user-address-btn-main" onClick={this.addressDetails.bind(this)}>우편번호 찾기</Button>
                            <Input className="input-optional" type="text" value={fullAddress} placeholder="주소" onChange={this.handleChange.bind(this, 'fullAddress')} />
                            {errorMsg && <p className="hasError">
                                {errors.fullAddress}
                            </p>}
                            <Input className="input-optional" type="text" id="sample6_detailAddress" placeholder="상세주소 (optional)" value={extraAddress} onChange={this.handleChange.bind(this, 'extraAddress')} />            
                            <Button className="add-user-address-btn-main" onClick={this.addAddress.bind(this)}>{this.state.editCase ? '수정' : 'Save' }</Button>
                        </div>
                    </div>
                </Modal>
            </div>
        )
    }
}
export default withRouter(compose(
    withApollo, [
        graphql(addressMutaion, { name: 'userAddress' }),
        graphql(editAddressMutation, { name: 'editAddressMutation' }),
        graphql(setToast, { name: 'setToast' }),
    ])(UserAddress));
