import React from 'react'
import { Container, Row, Col, Button, ListGroup, ListGroupItem } from 'reactstrap';
import './userDetails.css'
import { withRouter } from 'react-router';
import { withApollo, Query, compose, graphql } from 'react-apollo';
import { addToCartMutaion } from '../../Graphql/Mutations/userDetails';
import { showLoader, stopLoader} from '../../utils/functions';
import { setToast } from '../../actions/editor';
import { getCaseMutation } from '../../Graphql/Query/editor';

class UserSavedDesign extends React.Component {
    constructor() {
        super()
        this.state = {
            data: []
        }
    }
    componentDidMount() {
        showLoader()
        let userId = localStorage.getItem('id')
        this.props.client.query({
            query: getCaseMutation,
            fetchPolicy: "network-only",
            variables: {
                userId: userId,
            },
        }).then(res => {
            let array = res.data.getUserCases.cases
             this.setState({ data: array })
             stopLoader()
        })
        .catch(error => {
            stopLoader()
            console.log(error, "error")
            let errorText = { ...error }
            if (errorText !== {}) {
            this.props.setToast({
                variables: { type: 'error', message: "Some error occure please try again later" }
            })
        }
        })
    }
    addToCart(data){
            let cartData = JSON.parse(localStorage.getItem('cartData'))
            cartData = JSON.parse(JSON.stringify(cartData))
            let cartItems = cartData.items
            cartItems.push(data)
            let newCart = []
            let userID = localStorage.getItem('id')
            if(cartItems.length >= 1){
                cartItems.forEach((item, key) => {
                    if(item._id === null){
                    } else{
                        let existanceCheck = newCart.map(function(e) { return e._id; }).indexOf(item._id);
                        if(existanceCheck === -1){
                            let itemToSend = JSON.parse(item.caseData)
                            newCart.push({caseData: item.caseData,
                                count: item.count,
                                name: item.name,
                                preview: item.preview,
                                price: item.price,
                                _id: item._id,
                                model: itemToSend.editor.model || "",
                                material: itemToSend.editor.materials || "",
                                outlineSvgData: item.outlineSvgData
                            })
                        } else {
                            let count = newCart[existanceCheck].count
                            count = count + 1
                            newCart[existanceCheck].count = count
                        }
                    }  
                });
            } else {
                newCart.push({
                    caseData: data.caseData,
                    count: data.count,
                    name: data.name,
                    preview: data.preview,
                    price: data.price,
                    _id: data._id,
                    model: data.model || "",
                    material: data.material || "",
                    outlineSvgData: data.outlineSvgData
                })
            }
            this.props.addCart({
                    variables: {
                        userId: userID,
                        subTotal: 0,
                        cartId : cartData._id,
                        items: newCart
                    },
                }).then(res => {
                    this.props.setToast({
						variables: { type: 'success', message: "Item Added To Cart" }
                    })
                    cartData.items = newCart
                    localStorage.setItem('cartData' , JSON.stringify(cartData))
                })
                .catch(error => {
                        console.log(error, "error")
                        let errorText = { ...error }
                        if (errorText !== {}) {
                        this.props.setToast({
                            variables: { type: 'error', message: "Some error occure" }
                        })
                    }
                })
        
     
    }
    onEdit(params){
        const {match} = this.props
         this.props.history.push(`/editor/`+params)
    }
    render() {
        return (
            <div className="order-list-padding-saved">
                {this.state.data.map((val, index) => {
                    return (
                        <div key={index}>
                                <Row>
                                    <Col md={12} className="user-margin-btw no-padding">
                                        <div className="order-list-color-design"> 
                                            <Col md={12}  className="user-padding-first"> 
                                                <Row> 
                                                    <Col md={6} sm={6} >
                                                        <Row>
                                                            <Col md={3} xs={4} className="no-padding">
                                                                <div className="saveDesignImgOut">
                                                                    <img src={val.preview} className="imgAll"/>
                                                                </div>
                                                            </Col>
                                                            <Col md={6} xs={5} className="user-content-center no-padding">
                                                                <p>{val.name}</p>
                                                            </Col>
                                                            <Col md={3} xs={3} className="user-content-end no-padding">
                                                                <p><b>$ {val.price}</b></p>
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                    <Col md={6} sm={6}  className="SavedBtns">
                                                        <Row className="fullWidth">
                                                            <Col md={6} sm={6} className="edit_btn no-padding">
                                                                <Button onClick={this.onEdit.bind(this, val._id)} className="user-edit-btn-main">수정</Button>
                                                            </Col> 
                                                            <Col md={6} sm={6} className="cart_btn no-padding">
                                                                <Button onClick={this.addToCart.bind(this , val)} className="user-cart-btn-main">장바구니 담기</Button>
                                                            </Col> 
                                                        </Row>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </div>
                                    </Col>
                                </Row>
                        </div>
                    )
                })}
            </div>
        )
    }
}
export default withRouter(compose(
    withApollo,[
        graphql( addToCartMutaion, { name:'addCart' }),
        graphql( setToast, { name:'setToast' })]
    )(UserSavedDesign))



