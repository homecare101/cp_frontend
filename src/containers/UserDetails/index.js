import React from 'react'
import Header from '../../components/Header';
import { Container, Row, Col, Button, ListGroup, ListGroupItem } from 'reactstrap';
import './userDetails.css'
import { withRouter } from 'react-router';
import UserOrder from './UserOrder';
import UserSavedDesign from './UserSavedDesign';
import UserAddress from './UserAddress';
import UserPayment from './UserPayment';
import { Route, Link } from 'react-router-dom'
import ReactLoading from 'react-loading';
import LightFooter from '../../components/LightFooter';
import { withApollo } from 'react-apollo';
import { getUserByIdQuery } from '../../Graphql/Query/userDetails';


class UserDetails extends React.Component {
    constructor() {
        super()
        this.state = {
            toggelbtn: false,
            toogleMenu: true,
            toggelbtnclose: false,
            selectedTab: '',
            userDetails:null
        }
    }
    componentWillMount(){
        let userId = localStorage.getItem('id')
        this.props.client.query({
            query: getUserByIdQuery,
            fetchPolicy: "network-only",
            variables: {
                userId: userId,
            },
        }).then(res => {
            let userDetails = JSON.parse(JSON.stringify(res.data.getUserById))
            this.setState({userDetails})
            console.log(userDetails)
        })
        .catch(error => {
            console.log(error)
        })
    }
    componentDidMount(event) {
        this.handleResize()
        window.addEventListener('resize', () => {
            this.handleResize()
        });
        let selectedTab = localStorage.getItem('selectedTabId')
        this.setState({ selectedTab })

    }
    handleResize() {
        if (window.innerWidth < 767) {
            this.setState({ toggelbtn: true, toogleMenu: false })
        }
        if (window.innerWidth > 768) {
            this.setState({ toggleMenu: true, toggelbtn: false, toggelbtnclose: false })
        }
    }
    toggleOpenMenu() {
        this.setState({ toogleMenu: true, toggelbtn: false, toggelbtnclose: true })
    }
    toggleCloseMEnu() {
        this.setState({ toogleMenu: false, toggelbtn: true, toggelbtnclose: false })
    }
    logOut() {
        localStorage.removeItem('token')
        localStorage.removeItem('id')
        localStorage.removeItem('kakao_cafba843cbfbce3d139528cca7e72aa7')
        localStorage.removeItem('cartData')
    }
    handleClicksavedDesign(id) {
        this.setState({ selectedTab: id })
        localStorage.setItem('selectedTabId', id)
        const { match } = this.props
        this.props.history.push(`${match.url}/savedDesign`)
    }
    handleClickOrders(id) {
        localStorage.setItem('selectedTabId', id)
        this.setState({ selectedTab: id })
        const { match } = this.props
        this.props.history.push(`${match.url}/orders`)
    }
    handleClickAddress(id) {
        localStorage.setItem('selectedTabId', id)
        this.setState({ selectedTab: id })
        const { match } = this.props
        this.props.history.push(`${match.url}/address`)
    }
    handleClickPayment(id) {
        localStorage.setItem('selectedTabId', id)
        this.setState({ selectedTab: id })
        const { match } = this.props
        this.props.history.push(`${match.url}/payment`)
    }
    render() {
        const { match } = this.props
        let { userDetails } = this.state
        return (
            <div>
                <div id={'loader'} className="loader">
                    <ReactLoading type='spin' color='#fff' height={60} width={60} />
                </div>
                <Header />
                <Container fluid className="userModalOut">
                    <Row className="modelHeight">
                        <Col md={3} className="no-padding borderRightMain">
                            <div className="user-btn-toggle">
                                {this.state.toggelbtn ? <Button className="toggleButton" outline onClick={this.toggleOpenMenu.bind(this)}>
                                    <div>
                                        <div class="bar1"></div>
                                        <div class="bar2"></div>
                                        <div class="bar3"></div>
                                    </div>
                                </Button> : null}
                                {this.state.toggelbtnclose ? <Button className="toggleButton" outline onClick={this.toggleCloseMEnu.bind(this)}>
                                    <i class="fa fa-close" style={{ display: 'flex' }}></i>
                                </Button> : null}
                            </div>
                            <div className={this.state.toogleMenu ? "user-display-none activeMenu" : "user-display-none"}>
                                <div className="user-title">
                                    <div className="display-flex-profile">
                                        <div className="profilePic">
                                            <p>{ userDetails ? userDetails.userName && userDetails.userName.charAt(0) || userDetails.firstName&&  userDetails.firstName.charAt(0) : ''}</p>
                                        </div>
                                        <p>{ userDetails ? userDetails.userName || userDetails.firstName : ''}</p>
                                    </div>
                                </div>
                                <div className="user-list">
                                    <ListGroup>
                                        <div onClick={this.handleClickOrders.bind(this, 1)} className={this.state.selectedTab == 1 ? "activeTabMenu user-list-outter" : "user-list-outter"}>
                                            <div className="listIconOut">
                                                {this.state.selectedTab == 1 ? <img src="/list@3x.png" className="imgAll" /> : <img src="/list.png" className="imgAll" />}
                                            </div>
                                            <ListGroupItem >주문 내역</ListGroupItem>
                                            {this.state.selectedTab == 1 ? <div className="arrowLeftIcon"> <img src="/triangle@3x.png" className="imgAll" /> </div> : null}
                                        </div>
                                        <div onClick={this.handleClicksavedDesign.bind(this, 2)} className={this.state.selectedTab == 2 ? "activeTabMenu user-list-outter" : "user-list-outter"}>
                                            <div className="listIconOut">
                                                {this.state.selectedTab == 2 ? <img src="/saveBlue.png" className="imgAll" /> : <img src="/save@3x.png" className="imgAll" />}
                                            </div>
                                            <ListGroupItem >저장된 나의 디자인</ListGroupItem>
                                            {this.state.selectedTab == 2 ? <div className="arrowLeftIcon"> <img src="/triangle@3x.png" className="imgAll" /> </div> : null}
                                        </div>
                                        <div onClick={this.handleClickAddress.bind(this, 3)} className={this.state.selectedTab == 3 ? "activeTabMenu user-list-outter" : "user-list-outter"}>
                                            <div className="listIconOut">
                                                {this.state.selectedTab == 3 ? <img src="/personBlue.png" className="imgAll" /> : <img src="/person.png" className="imgAll" />}
                                            </div>
                                            <ListGroupItem >개인 세부 정보</ListGroupItem>
                                            {this.state.selectedTab == 3 ? <div className="arrowLeftIcon"> <img src="/triangle@3x.png" className="imgAll" /> </div> : null}
                                        </div>
                                        <div onClick={this.handleClickPayment.bind(this, 4)} className={this.state.selectedTab == 4 ? "activeTabMenu user-list-outter" : "user-list-outter"}>
                                            <div className="listIconOut">
                                                {this.state.selectedTab == 4 ? <img src="/cardBlue.png" className="imgAll" /> : <img src="/card@3x.png" className="imgAll" />}
                                            </div>
                                            <ListGroupItem >결제정보</ListGroupItem>
                                            {this.state.selectedTab == 4 ? <div className="arrowLeftIcon"> <img src="/triangle@3x.png" className="imgAll" /> </div> : null}
                                        </div>
                                        <div onClick={this.logOut.bind(this)} className={this.state.selectedTab == 5 ? "activeTabMenu user-list-outter" : "user-list-outter"}>
                                            <div className="listIconOut">
                                                <img src="/sign-out@3x.png" className="imgAll" />
                                            </div>
                                            <Link className="decorationNone" to="/" ><ListGroupItem><span className="UserLogout">로그아웃</span></ListGroupItem></Link>
                                        </div>
                                    </ListGroup>
                                </div>
                                <div className="border-right-end"></div>
                            </div>
                        </Col>
                        <Col md={9} className="backgroundWhiteALl">
                            <div>
                                <Route
                                    exact
                                    path={`${match.url}/orders`}
                                    component={UserOrder}
                                />
                                <Route
                                    exact
                                    path={`${match.url}/savedDesign`}
                                    component={UserSavedDesign}
                                />
                                <Route
                                    exact
                                    path={`${match.url}/address`}
                                    component={UserAddress}
                                />
                                <Route
                                    exact
                                    path={`${match.url}/payment`}
                                    component={UserPayment}
                                />
                            </div>
                        </Col>
                    </Row>
                </Container>
                <LightFooter />
            </div>
        )
    }
}
export default withRouter(withApollo(UserDetails));