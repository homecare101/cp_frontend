import React from 'react'
import { Container, Row, Col, Button, Input, Modal, ListGroup, ListGroupItem } from 'reactstrap';
import './userDetails.css'
import { withRouter } from 'react-router';
import { IsValidForm } from '../../components/common/validation';
import { withApollo, compose, graphql } from 'react-apollo';
import { showLoader, stopLoader } from '../../utils/functions';
import ReactLoading from 'react-loading';
import { addPaymentMutaion } from '../../Graphql/Mutations/userDetails';
import { setToast } from '../../actions/editor';
import { getPaymentMutation, deletePaymentMutation, selectPrimaryCardQuery } from '../../Graphql/Query/userDetails';

class UserPayment extends React.Component {
    constructor() {
        super()
        this.state = {
            selectedId: '',
            modal: false,
            errorDate: false,
            errors: null,
            enterMonth: '',
            enterYear: '',
            NewAccount: {
                id: '',
                accNo: '',
                date: '',
                selected: false
            },
            account: [],
        }
    }
    componentDidMount() {
        showLoader()
        this.getPaymentMethods()
    }
    getPaymentMethods() {
        var userId = localStorage.getItem('id')
        this.props.client.query({
            query: getPaymentMutation,
            fetchPolicy: "network-only",
            variables: {
                userId: userId,
            },
        }).then(res => {
            var responce = res.data.getPaymentMethod.paymentMethod
            responce = JSON.parse(JSON.stringify(responce))
            this.setState({ account: responce })
            stopLoader()
        })
        .catch(error => {
            stopLoader()
            console.log(error, "error")
        })
    }
    onSelect(cardId) {
        showLoader()
        var userId = localStorage.getItem('id')
        this.props.client.query({
            query: selectPrimaryCardQuery,
            fetchPolicy: "network-only",
            variables: {
                userId: userId,
                cardId: cardId
            },
        }).then(res => {
            stopLoader()
            this.getPaymentMethods()
            this.props.setToast({
                variables: { type: 'success', message: "Card Set As Primary" }
            })
        })
        .catch(error => {
            stopLoader()
            console.log(error, "error")
        })

    }
    loginToggle() {
        let { NewAccount } = this.state
        NewAccount = {
            date: '',
            accNo: '',
        }
        this.setState({ modal: !this.state.modal, errorInd: false, errorDate: false, NewAccount });
    }
    handleChange(key, event) {
        let value = event.target.value;
        let { NewAccount } = this.state
        NewAccount[key] = value
        this.setState({ NewAccount, errorInd: false, errorDate: false, errors:null });
    }

    handleChangeDate(key, event) {      
        this.setState({ errorDate: false, errors:null })
        var event = event.target.value
        let { NewAccount } = this.state
        let fields = ['date']
        if (key == 'date' && NewAccount[key].length < event.length) {
            if (event.length == 1) {
                if (parseInt(event) > 1) {
                    NewAccount[key] = '0';
                }
                else {
                    NewAccount[key] = event;
                }
            }
            else if (event.length == 2) {
                if (parseInt(event) > 12) {
                    NewAccount[key] = '12/';
                }
                else {
                    NewAccount[key] = event + '/';
                }
            }
            else if (event.length > 3) {
                NewAccount[key] = event;
                this.setState({ NewAccount })
            }
            let inputyear = NewAccount[key].toString().substr(-2)
            this.setState({ enterYear: inputyear, NewAccount })
        }
        else {
            NewAccount[key] = event;
            this.setState({ NewAccount })
        }


    }
    onSave() {
        let fields = ['accNo', 'date']
        let { account } = this.state
        let formValidation = IsValidForm(fields, this.state.NewAccount)
        this.setState({ errors: formValidation.errors })
        var { accNo, date, selected } = this.state.NewAccount
        var inputMonth = date.toString().substring(0, 2);
        var month = new Date().getMonth().toString()
        var year = new Date().getFullYear().toString().substr(-2)
        if (formValidation.validate) {
            if (year > this.state.enterYear || this.state.enterYear === '') {
                this.setState({ errorDate: true })
            }
            else {
                if (this.state.NewAccount['date'].length < 5) {
                    this.setState({ errorDate: true })
                }
                else {
                    showLoader()
                    var { NewAccount } = this.state
                    var userId = localStorage.getItem('id')
                    this.props.addPayment({
                        variables: {
                            userId: userId,
                            accNo: accNo,
                            date: date,
                            selected: false,
                        },
                    }).then(res => {
                        var dataRes = res.data.addPaymentMethod
                        dataRes = JSON.parse(JSON.stringify(dataRes))
                        account.push(dataRes)
                        this.setState({ modal: false, account })
                        stopLoader()
                        this.props.setToast({
                            variables: { type: 'success', message: "Card Added Succesfully" }
                        })
                    })
                    .catch(error => {
                        console.log(error, "error")
                    })
                }
            }
        } else {
            this.setState({ errorInd: true })
        }
    }
    onDeleteCard(val) {
        var userId = localStorage.getItem('id')
        showLoader()
        this.props.client.query({
            query: deletePaymentMutation,
            variables: {
                paymentId: val,
                userId: userId,
            },
        }).then(res => {
            let { account } = this.state
            let index = account.findIndex(value => value._id == val)
            account.splice(index, 1)
            this.setState({ modal: false, account })
            stopLoader()
            this.props.setToast({
                variables: { type: 'success', message: "Card Deleted Succesfully" }
            })
        })
            .catch(error => {
                console.log(error, "error")
            })
    }
    render() {
        let { accNo, date } = this.state.NewAccount
        let { errors } = this.state
        return (
            <div className="payMain">
                <div id={'loader'} className="loader">
                    <ReactLoading type='spin' color='#fff' height={60} width={60} />
                </div>
                <Col md={12}>
                    <Row>
                        <div onClick={this.loginToggle.bind(this)} className="payOut">
                            <div className="payIconOut">
                                <div className="payIconMainOut">
                                    <i className="fa fa-plus payIcon"></i>
                                </div>
                                <p>결제카드 추가</p>
                            </div>
                        </div>
                    </Row>
                    <Row>
                        <div className="payConatainer">
                            {this.state.account.map((val, index) => {
                                return (
                                    <div key={index} className={val.selected ? "payOutCardsSelected" : "payOutCards"}>
                                        <div className="payCardOut">
                                            <p className="payAccNo">{val.accNo}</p>
                                            <span>{val.date}</span>
                                            <div className="payPrimaryText">
                                                {val.selected ? <span><span className="pay-tick">&#10003;</span>
                                                    <span>주결제카드</span></span> : <span onClick={this.onSelect.bind(this, val._id)}>주 결제카드로 변경</span>}
                                            </div>
                                            <div className="payCardIcon" >
                                                <span onClick={this.onDeleteCard.bind(this, val._id)}><u>삭제</u></span>
                                                <div className="payImgOut">
                                                    <img src="/mc.png" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}
                        </div>
                    </Row>
                </Col>
                <Modal isOpen={this.state.modal} centered className="home-auth-modal">
                    <button type="button" onClick={this.loginToggle.bind(this)}  className="modal-close-buttonMain" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div className="modalOpen">
                        <h1>결제카드 입력</h1>
                        <div className="textBox">
                            <div className="textBoxError">
                                <Input className="name" type="number" name="accNo" maxLength={16} value={accNo} onChange={this.handleChange.bind(this, 'accNo')} placeholder="카드번호" />
                                {errors ? <span className="hasError errorMsgCard">{errors.accNo}</span> : null}
                            </div>
                            <div className="textBoxError">
                                <Input className="name" type="text" name="date" maxLength={5} value={date} onChange={this.handleChangeDate.bind(this, 'date')} placeholder="유효기간" />
                                {errors ? <span className="hasError errorMsgCard">{errors.date}</span> : null}
                                {this.state.errorDate ? <span className="hasError errorMsgCard">유효기간을 입력하세요</span> : null}
                            </div>
                        </div>
                        <Button onClick={this.onSave.bind(this)} className="add-user-payment-btn-main">저장</Button>
                    </div>
                </Modal>
            </div>
        )
    }
}
//export default withRouter(withApollo(UserPayment))

export default withRouter(compose(
    withApollo,
    [graphql(addPaymentMutaion, { name: 'addPayment' }),
    graphql(setToast, { name: 'setToast' }),]
)(UserPayment))
