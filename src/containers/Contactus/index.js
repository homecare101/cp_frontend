import React from 'react'
import Header from '../../components/Header';
import { Button, Input, Form, FormGroup, Container, Row, Col, Alert } from 'reactstrap';
import './contactUs.css'
import { withRouter } from 'react-router';
import { Query, graphql, compose } from 'react-apollo';
import { IsValidForm } from '../../components/common/validation';
import { contactUsMutaion } from '../../Graphql/Mutations/auth';
import { setTimeout } from 'timers';
import LightFooter from '../../components/LightFooter';
class Contactus extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sendMessage: {
                name: '',
                email: '',
                message: '',
            },
            resMsg: '',
            errors: {},
            errorInd: false,
            errorRes: '',
            visible: false
        };
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(key, event) {
        let value = event.target.value;
        let { sendMessage } = this.state
        sendMessage[key] = value
        this.setState({ sendMessage });
    }
    sendMsg() {
        let fields = ['name', 'email', 'message']
        let formValidation = IsValidForm(fields, this.state.sendMessage)
        this.setState({ errors: formValidation.errors })
        if (formValidation.validate) {
            this.setState({ errorInd: false })
            let { name, email, message } = this.state.sendMessage
            this.props.contactUs({
                    variables: {
                        name: name,
                        email: email,
                        message: message,
                    },
                }).then(response => {
                    let { name, email, message } = this.state.sendMessage
                    var responseMsg = response.data.contactUs.message
                    let { sendMessage } = this.state
                    sendMessage= {
                        name: '',
                        email: '',
                        message: '',
                    }
                    this.setState({ sendMessage, resMsg: responseMsg, visible:true },(res)=>{
                        setTimeout(() => {
                            this.setState({visible:false })
                        }, 2000);
                    })
                }).catch(error => {
                    let errorText = { ...error }
                    if (errorText !== {}) {
                        this.setState({ errorRes: errorText.graphQLErrors[0].message, errorInd: true })
                    }
                })

        } else {
            this.setState({ errorInd: true })
        }
    }
    onDismiss() {
        this.setState({ visible: false });
      }
    render() {
        let { name, email, message } = this.state.sendMessage
        const { errorInd, errors, errorRes, resMsg, visible } = this.state
        return (
            <div>
                <Header />
                <Container fluid>
                    <Row>
                        <Col md={12} className="heading-out">
                            <h1>Contact us</h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6} >
                            <div className="contactus-block-main">
                                <div className="contactus-block-content">
                                    <div className="contactus-icon-out"><i className="zmdi zmdi-phone contactus-icon"></i></div>
                                    <div className="contactus-block-text">Call us at <b>033 456 7890</b><a href="#" className="conatctus-link-text"><u>request callback</u></a></div>
                                </div>
                                <div className="contactus-block-content">
                                    <div className="contactus-icon-out"><i className="zmdi zmdi-instagram contactus-icon"></i></div>
                                    <div className="contactus-block-text">Follow us #customPark</div>
                                </div>
                                <div className="contactus-block-content">
                                    <div className="contactus-icon-out"><i className="zmdi zmdi-email-open contactus-icon"></i></div>
                                    <div className="contactus-block-text">Email us on hello@custompark.com</div>
                                </div>
                                <div className="contactus-block-content-last">
                                    <div className="contactus-icon-out"><i className="zmdi zmdi-pin contactus-icon"></i></div>
                                    <div className="contactus-block-text">Visit us in Seoul <div> Apt. 102, Sajik-ro-3-gil 23 Jongno-gu, Seoul  30174</div></div>
                                </div>
                            </div>
                        </Col>
                        <Col md={{ size: 4, offset: 1 }} className="contactus-block-input">
                            <Row className="reduceSpacing">
                                <Form>
                                    <Col md={12}>
                                    <Row>
                                        <Col md={6}>
                                            <FormGroup>
                                                <Input type="text" name="name" value={name} placeholder="Name" className="contcatus-textbox" onChange={this.handleChange.bind(this, 'name')} />
                                                {errorInd && <p className="hasError">
                                                    {errors.name}
                                                </p>}
                                            </FormGroup>
                                        </Col>
                                        <Col md={6}>
                                            <FormGroup>
                                                <Input type="email" value={email} name="email" placeholder="Email" className="contcatus-textbox" onChange={this.handleChange.bind(this, 'email')} />
                                                {errorInd && <p className="hasError">
                                                    {errors.email}
                                                </p>}
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    </Col>
                                    <Col md={12}>
                                        <FormGroup>
                                            <Input type="textarea" value={message} name="message" placeholder="Message" className="contactus-input-message" onChange={this.handleChange.bind(this, 'message')} />
                                            {errorInd && <p className="hasError">
                                                {errors.message}
                                            </p>}
                                        </FormGroup>
                                    </Col>
                                    <Col md={12}>
                                        <Row>
                                            <Col md={5}>
                                                <Button className="contactus-button" onClick={this.sendMsg.bind(this)}>Send</Button>
                                            </Col>
                                            <Col md={7}>
                                            <Alert color="success" isOpen={visible} toggle={this.onDismiss.bind(this)} fade={false} className="contactus-alert">
                                                {/* {resMsg} */}
                                                your message is registered
                                            </Alert>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Form>
                            </Row>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12} className="no-padding">
                            <img src="/map4.png" className="contactus-image-map" />
                        </Col>
                    </Row>
                </Container>
                <LightFooter />
            </div>
        )
    }
}
export default withRouter(compose(
    graphql(contactUsMutaion, { name: 'contactUs' })
)(Contactus));