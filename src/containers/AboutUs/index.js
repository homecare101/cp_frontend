import React, { Component } from 'react';
import Header from '../../components/Header';
import { Container, Row, Col } from 'reactstrap';
import './AboutUs.css'
import QueryContactUs from '../../components/QueryContactUs';
import LightFooter from '../../components/LightFooter';
class AboutUs extends Component {
    render() {
        return (
            <div>
                <Header />
                <Container fluid>
                    <Row>
                        <Col md={12}>
                            <Col md={7}>
                                <h1 className="about-heading">About Us</h1>
                                <div className="about-para">
                                    <p>
                                        CaseApp was created as a solution to make it easy for all people to be able to order custom designed products online. As part of Stickers That Stick LLC, we have more than 9 years in the business of printing and now has become a worldwide supplier of custom phone cases.
                                    </p>
                                    <p>
                                        Our team is filled with ambitious people excited about making the best cases and skins possible built on a user friendly website. We are constantly pushing our creativity and advancing our technologies to become the number one source for custom cases.
                                    </p>
                                    <p>
                                        We always strive for 100% customer satisfaction, so if you have any thoughts or feedback you want to share with us please send us an email to info@caseapp.com.
                                    </p>
                                    <p>
                                        Why not take a look at our instagram @caseapp where over 40,000 people follow us and get inspired by us everyday.
                                    </p>
                                </div>
                            </Col>
                            <Col md={5} className="aboutus-con-out">
                                <div className="aboutus-con-main">

                                </div>
                            </Col>
                        </Col>
                    </Row>
                        <QueryContactUs />
                </Container>
                <LightFooter />
            </div>
        )
    }
}

export default AboutUs